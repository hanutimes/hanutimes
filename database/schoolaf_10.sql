-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 29, 2013 at 07:15 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `schoolaf_10`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_url`
--

CREATE TABLE IF NOT EXISTS `access_url` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `description` text,
  `active` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `url_type` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `access_url`
--

INSERT INTO `access_url` (`id`, `url`, `description`, `active`, `created_by`, `tms`, `url_type`) VALUES
(1, 'http://localhost/', ' ', 1, 1, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `access_url_rel_course`
--

CREATE TABLE IF NOT EXISTS `access_url_rel_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_url_id` int(10) unsigned NOT NULL,
  `c_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `access_url_rel_course`
--

INSERT INTO `access_url_rel_course` (`id`, `access_url_id`, `c_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `access_url_rel_session`
--

CREATE TABLE IF NOT EXISTS `access_url_rel_session` (
  `access_url_id` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`access_url_id`,`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_url_rel_session`
--


-- --------------------------------------------------------

--
-- Table structure for table `access_url_rel_user`
--

CREATE TABLE IF NOT EXISTS `access_url_rel_user` (
  `access_url_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`access_url_id`,`user_id`),
  KEY `idx_access_url_rel_user_user` (`user_id`),
  KEY `idx_access_url_rel_user_access_url` (`access_url_id`),
  KEY `idx_access_url_rel_user_access_url_user` (`user_id`,`access_url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_url_rel_user`
--

INSERT INTO `access_url_rel_user` (`access_url_id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `announcement_rel_group`
--

CREATE TABLE IF NOT EXISTS `announcement_rel_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `announcement_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `announcement_rel_group`
--


-- --------------------------------------------------------

--
-- Table structure for table `block`
--

CREATE TABLE IF NOT EXISTS `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `path` varchar(255) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `block`
--


-- --------------------------------------------------------

--
-- Table structure for table `branch_sync`
--

CREATE TABLE IF NOT EXISTS `branch_sync` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_url_id` int(10) unsigned NOT NULL,
  `branch_name` varchar(250) DEFAULT '',
  `branch_ip` varchar(40) DEFAULT '',
  `latitude` decimal(15,7) DEFAULT NULL,
  `longitude` decimal(15,7) DEFAULT NULL,
  `dwn_speed` int(10) unsigned DEFAULT NULL,
  `up_speed` int(10) unsigned DEFAULT NULL,
  `delay` int(10) unsigned DEFAULT NULL,
  `admin_mail` varchar(250) DEFAULT '',
  `admin_name` varchar(250) DEFAULT '',
  `admin_phone` varchar(250) DEFAULT '',
  `last_sync_trans_id` bigint(20) unsigned DEFAULT '0',
  `last_sync_trans_date` datetime DEFAULT NULL,
  `last_sync_type` char(20) DEFAULT 'full',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `branch_sync`
--


-- --------------------------------------------------------

--
-- Table structure for table `branch_sync_log`
--

CREATE TABLE IF NOT EXISTS `branch_sync_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `branch_sync_id` int(10) unsigned NOT NULL,
  `sync_trans_id` bigint(20) unsigned DEFAULT '0',
  `sync_trans_date` datetime DEFAULT NULL,
  `sync_type` char(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `branch_sync_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `branch_transaction`
--

CREATE TABLE IF NOT EXISTS `branch_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `action` char(20) DEFAULT NULL,
  `item_id` char(36) DEFAULT NULL,
  `orig_id` char(36) DEFAULT NULL,
  `dest_id` char(36) DEFAULT NULL,
  `info` char(20) DEFAULT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '0',
  `time_insert` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`transaction_id`,`branch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `branch_transaction`
--


-- --------------------------------------------------------

--
-- Table structure for table `branch_transaction_status`
--

CREATE TABLE IF NOT EXISTS `branch_transaction_status` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `title` char(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `branch_transaction_status`
--

INSERT INTO `branch_transaction_status` (`id`, `title`) VALUES
(1, 'To be executed'),
(2, 'Executed successfull'),
(3, 'Execution deprecated'),
(4, 'Execution failed');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE IF NOT EXISTS `career` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `career`
--


-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_user` int(11) DEFAULT NULL,
  `to_user` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_chat_to_user` (`to_user`),
  KEY `idx_chat_from_user` (`from_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `chat`
--


-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(40) DEFAULT '',
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `class`
--


-- --------------------------------------------------------

--
-- Table structure for table `class_user`
--

CREATE TABLE IF NOT EXISTS `class_user` (
  `class_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`class_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) NOT NULL,
  `directory` varchar(40) DEFAULT NULL,
  `db_name` varchar(40) DEFAULT NULL,
  `course_language` varchar(20) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `category_code` varchar(40) DEFAULT NULL,
  `visibility` tinyint(4) DEFAULT '0',
  `show_score` int(11) NOT NULL DEFAULT '1',
  `tutor_name` varchar(200) DEFAULT NULL,
  `visual_code` varchar(40) DEFAULT NULL,
  `department_name` varchar(30) DEFAULT NULL,
  `department_url` varchar(180) DEFAULT NULL,
  `disk_quota` bigint(20) unsigned DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `last_edit` datetime DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `target_course_code` varchar(40) DEFAULT NULL,
  `subscribe` tinyint(4) NOT NULL DEFAULT '1',
  `unsubscribe` tinyint(4) NOT NULL DEFAULT '1',
  `registration_code` varchar(255) NOT NULL DEFAULT '',
  `legal` text NOT NULL,
  `activate_legal` int(11) NOT NULL DEFAULT '0',
  `course_type_id` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `idx_course_category_code` (`category_code`),
  KEY `idx_course_directory` (`directory`(10))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `course`
--


-- --------------------------------------------------------

--
-- Table structure for table `course_category`
--

CREATE TABLE IF NOT EXISTS `course_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `code` varchar(40) NOT NULL DEFAULT '',
  `parent_id` varchar(40) DEFAULT NULL,
  `tree_pos` int(10) unsigned DEFAULT NULL,
  `children_count` smallint(6) DEFAULT NULL,
  `auth_course_child` enum('TRUE','FALSE') DEFAULT 'TRUE',
  `auth_cat_child` enum('TRUE','FALSE') DEFAULT 'TRUE',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `parent_id` (`parent_id`),
  KEY `tree_pos` (`tree_pos`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `course_category`
--

INSERT INTO `course_category` (`id`, `name`, `code`, `parent_id`, `tree_pos`, `children_count`, `auth_course_child`, `auth_cat_child`) VALUES
(1, 'Language skills', 'LANG', NULL, 1, 0, 'TRUE', 'TRUE'),
(2, 'PC Skills', 'PC', NULL, 2, 0, 'TRUE', 'TRUE'),
(3, 'Projects', 'PROJ', NULL, 3, 0, 'TRUE', 'TRUE');

-- --------------------------------------------------------

--
-- Table structure for table `course_field`
--

CREATE TABLE IF NOT EXISTS `course_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_type` int(11) NOT NULL DEFAULT '1',
  `field_variable` varchar(64) NOT NULL,
  `field_display_text` varchar(64) DEFAULT NULL,
  `field_default_value` text,
  `field_order` int(11) DEFAULT NULL,
  `field_visible` tinyint(4) DEFAULT '0',
  `field_changeable` tinyint(4) DEFAULT '0',
  `field_filter` tinyint(4) DEFAULT '0',
  `field_loggeable` int(11) DEFAULT '0',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `course_field`
--

INSERT INTO `course_field` (`id`, `field_type`, `field_variable`, `field_display_text`, `field_default_value`, `field_order`, `field_visible`, `field_changeable`, `field_filter`, `field_loggeable`, `tms`) VALUES
(1, 13, 'special_course', 'Special course', 'Yes', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_field_options`
--

CREATE TABLE IF NOT EXISTS `course_field_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `option_value` text,
  `option_display_text` varchar(255) DEFAULT NULL,
  `option_order` int(11) DEFAULT NULL,
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `course_field_options`
--


-- --------------------------------------------------------

--
-- Table structure for table `course_field_values`
--

CREATE TABLE IF NOT EXISTS `course_field_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `course_code` varchar(40) NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` text,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(100) DEFAULT '',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `course_field_values`
--


-- --------------------------------------------------------

--
-- Table structure for table `course_module`
--

CREATE TABLE IF NOT EXISTS `course_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `row_module` int(10) unsigned NOT NULL DEFAULT '0',
  `column_module` int(10) unsigned NOT NULL DEFAULT '0',
  `position` varchar(20) NOT NULL DEFAULT 'basic',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `course_module`
--

INSERT INTO `course_module` (`id`, `name`, `link`, `image`, `row_module`, `column_module`, `position`) VALUES
(1, 'calendar_event', 'calendar/agenda.php', 'agenda.gif', 1, 1, 'basic'),
(2, 'link', 'link/link.php', 'links.gif', 4, 1, 'basic'),
(3, 'document', 'document/document.php', 'documents.gif', 3, 1, 'basic'),
(4, 'student_publication', 'work/work.php', 'works.gif', 3, 2, 'basic'),
(5, 'announcement', 'announcements/announcements.php', 'valves.gif', 2, 1, 'basic'),
(6, 'user', 'user/user.php', 'members.gif', 2, 3, 'basic'),
(7, 'forum', 'forum/index.php', 'forum.gif', 1, 2, 'basic'),
(8, 'quiz', 'exercice/exercice.php', 'quiz.gif', 2, 2, 'basic'),
(9, 'group', 'group/group.php', 'group.gif', 3, 3, 'basic'),
(10, 'course_description', 'course_description/', 'info.gif', 1, 3, 'basic'),
(11, 'chat', 'chat/chat.php', 'chat.gif', 0, 0, 'external'),
(12, 'dropbox', 'dropbox/index.php', 'dropbox.gif', 4, 2, 'basic'),
(13, 'tracking', 'tracking/courseLog.php', 'statistics.gif', 1, 3, 'courseadmin'),
(14, 'homepage_link', 'link/link.php?action=addlink', 'npage.gif', 1, 1, 'courseadmin'),
(15, 'course_setting', 'course_info/infocours.php', 'reference.gif', 1, 1, 'courseadmin'),
(16, 'External', '', 'external.gif', 0, 0, 'external'),
(17, 'AddedLearnpath', '', 'scormbuilder.gif', 0, 0, 'external'),
(18, 'conference', 'conference/index.php?type=conference', 'conf.gif', 0, 0, 'external'),
(19, 'conference', 'conference/index.php?type=classroom', 'conf.gif', 0, 0, 'external'),
(20, 'learnpath', 'newscorm/lp_controller.php', 'scorms.gif', 5, 1, 'basic'),
(21, 'blog', 'blog/blog.php', 'blog.gif', 1, 2, 'basic'),
(22, 'blog_management', 'blog/blog_admin.php', 'blog_admin.gif', 1, 2, 'courseadmin'),
(23, 'course_maintenance', 'course_info/maintenance.php', 'backup.gif', 2, 3, 'courseadmin'),
(24, 'survey', 'survey/survey_list.php', 'survey.gif', 2, 1, 'basic'),
(25, 'wiki', 'wiki/index.php', 'wiki.gif', 2, 3, 'basic'),
(26, 'gradebook', 'gradebook/index.php', 'gradebook.gif', 2, 2, 'basic'),
(27, 'glossary', 'glossary/index.php', 'glossary.gif', 2, 1, 'basic'),
(28, 'notebook', 'notebook/index.php', 'notebook.gif', 2, 1, 'basic'),
(29, 'attendance', 'attendance/index.php', 'attendance.gif', 2, 1, 'basic'),
(30, 'course_progress', 'course_progress/index.php', 'course_progress.gif', 2, 1, 'basic');

-- --------------------------------------------------------

--
-- Table structure for table `course_rel_class`
--

CREATE TABLE IF NOT EXISTS `course_rel_class` (
  `course_code` char(40) NOT NULL,
  `class_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`course_code`,`class_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_rel_class`
--


-- --------------------------------------------------------

--
-- Table structure for table `course_rel_user`
--

CREATE TABLE IF NOT EXISTS `course_rel_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(10) unsigned NOT NULL,
  `course_code` varchar(40) NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '5',
  `role` varchar(60) DEFAULT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `tutor_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  `user_course_cat` int(11) DEFAULT '0',
  `relation_type` int(11) DEFAULT '0',
  `legal_agreement` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `c_id` (`c_id`,`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `course_rel_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `course_request`
--

CREATE TABLE IF NOT EXISTS `course_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `directory` varchar(40) DEFAULT NULL,
  `db_name` varchar(40) DEFAULT NULL,
  `course_language` varchar(20) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `category_code` varchar(40) DEFAULT NULL,
  `tutor_name` varchar(200) DEFAULT NULL,
  `visual_code` varchar(40) DEFAULT NULL,
  `request_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `objetives` text,
  `target_audience` text,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  `info` int(10) unsigned NOT NULL DEFAULT '0',
  `exemplary_content` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `course_request`
--


-- --------------------------------------------------------

--
-- Table structure for table `course_type`
--

CREATE TABLE IF NOT EXISTS `course_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `translation_var` char(40) DEFAULT 'UndefinedCourseTypeLabel',
  `description` text,
  `props` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `course_type`
--

INSERT INTO `course_type` (`id`, `name`, `translation_var`, `description`, `props`) VALUES
(1, 'All tools', 'UndefinedCourseTypeLabel', NULL, NULL),
(2, 'Entry exam', 'UndefinedCourseTypeLabel', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `c_announcement`
--

CREATE TABLE IF NOT EXISTS `c_announcement` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text,
  `content` mediumtext,
  `end_date` date DEFAULT NULL,
  `display_order` mediumint(9) NOT NULL DEFAULT '0',
  `email_sent` tinyint(4) DEFAULT '0',
  `session_id` int(11) DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_announcement`
--

INSERT INTO `c_announcement` (`c_id`, `id`, `title`, `content`, `end_date`, `display_order`, `email_sent`, `session_id`) VALUES
(1, 1, 'This is an announcement example', 'This is an announcement example. Only trainers are allowed to publish announcements.', '2013-05-25', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_announcement_attachment`
--

CREATE TABLE IF NOT EXISTS `c_announcement_attachment` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `comment` text,
  `size` int(11) NOT NULL DEFAULT '0',
  `announcement_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_announcement_attachment`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_attendance`
--

CREATE TABLE IF NOT EXISTS `c_attendance` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `attendance_qualify_title` varchar(255) DEFAULT NULL,
  `attendance_qualify_max` int(11) NOT NULL DEFAULT '0',
  `attendance_weight` float(6,2) NOT NULL DEFAULT '0.00',
  `session_id` int(11) NOT NULL DEFAULT '0',
  `locked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `session_id` (`session_id`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_attendance`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_attendance_calendar`
--

CREATE TABLE IF NOT EXISTS `c_attendance_calendar` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attendance_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `done_attendance` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `attendance_id` (`attendance_id`),
  KEY `done_attendance` (`done_attendance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_attendance_calendar`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_attendance_result`
--

CREATE TABLE IF NOT EXISTS `c_attendance_result` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `attendance_id` int(11) NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `attendance_id` (`attendance_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_attendance_result`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_attendance_sheet`
--

CREATE TABLE IF NOT EXISTS `c_attendance_sheet` (
  `c_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `attendance_calendar_id` int(11) NOT NULL,
  `presence` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`user_id`,`attendance_calendar_id`),
  KEY `presence` (`presence`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_attendance_sheet`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_attendance_sheet_log`
--

CREATE TABLE IF NOT EXISTS `c_attendance_sheet_log` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attendance_id` int(11) NOT NULL DEFAULT '0',
  `lastedit_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastedit_type` varchar(200) NOT NULL,
  `lastedit_user_id` int(11) NOT NULL DEFAULT '0',
  `calendar_date_value` datetime DEFAULT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_attendance_sheet_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_blog`
--

CREATE TABLE IF NOT EXISTS `c_blog` (
  `c_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_name` varchar(250) NOT NULL DEFAULT '',
  `blog_subtitle` varchar(250) DEFAULT NULL,
  `date_creation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `visibility` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `session_id` int(11) DEFAULT '0',
  PRIMARY KEY (`c_id`,`blog_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table with blogs in this course' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_blog`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_blog_attachment`
--

CREATE TABLE IF NOT EXISTS `c_blog_attachment` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL COMMENT 'the real filename',
  `comment` text,
  `size` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL COMMENT 'the user s file name',
  `blog_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_blog_attachment`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_blog_comment`
--

CREATE TABLE IF NOT EXISTS `c_blog_comment` (
  `c_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL DEFAULT '',
  `comment` longtext NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT '0',
  `date_creation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) DEFAULT NULL,
  `parent_comment_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table with comments on posts in a blog' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_blog_comment`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_blog_post`
--

CREATE TABLE IF NOT EXISTS `c_blog_post` (
  `c_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL DEFAULT '',
  `full_text` longtext NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `author_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table with posts / blog.' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_blog_post`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_blog_rating`
--

CREATE TABLE IF NOT EXISTS `c_blog_rating` (
  `c_id` int(11) NOT NULL,
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `rating_type` enum('post','comment') NOT NULL DEFAULT 'post',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`rating_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table with ratings for post/comments in a certain blog' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_blog_rating`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_blog_rel_user`
--

CREATE TABLE IF NOT EXISTS `c_blog_rel_user` (
  `c_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`blog_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table representing users subscribed to a blog';

--
-- Dumping data for table `c_blog_rel_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_blog_task`
--

CREATE TABLE IF NOT EXISTS `c_blog_task` (
  `c_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `color` varchar(10) NOT NULL DEFAULT '',
  `system_task` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table with tasks for a blog' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_blog_task`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_blog_task_rel_user`
--

CREATE TABLE IF NOT EXISTS `c_blog_task_rel_user` (
  `c_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `target_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`c_id`,`blog_id`,`user_id`,`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table with tasks assigned to a user in a blog';

--
-- Dumping data for table `c_blog_task_rel_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_calendar_event`
--

CREATE TABLE IF NOT EXISTS `c_calendar_event` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `parent_event_id` int(11) DEFAULT NULL,
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  `all_day` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_calendar_event`
--

INSERT INTO `c_calendar_event` (`c_id`, `id`, `title`, `content`, `start_date`, `end_date`, `parent_event_id`, `session_id`, `all_day`) VALUES
(1, 1, 'Training creation', 'This training has been created on this moment.', '2013-05-25 00:56:52', '2013-05-25 00:56:52', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_calendar_event_attachment`
--

CREATE TABLE IF NOT EXISTS `c_calendar_event_attachment` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `comment` text,
  `size` int(11) NOT NULL DEFAULT '0',
  `agenda_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_calendar_event_attachment`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_calendar_event_repeat`
--

CREATE TABLE IF NOT EXISTS `c_calendar_event_repeat` (
  `c_id` int(11) NOT NULL,
  `cal_id` int(11) NOT NULL DEFAULT '0',
  `cal_type` varchar(20) DEFAULT NULL,
  `cal_end` int(11) DEFAULT NULL,
  `cal_frequency` int(11) DEFAULT '1',
  `cal_days` char(7) DEFAULT NULL,
  PRIMARY KEY (`c_id`,`cal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_calendar_event_repeat`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_calendar_event_repeat_not`
--

CREATE TABLE IF NOT EXISTS `c_calendar_event_repeat_not` (
  `c_id` int(11) NOT NULL,
  `cal_id` int(11) NOT NULL,
  `cal_date` int(11) NOT NULL,
  PRIMARY KEY (`c_id`,`cal_id`,`cal_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_calendar_event_repeat_not`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_chat_connected`
--

CREATE TABLE IF NOT EXISTS `c_chat_connected` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `last_connection` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `session_id` int(11) NOT NULL DEFAULT '0',
  `to_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`,`user_id`,`last_connection`),
  KEY `char_connected_index` (`user_id`,`session_id`,`to_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_chat_connected`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_course_description`
--

CREATE TABLE IF NOT EXISTS `c_course_description` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `session_id` int(11) DEFAULT '0',
  `description_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `progress` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_course_description`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_course_setting`
--

CREATE TABLE IF NOT EXISTS `c_course_setting` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `variable` varchar(255) NOT NULL DEFAULT '',
  `subkey` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `comment` varchar(255) DEFAULT NULL,
  `subkeytext` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_course_setting`
--

INSERT INTO `c_course_setting` (`c_id`, `id`, `variable`, `subkey`, `type`, `category`, `value`, `title`, `comment`, `subkeytext`) VALUES
(1, 1, 'email_alert_manager_on_new_doc', NULL, NULL, 'work', '0', '', NULL, NULL),
(1, 2, 'email_alert_on_new_doc_dropbox', NULL, NULL, 'dropbox', '0', '', NULL, NULL),
(1, 3, 'allow_user_edit_agenda', NULL, NULL, 'agenda', '0', '', NULL, NULL),
(1, 4, 'allow_user_edit_announcement', NULL, NULL, 'announcement', '0', '', NULL, NULL),
(1, 5, 'email_alert_manager_on_new_quiz', NULL, NULL, 'quiz', '1', '', NULL, NULL),
(1, 6, 'allow_user_image_forum', NULL, NULL, 'forum', '1', '', NULL, NULL),
(1, 7, 'course_theme', NULL, NULL, 'theme', '', '', NULL, NULL),
(1, 8, 'allow_learning_path_theme', NULL, NULL, 'theme', '1', '', NULL, NULL),
(1, 9, 'allow_open_chat_window', NULL, NULL, 'chat', '1', '', NULL, NULL),
(1, 10, 'email_alert_to_teacher_on_new_user_in_course', NULL, NULL, 'registration', '0', '', NULL, NULL),
(1, 11, 'allow_user_view_user_list', NULL, NULL, 'user', '1', '', NULL, NULL),
(1, 12, 'display_info_advance_inside_homecourse', NULL, NULL, 'thematic_advance', '1', '', NULL, NULL),
(1, 13, 'email_alert_students_on_new_homework', NULL, NULL, 'work', '0', '', NULL, NULL),
(1, 14, 'enable_lp_auto_launch', NULL, NULL, 'learning_path', '0', '', NULL, NULL),
(1, 15, 'pdf_export_watermark_text', NULL, NULL, 'learning_path', '', '', NULL, NULL),
(1, 16, 'allow_public_certificates', NULL, NULL, 'certificates', '', '', NULL, NULL),
(1, 17, 'allow_fast_exercise_edition', NULL, NULL, 'exercise', '0', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `c_document`
--

CREATE TABLE IF NOT EXISTS `c_document` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL DEFAULT '',
  `comment` text,
  `title` varchar(255) DEFAULT NULL,
  `filetype` set('file','folder') NOT NULL DEFAULT 'file',
  `size` int(11) NOT NULL DEFAULT '0',
  `readonly` tinyint(3) unsigned NOT NULL,
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_document`
--

INSERT INTO `c_document` (`c_id`, `id`, `path`, `comment`, `title`, `filetype`, `size`, `readonly`, `session_id`) VALUES
(1, 1, '/shared_folder', NULL, 'Folders of users', 'folder', 0, 0, 0),
(1, 2, '/chat_files', NULL, 'Chat conversations history', 'folder', 0, 0, 0),
(1, 3, '/images', NULL, 'Images', 'folder', 0, 0, 0),
(1, 4, '/images/gallery', NULL, 'Gallery', 'folder', 0, 0, 0),
(1, 5, '/audio', NULL, 'Audio', 'folder', 0, 0, 0),
(1, 6, '/flash', NULL, 'Flash', 'folder', 0, 0, 0),
(1, 7, '/video', NULL, 'Video', 'folder', 0, 0, 0),
(1, 8, '/certificates', NULL, 'Certificates', 'folder', 0, 0, 0),
(1, 9, '/images/gallery/diagrams', NULL, 'diagrams', 'folder', 0, 0, 0),
(1, 10, '/images/gallery/diagrams/animated', NULL, 'animated', 'folder', 0, 0, 0),
(1, 11, '/images/gallery/mr_dokeos', NULL, 'mr_dokeos', 'folder', 0, 0, 0),
(1, 12, '/images/gallery/mr_dokeos/animated', NULL, 'animated', 'folder', 0, 0, 0),
(1, 13, '/images/gallery/small', NULL, 'small', 'folder', 0, 0, 0),
(1, 14, '/images/gallery/trainer', NULL, 'trainer', 'folder', 0, 0, 0),
(1, 15, '/images/gallery/board.jpg', NULL, 'board.jpg', 'file', 24054, 0, 0),
(1, 16, '/images/gallery/book.jpg', NULL, 'book.jpg', 'file', 21524, 0, 0),
(1, 17, '/images/gallery/bookcase.jpg', NULL, 'bookcase.jpg', 'file', 26783, 0, 0),
(1, 18, '/images/gallery/book_highlight.jpg', NULL, 'book_highlight.jpg', 'file', 23635, 0, 0),
(1, 19, '/images/gallery/computer.jpg', NULL, 'computer.jpg', 'file', 22319, 0, 0),
(1, 20, '/images/gallery/diagrams/accident_1.png', NULL, 'accident_1.png', 'file', 151329, 0, 0),
(1, 21, '/images/gallery/diagrams/alaska_chart.png', NULL, 'alaska_chart.png', 'file', 35312, 0, 0),
(1, 22, '/images/gallery/diagrams/animated/anim_corriolis.gif', NULL, 'anim_corriolis.gif', 'file', 151742, 0, 0),
(1, 23, '/images/gallery/diagrams/animated/anim_electrolysis.gif', NULL, 'anim_electrolysis.gif', 'file', 49668, 0, 0),
(1, 24, '/images/gallery/diagrams/animated/anim_japanese.gif', NULL, 'anim_japanese.gif', 'file', 11892, 0, 0),
(1, 25, '/images/gallery/diagrams/animated/anim_loco.gif', NULL, 'anim_loco.gif', 'file', 87422, 0, 0),
(1, 26, '/images/gallery/diagrams/animated/anim_pendul.gif', NULL, 'anim_pendul.gif', 'file', 49559, 0, 0),
(1, 27, '/images/gallery/diagrams/animated/anim_rome.gif', NULL, 'anim_rome.gif', 'file', 401677, 0, 0),
(1, 28, '/images/gallery/diagrams/animated/anim_twostroke.gif', NULL, 'anim_twostroke.gif', 'file', 209400, 0, 0),
(1, 29, '/images/gallery/diagrams/animated/anim_wave_frequency.gif', NULL, 'anim_wave_frequency.gif', 'file', 97141, 0, 0),
(1, 30, '/images/gallery/diagrams/animated/anim_yugoslavia.gif', NULL, 'anim_yugoslavia.gif', 'file', 44252, 0, 0),
(1, 31, '/images/gallery/diagrams/argandgaussplane.jpg', NULL, 'argandgaussplane.jpg', 'file', 7754, 0, 0),
(1, 32, '/images/gallery/diagrams/asus-eee-pc.jpg', NULL, 'asus-eee-pc.jpg', 'file', 21133, 0, 0),
(1, 33, '/images/gallery/diagrams/bandgap_dv.jpg', NULL, 'bandgap_dv.jpg', 'file', 11121, 0, 0),
(1, 34, '/images/gallery/diagrams/barbitursyra.jpg', NULL, 'barbitursyra.jpg', 'file', 6811, 0, 0),
(1, 35, '/images/gallery/diagrams/bottom_arrow.png', NULL, 'bottom_arrow.png', 'file', 2841, 0, 0),
(1, 36, '/images/gallery/diagrams/brain.png', NULL, 'brain.png', 'file', 103017, 0, 0),
(1, 37, '/images/gallery/diagrams/chainette_formule.jpg', NULL, 'chainette_formule.jpg', 'file', 14880, 0, 0),
(1, 38, '/images/gallery/diagrams/coccidioides.jpg', NULL, 'coccidioides.jpg', 'file', 27833, 0, 0),
(1, 39, '/images/gallery/diagrams/constitution_1.png', NULL, 'constitution_1.png', 'file', 50616, 0, 0),
(1, 40, '/images/gallery/diagrams/distance.png', NULL, 'distance.png', 'file', 2929, 0, 0),
(1, 41, '/images/gallery/diagrams/dokeos_stones.png', NULL, 'dokeos_stones.png', 'file', 95264, 0, 0),
(1, 42, '/images/gallery/diagrams/dokeos_stones_external.png', NULL, 'dokeos_stones_external.png', 'file', 218411, 0, 0),
(1, 43, '/images/gallery/diagrams/elearning_project.png', NULL, 'elearning_project.png', 'file', 37981, 0, 0),
(1, 44, '/images/gallery/diagrams/europecouncilorange200_1.png', NULL, 'europecouncilorange200_1.png', 'file', 88995, 0, 0),
(1, 45, '/images/gallery/diagrams/europemap.jpg', NULL, 'europemap.jpg', 'file', 61208, 0, 0),
(1, 46, '/images/gallery/diagrams/gearbox.jpg', NULL, 'gearbox.jpg', 'file', 31176, 0, 0),
(1, 47, '/images/gallery/diagrams/halleffect.jpg', NULL, 'halleffect.jpg', 'file', 49877, 0, 0),
(1, 48, '/images/gallery/diagrams/head_olfactory_nerve.png', NULL, 'head_olfactory_nerve.png', 'file', 182785, 0, 0),
(1, 49, '/images/gallery/diagrams/lightbulb.jpg', NULL, 'lightbulb.jpg', 'file', 24030, 0, 0),
(1, 50, '/images/gallery/diagrams/matching_electric_1.png', NULL, 'matching_electric_1.png', 'file', 11174, 0, 0),
(1, 51, '/images/gallery/diagrams/oilwell.jpg', NULL, 'oilwell.jpg', 'file', 23782, 0, 0),
(1, 52, '/images/gallery/diagrams/pc.jpg', NULL, 'pc.jpg', 'file', 13711, 0, 0),
(1, 53, '/images/gallery/diagrams/piano.jpg', NULL, 'piano.jpg', 'file', 26538, 0, 0),
(1, 54, '/images/gallery/diagrams/precession.jpg', NULL, 'precession.jpg', 'file', 8319, 0, 0),
(1, 55, '/images/gallery/diagrams/pythagore.jpg', NULL, 'pythagore.jpg', 'file', 6308, 0, 0),
(1, 56, '/images/gallery/diagrams/radiograph.png', NULL, 'radiograph.png', 'file', 46503, 0, 0),
(1, 57, '/images/gallery/diagrams/receiver.jpg', NULL, 'receiver.jpg', 'file', 4745, 0, 0),
(1, 58, '/images/gallery/diagrams/spectre.jpg', NULL, 'spectre.jpg', 'file', 27391, 0, 0),
(1, 59, '/images/gallery/diagrams/synapse.jpg', NULL, 'synapse.jpg', 'file', 17330, 0, 0),
(1, 60, '/images/gallery/diagrams/tetralogy.png', NULL, 'tetralogy.png', 'file', 75038, 0, 0),
(1, 61, '/images/gallery/diagrams/top_arrow.png', NULL, 'top_arrow.png', 'file', 2806, 0, 0),
(1, 62, '/images/gallery/diagrams/velocity.jpg', NULL, 'velocity.jpg', 'file', 6536, 0, 0),
(1, 63, '/images/gallery/diagrams/waterloo.png', NULL, 'waterloo.png', 'file', 41939, 0, 0),
(1, 64, '/images/gallery/diagrams/yalta_1.png', NULL, 'yalta_1.png', 'file', 31706, 0, 0),
(1, 65, '/images/gallery/emot_happy.jpg', NULL, 'emot_happy.jpg', 'file', 21933, 0, 0),
(1, 66, '/images/gallery/emot_neutral.jpg', NULL, 'emot_neutral.jpg', 'file', 20766, 0, 0),
(1, 67, '/images/gallery/emot_sad.jpg', NULL, 'emot_sad.jpg', 'file', 21522, 0, 0),
(1, 68, '/images/gallery/emot_wink.jpg', NULL, 'emot_wink.jpg', 'file', 21552, 0, 0),
(1, 69, '/images/gallery/female.jpg', NULL, 'female.jpg', 'file', 20454, 0, 0),
(1, 70, '/images/gallery/geometry.jpg', NULL, 'geometry.jpg', 'file', 27041, 0, 0),
(1, 71, '/images/gallery/homework.jpg', NULL, 'homework.jpg', 'file', 24694, 0, 0),
(1, 72, '/images/gallery/idea.jpg', NULL, 'idea.jpg', 'file', 20308, 0, 0),
(1, 73, '/images/gallery/interaction.jpg', NULL, 'interaction.jpg', 'file', 22036, 0, 0),
(1, 74, '/images/gallery/logo_chamilo.png', NULL, 'logo_chamilo.png', 'file', 2986, 0, 0),
(1, 75, '/images/gallery/logo_dokeos.png', NULL, 'logo_dokeos.png', 'file', 2986, 0, 0),
(1, 76, '/images/gallery/male.jpg', NULL, 'male.jpg', 'file', 19132, 0, 0),
(1, 77, '/images/gallery/maths.jpg', NULL, 'maths.jpg', 'file', 21865, 0, 0),
(1, 78, '/images/gallery/mechanism.jpg', NULL, 'mechanism.jpg', 'file', 24639, 0, 0),
(1, 79, '/images/gallery/mouse.jpg', NULL, 'mouse.jpg', 'file', 20171, 0, 0),
(1, 80, '/images/gallery/mr_dokeos/animated/creativeAnim.gif', NULL, 'creativeAnim.gif', 'file', 15656, 0, 0),
(1, 81, '/images/gallery/mr_dokeos/animated/pointerAnim.gif', NULL, 'pointerAnim.gif', 'file', 24349, 0, 0),
(1, 82, '/images/gallery/mr_dokeos/animated/practicerAnim.gif', NULL, 'practicerAnim.gif', 'file', 22164, 0, 0),
(1, 83, '/images/gallery/mr_dokeos/animated/readerAnim.gif', NULL, 'readerAnim.gif', 'file', 15393, 0, 0),
(1, 84, '/images/gallery/mr_dokeos/animated/teacherAnim.gif', NULL, 'teacherAnim.gif', 'file', 20693, 0, 0),
(1, 85, '/images/gallery/mr_dokeos/animated/thinkerAnim.gif', NULL, 'thinkerAnim.gif', 'file', 32847, 0, 0),
(1, 86, '/images/gallery/mr_dokeos/anim_pointing_left.jpg', NULL, 'anim_pointing_left.jpg', 'file', 19489, 0, 0),
(1, 87, '/images/gallery/mr_dokeos/anim_pointing_right.jpg', NULL, 'anim_pointing_right.jpg', 'file', 29795, 0, 0),
(1, 88, '/images/gallery/mr_dokeos/anim_practicing.jpg', NULL, 'anim_practicing.jpg', 'file', 38355, 0, 0),
(1, 89, '/images/gallery/mr_dokeos/anim_reading.jpg', NULL, 'anim_reading.jpg', 'file', 33330, 0, 0),
(1, 90, '/images/gallery/mr_dokeos/anim_teaching.jpg', NULL, 'anim_teaching.jpg', 'file', 34214, 0, 0),
(1, 91, '/images/gallery/mr_dokeos/anim_thinking.jpg', NULL, 'anim_thinking.jpg', 'file', 28710, 0, 0),
(1, 92, '/images/gallery/mr_dokeos/anim_writing.jpg', NULL, 'anim_writing.jpg', 'file', 32445, 0, 0),
(1, 93, '/images/gallery/mr_dokeos/collaborating.jpg', NULL, 'collaborating.jpg', 'file', 20052, 0, 0),
(1, 94, '/images/gallery/mr_dokeos/collaborative.png', NULL, 'collaborative.png', 'file', 35703, 0, 0),
(1, 95, '/images/gallery/mr_dokeos/collaborative_big.png', NULL, 'collaborative_big.png', 'file', 32541, 0, 0),
(1, 96, '/images/gallery/mr_dokeos/group.jpg', NULL, 'group.jpg', 'file', 20481, 0, 0),
(1, 97, '/images/gallery/mr_dokeos/mr_dokeosleft.png', NULL, 'mr_dokeosleft.png', 'file', 6858, 0, 0),
(1, 98, '/images/gallery/mr_dokeos/pointing_left.jpg', NULL, 'pointing_left.jpg', 'file', 18391, 0, 0),
(1, 99, '/images/gallery/mr_dokeos/pointing_right.jpg', NULL, 'pointing_right.jpg', 'file', 18574, 0, 0),
(1, 100, '/images/gallery/mr_dokeos/practicing.jpg', NULL, 'practicing.jpg', 'file', 20816, 0, 0),
(1, 101, '/images/gallery/mr_dokeos/reading.jpg', NULL, 'reading.jpg', 'file', 19006, 0, 0),
(1, 102, '/images/gallery/mr_dokeos/teaching.jpg', NULL, 'teaching.jpg', 'file', 19512, 0, 0),
(1, 103, '/images/gallery/mr_dokeos/thinking.jpg', NULL, 'thinking.jpg', 'file', 18384, 0, 0),
(1, 104, '/images/gallery/mr_dokeos/writing.jpg', NULL, 'writing.jpg', 'file', 18580, 0, 0),
(1, 105, '/images/gallery/newspaper.jpg', NULL, 'newspaper.jpg', 'file', 21935, 0, 0),
(1, 106, '/images/gallery/note.jpg', NULL, 'note.jpg', 'file', 10948, 0, 0),
(1, 107, '/images/gallery/pencil.png', NULL, 'pencil.png', 'file', 4794, 0, 0),
(1, 108, '/images/gallery/presentation.jpg', NULL, 'presentation.jpg', 'file', 20445, 0, 0),
(1, 109, '/images/gallery/redlight.jpg', NULL, 'redlight.jpg', 'file', 20374, 0, 0),
(1, 110, '/images/gallery/science.jpg', NULL, 'science.jpg', 'file', 24035, 0, 0),
(1, 111, '/images/gallery/servicesgather.png', NULL, 'servicesgather.png', 'file', 19575, 0, 0),
(1, 112, '/images/gallery/silhouette.png', NULL, 'silhouette.png', 'file', 4358, 0, 0),
(1, 113, '/images/gallery/small/00.png', NULL, '00.png', 'file', 6285, 0, 0),
(1, 114, '/images/gallery/small/01.png', NULL, '01.png', 'file', 6112, 0, 0),
(1, 115, '/images/gallery/small/02.png', NULL, '02.png', 'file', 6260, 0, 0),
(1, 116, '/images/gallery/small/03.png', NULL, '03.png', 'file', 6324, 0, 0),
(1, 117, '/images/gallery/small/04.png', NULL, '04.png', 'file', 6147, 0, 0),
(1, 118, '/images/gallery/small/05.png', NULL, '05.png', 'file', 6201, 0, 0),
(1, 119, '/images/gallery/small/06.png', NULL, '06.png', 'file', 6296, 0, 0),
(1, 120, '/images/gallery/small/07.png', NULL, '07.png', 'file', 6130, 0, 0),
(1, 121, '/images/gallery/small/08.png', NULL, '08.png', 'file', 6351, 0, 0),
(1, 122, '/images/gallery/small/09.png', NULL, '09.png', 'file', 6343, 0, 0),
(1, 123, '/images/gallery/small/accessibilty.jpg', NULL, 'accessibilty.jpg', 'file', 13859, 0, 0),
(1, 124, '/images/gallery/small/agenda.jpg', NULL, 'agenda.jpg', 'file', 14108, 0, 0),
(1, 125, '/images/gallery/small/annoncement.jpg', NULL, 'annoncement.jpg', 'file', 13770, 0, 0),
(1, 126, '/images/gallery/small/arrow.png', NULL, 'arrow.png', 'file', 1441, 0, 0),
(1, 127, '/images/gallery/small/attach.jpg', NULL, 'attach.jpg', 'file', 14049, 0, 0),
(1, 128, '/images/gallery/small/board.jpg', NULL, 'board.jpg', 'file', 13913, 0, 0),
(1, 129, '/images/gallery/small/bookcase.jpg', NULL, 'bookcase.jpg', 'file', 14038, 0, 0),
(1, 130, '/images/gallery/small/button_cancel.jpg', NULL, 'button_cancel.jpg', 'file', 14156, 0, 0),
(1, 131, '/images/gallery/small/button_ok.jpg', NULL, 'button_ok.jpg', 'file', 13817, 0, 0),
(1, 132, '/images/gallery/small/chart.jpg', NULL, 'chart.jpg', 'file', 14129, 0, 0),
(1, 133, '/images/gallery/small/collaboration.jpg', NULL, 'collaboration.jpg', 'file', 13812, 0, 0),
(1, 134, '/images/gallery/small/computer.jpg', NULL, 'computer.jpg', 'file', 13673, 0, 0),
(1, 135, '/images/gallery/small/down.jpg', NULL, 'down.jpg', 'file', 13862, 0, 0),
(1, 136, '/images/gallery/small/email.jpg', NULL, 'email.jpg', 'file', 13682, 0, 0),
(1, 137, '/images/gallery/small/emot_happy.jpg', NULL, 'emot_happy.jpg', 'file', 14110, 0, 0),
(1, 138, '/images/gallery/small/emot_neutral.jpg', NULL, 'emot_neutral.jpg', 'file', 13672, 0, 0),
(1, 139, '/images/gallery/small/emot_sad.jpg', NULL, 'emot_sad.jpg', 'file', 14085, 0, 0),
(1, 140, '/images/gallery/small/emot_wink.jpg', NULL, 'emot_wink.jpg', 'file', 14074, 0, 0),
(1, 141, '/images/gallery/small/fish.jpg', NULL, 'fish.jpg', 'file', 13768, 0, 0),
(1, 142, '/images/gallery/small/group.jpg', NULL, 'group.jpg', 'file', 13894, 0, 0),
(1, 143, '/images/gallery/small/help.jpg', NULL, 'help.jpg', 'file', 14025, 0, 0),
(1, 144, '/images/gallery/small/important.jpg', NULL, 'important.jpg', 'file', 13929, 0, 0),
(1, 145, '/images/gallery/small/left.jpg', NULL, 'left.jpg', 'file', 13974, 0, 0),
(1, 146, '/images/gallery/small/listen.jpg', NULL, 'listen.jpg', 'file', 13727, 0, 0),
(1, 147, '/images/gallery/small/maths.jpg', NULL, 'maths.jpg', 'file', 13756, 0, 0),
(1, 148, '/images/gallery/small/mime_access.jpg', NULL, 'mime_access.jpg', 'file', 13681, 0, 0),
(1, 149, '/images/gallery/small/mime_audio.jpg', NULL, 'mime_audio.jpg', 'file', 13709, 0, 0),
(1, 150, '/images/gallery/small/mime_excel.jpg', NULL, 'mime_excel.jpg', 'file', 13595, 0, 0),
(1, 151, '/images/gallery/small/mime_flash.jpg', NULL, 'mime_flash.jpg', 'file', 13867, 0, 0),
(1, 152, '/images/gallery/small/mime_movie.jpg', NULL, 'mime_movie.jpg', 'file', 13628, 0, 0),
(1, 153, '/images/gallery/small/mime_music.jpg', NULL, 'mime_music.jpg', 'file', 13763, 0, 0),
(1, 154, '/images/gallery/small/mime_pdf.jpg', NULL, 'mime_pdf.jpg', 'file', 14044, 0, 0),
(1, 155, '/images/gallery/small/mime_ppt.jpg', NULL, 'mime_ppt.jpg', 'file', 13590, 0, 0),
(1, 156, '/images/gallery/small/mime_publisher.jpg', NULL, 'mime_publisher.jpg', 'file', 13665, 0, 0),
(1, 157, '/images/gallery/small/mime_visio.jpg', NULL, 'mime_visio.jpg', 'file', 13749, 0, 0),
(1, 158, '/images/gallery/small/mime_word.jpg', NULL, 'mime_word.jpg', 'file', 13640, 0, 0),
(1, 159, '/images/gallery/small/mime_zip.jpg', NULL, 'mime_zip.jpg', 'file', 13766, 0, 0),
(1, 160, '/images/gallery/small/mouse.jpg', NULL, 'mouse.jpg', 'file', 13662, 0, 0),
(1, 161, '/images/gallery/small/quicktime.jpg', NULL, 'quicktime.jpg', 'file', 13806, 0, 0),
(1, 162, '/images/gallery/small/redlight.jpg', NULL, 'redlight.jpg', 'file', 13505, 0, 0),
(1, 163, '/images/gallery/small/right.jpg', NULL, 'right.jpg', 'file', 13903, 0, 0),
(1, 164, '/images/gallery/small/save.jpg', NULL, 'save.jpg', 'file', 13955, 0, 0),
(1, 165, '/images/gallery/small/science.jpg', NULL, 'science.jpg', 'file', 13851, 0, 0),
(1, 166, '/images/gallery/small/search.jpg', NULL, 'search.jpg', 'file', 13473, 0, 0),
(1, 167, '/images/gallery/small/speak.jpg', NULL, 'speak.jpg', 'file', 13520, 0, 0),
(1, 168, '/images/gallery/small/talking.jpg', NULL, 'talking.jpg', 'file', 13812, 0, 0),
(1, 169, '/images/gallery/small/teacher.jpg', NULL, 'teacher.jpg', 'file', 13879, 0, 0),
(1, 170, '/images/gallery/small/teacher_male.jpg', NULL, 'teacher_male.jpg', 'file', 13660, 0, 0),
(1, 171, '/images/gallery/small/tutorial.jpg', NULL, 'tutorial.jpg', 'file', 13560, 0, 0),
(1, 172, '/images/gallery/small/up.jpg', NULL, 'up.jpg', 'file', 13961, 0, 0),
(1, 173, '/images/gallery/small/videoconference.jpg', NULL, 'videoconference.jpg', 'file', 13700, 0, 0),
(1, 174, '/images/gallery/small/work.jpg', NULL, 'work.jpg', 'file', 13827, 0, 0),
(1, 175, '/images/gallery/speech.jpg', NULL, 'speech.jpg', 'file', 22652, 0, 0),
(1, 176, '/images/gallery/time.jpg', NULL, 'time.jpg', 'file', 26028, 0, 0),
(1, 177, '/images/gallery/trainer/trainer_case.png', NULL, 'trainer_case.png', 'file', 33094, 0, 0),
(1, 178, '/images/gallery/trainer/trainer_chair.png', NULL, 'trainer_chair.png', 'file', 53450, 0, 0),
(1, 179, '/images/gallery/trainer/trainer_face.png', NULL, 'trainer_face.png', 'file', 38924, 0, 0),
(1, 180, '/images/gallery/trainer/trainer_glasses.png', NULL, 'trainer_glasses.png', 'file', 45939, 0, 0),
(1, 181, '/images/gallery/trainer/trainer_join_hands.png', NULL, 'trainer_join_hands.png', 'file', 37865, 0, 0),
(1, 182, '/images/gallery/trainer/trainer_join_left.png', NULL, 'trainer_join_left.png', 'file', 37600, 0, 0),
(1, 183, '/images/gallery/trainer/trainer_points_left.png', NULL, 'trainer_points_left.png', 'file', 27068, 0, 0),
(1, 184, '/images/gallery/trainer/trainer_points_right.png', NULL, 'trainer_points_right.png', 'file', 26535, 0, 0),
(1, 185, '/images/gallery/trainer/trainer_reads.png', NULL, 'trainer_reads.png', 'file', 30209, 0, 0),
(1, 186, '/images/gallery/trainer/trainer_smile.png', NULL, 'trainer_smile.png', 'file', 16683, 0, 0),
(1, 187, '/images/gallery/trainer/trainer_standing.png', NULL, 'trainer_standing.png', 'file', 31892, 0, 0),
(1, 188, '/images/gallery/trainer/trainer_staring.png', NULL, 'trainer_staring.png', 'file', 22486, 0, 0),
(1, 189, '/images/gallery/tutorial.jpg', NULL, 'tutorial.jpg', 'file', 18883, 0, 0),
(1, 190, '/images/gallery/twopeople.png', NULL, 'twopeople.png', 'file', 10453, 0, 0),
(1, 191, '/images/gallery/world.jpg', NULL, 'world.jpg', 'file', 22705, 0, 0),
(1, 192, '/images/gallery/write.jpg', NULL, 'write.jpg', 'file', 19862, 0, 0),
(1, 193, '/audio/ListeningComprehension.mp3', NULL, 'ListeningComprehension.mp3', 'file', 147854, 0, 0),
(1, 194, '/flash/ArtefactsInRMI.swf', NULL, 'ArtefactsInRMI.swf', 'file', 100988, 0, 0),
(1, 195, '/flash/PonderationOfMrSignal.swf', NULL, 'PonderationOfMrSignal.swf', 'file', 18825, 0, 0),
(1, 196, '/flash/SpinEchoSequence.swf', NULL, 'SpinEchoSequence.swf', 'file', 17904, 0, 0),
(1, 197, '/video/flv', NULL, 'flv', 'folder', 0, 0, 0),
(1, 198, '/video/flv/example.flv', NULL, 'example.flv', 'file', 1093260, 0, 0),
(1, 199, '/video/painting.mpg', NULL, 'painting.mpg', 'file', 2353702, 0, 0),
(1, 200, '/certificates/default.html', NULL, 'default.html', 'file', 2157, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_dropbox_category`
--

CREATE TABLE IF NOT EXISTS `c_dropbox_category` (
  `c_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` text NOT NULL,
  `received` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`cat_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_dropbox_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_dropbox_feedback`
--

CREATE TABLE IF NOT EXISTS `c_dropbox_feedback` (
  `c_id` int(11) NOT NULL,
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL DEFAULT '0',
  `author_user_id` int(11) NOT NULL DEFAULT '0',
  `feedback` text NOT NULL,
  `feedback_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`c_id`,`feedback_id`),
  KEY `file_id` (`file_id`),
  KEY `author_user_id` (`author_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_dropbox_feedback`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_dropbox_file`
--

CREATE TABLE IF NOT EXISTS `c_dropbox_file` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uploader_id` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL,
  `title` varchar(250) DEFAULT '',
  `description` varchar(250) DEFAULT '',
  `author` varchar(250) DEFAULT '',
  `upload_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_upload_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `session_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`c_id`,`id`),
  UNIQUE KEY `UN_filename` (`filename`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_dropbox_file`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_dropbox_person`
--

CREATE TABLE IF NOT EXISTS `c_dropbox_person` (
  `c_id` int(11) NOT NULL,
  `file_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`file_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_dropbox_person`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_dropbox_post`
--

CREATE TABLE IF NOT EXISTS `c_dropbox_post` (
  `c_id` int(11) NOT NULL,
  `file_id` int(10) unsigned NOT NULL,
  `dest_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `feedback_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `feedback` text,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `session_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`c_id`,`file_id`,`dest_user_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_dropbox_post`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_forum_attachment`
--

CREATE TABLE IF NOT EXISTS `c_forum_attachment` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `comment` text,
  `size` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_attachment`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_forum_category`
--

CREATE TABLE IF NOT EXISTS `c_forum_category` (
  `c_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_title` varchar(255) NOT NULL DEFAULT '',
  `cat_comment` text,
  `cat_order` int(11) NOT NULL DEFAULT '0',
  `locked` int(11) NOT NULL DEFAULT '0',
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`cat_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_category`
--

INSERT INTO `c_forum_category` (`c_id`, `cat_id`, `cat_title`, `cat_comment`, `cat_order`, `locked`, `session_id`) VALUES
(1, 1, 'Example Forum Category', '', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_forum_forum`
--

CREATE TABLE IF NOT EXISTS `c_forum_forum` (
  `c_id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_title` varchar(255) NOT NULL DEFAULT '',
  `forum_comment` text,
  `forum_threads` int(11) DEFAULT '0',
  `forum_posts` int(11) DEFAULT '0',
  `forum_last_post` int(11) DEFAULT '0',
  `forum_category` int(11) DEFAULT NULL,
  `allow_anonymous` int(11) DEFAULT NULL,
  `allow_edit` int(11) DEFAULT NULL,
  `approval_direct_post` varchar(20) DEFAULT NULL,
  `allow_attachments` int(11) DEFAULT NULL,
  `allow_new_threads` int(11) DEFAULT NULL,
  `default_view` varchar(20) DEFAULT NULL,
  `forum_of_group` varchar(20) DEFAULT NULL,
  `forum_group_public_private` varchar(20) DEFAULT 'public',
  `forum_order` int(11) DEFAULT NULL,
  `locked` int(11) NOT NULL DEFAULT '0',
  `session_id` int(11) NOT NULL DEFAULT '0',
  `forum_image` varchar(255) NOT NULL DEFAULT '',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`c_id`,`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_forum`
--

INSERT INTO `c_forum_forum` (`c_id`, `forum_id`, `forum_title`, `forum_comment`, `forum_threads`, `forum_posts`, `forum_last_post`, `forum_category`, `allow_anonymous`, `allow_edit`, `approval_direct_post`, `allow_attachments`, `allow_new_threads`, `default_view`, `forum_of_group`, `forum_group_public_private`, `forum_order`, `locked`, `session_id`, `forum_image`, `start_time`, `end_time`) VALUES
(1, 1, 'Example Forum', '', 0, 0, 0, 1, 0, 1, NULL, 0, 1, 'flat', '0', 'public', 1, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `c_forum_mailcue`
--

CREATE TABLE IF NOT EXISTS `c_forum_mailcue` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`c_id`,`thread_id`,`user_id`,`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_mailcue`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_forum_notification`
--

CREATE TABLE IF NOT EXISTS `c_forum_notification` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `forum_id` int(11) NOT NULL DEFAULT '0',
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`c_id`,`user_id`,`forum_id`,`thread_id`,`post_id`),
  KEY `user_id` (`user_id`),
  KEY `forum_id` (`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_notification`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_forum_post`
--

CREATE TABLE IF NOT EXISTS `c_forum_post` (
  `c_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(250) DEFAULT NULL,
  `post_text` text,
  `thread_id` int(11) DEFAULT '0',
  `forum_id` int(11) DEFAULT '0',
  `poster_id` int(11) DEFAULT '0',
  `poster_name` varchar(100) DEFAULT '',
  `post_date` datetime DEFAULT '0000-00-00 00:00:00',
  `post_notification` tinyint(4) DEFAULT '0',
  `post_parent_id` int(11) DEFAULT '0',
  `visible` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`c_id`,`post_id`),
  KEY `poster_id` (`poster_id`),
  KEY `forum_id` (`forum_id`),
  KEY `idx_forum_post_thread_id` (`thread_id`),
  KEY `idx_forum_post_visible` (`visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_post`
--

INSERT INTO `c_forum_post` (`c_id`, `post_id`, `post_title`, `post_text`, `thread_id`, `forum_id`, `poster_id`, `poster_name`, `post_date`, `post_notification`, `post_parent_id`, `visible`) VALUES
(1, 1, 'Example Thread', 'Example content', 1, 1, 1, '', '2013-05-25 00:56:52', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_forum_thread`
--

CREATE TABLE IF NOT EXISTS `c_forum_thread` (
  `c_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_title` varchar(255) DEFAULT NULL,
  `forum_id` int(11) DEFAULT NULL,
  `thread_replies` int(11) DEFAULT '0',
  `thread_poster_id` int(11) DEFAULT NULL,
  `thread_poster_name` varchar(100) DEFAULT '',
  `thread_views` int(11) DEFAULT '0',
  `thread_last_post` int(11) DEFAULT NULL,
  `thread_date` datetime DEFAULT '0000-00-00 00:00:00',
  `thread_sticky` tinyint(3) unsigned DEFAULT '0',
  `locked` int(11) NOT NULL DEFAULT '0',
  `session_id` int(10) unsigned DEFAULT NULL,
  `thread_title_qualify` varchar(255) DEFAULT '',
  `thread_qualify_max` float(6,2) unsigned NOT NULL DEFAULT '0.00',
  `thread_close_date` datetime DEFAULT '0000-00-00 00:00:00',
  `thread_weight` float(6,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`c_id`,`thread_id`),
  KEY `idx_forum_thread_forum_id` (`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_thread`
--

INSERT INTO `c_forum_thread` (`c_id`, `thread_id`, `thread_title`, `forum_id`, `thread_replies`, `thread_poster_id`, `thread_poster_name`, `thread_views`, `thread_last_post`, `thread_date`, `thread_sticky`, `locked`, `session_id`, `thread_title_qualify`, `thread_qualify_max`, `thread_close_date`, `thread_weight`) VALUES
(1, 1, 'Example Thread', 1, 0, 1, '', 0, 1, '2013-05-25 00:56:52', 0, 0, 0, '', 10.00, '0000-00-00 00:00:00', 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `c_forum_thread_qualify`
--

CREATE TABLE IF NOT EXISTS `c_forum_thread_qualify` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `thread_id` int(11) NOT NULL,
  `qualify` float(6,2) NOT NULL DEFAULT '0.00',
  `qualify_user_id` int(11) DEFAULT NULL,
  `qualify_time` datetime DEFAULT '0000-00-00 00:00:00',
  `session_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`c_id`,`id`),
  KEY `user_id` (`user_id`,`thread_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_thread_qualify`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_forum_thread_qualify_log`
--

CREATE TABLE IF NOT EXISTS `c_forum_thread_qualify_log` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `thread_id` int(11) NOT NULL,
  `qualify` float(6,2) NOT NULL DEFAULT '0.00',
  `qualify_user_id` int(11) DEFAULT NULL,
  `qualify_time` datetime DEFAULT '0000-00-00 00:00:00',
  `session_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`c_id`,`id`),
  KEY `user_id` (`user_id`,`thread_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_forum_thread_qualify_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_glossary`
--

CREATE TABLE IF NOT EXISTS `c_glossary` (
  `c_id` int(11) NOT NULL,
  `glossary_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT '0',
  PRIMARY KEY (`c_id`,`glossary_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_glossary`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_group_category`
--

CREATE TABLE IF NOT EXISTS `c_group_category` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `doc_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `calendar_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `work_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `announcements_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `forum_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `wiki_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `chat_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `max_student` int(10) unsigned NOT NULL DEFAULT '8',
  `self_reg_allowed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `self_unreg_allowed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `groups_per_user` int(10) unsigned NOT NULL DEFAULT '0',
  `display_order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_group_category`
--

INSERT INTO `c_group_category` (`c_id`, `id`, `title`, `description`, `doc_state`, `calendar_state`, `work_state`, `announcements_state`, `forum_state`, `wiki_state`, `chat_state`, `max_student`, `self_reg_allowed`, `self_unreg_allowed`, `groups_per_user`, `display_order`) VALUES
(1, 2, 'Default groups', '', 1, 1, 1, 1, 1, 1, 1, 8, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_group_info`
--

CREATE TABLE IF NOT EXISTS `c_group_info` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `max_student` int(10) unsigned NOT NULL DEFAULT '8',
  `doc_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `calendar_state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `work_state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `announcements_state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `forum_state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `wiki_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `chat_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `secret_directory` varchar(255) DEFAULT NULL,
  `self_registration_allowed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `self_unregistration_allowed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `session_id` int(11) NOT NULL,
  `iid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`iid`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_group_info`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_group_rel_tutor`
--

CREATE TABLE IF NOT EXISTS `c_group_rel_tutor` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_group_rel_tutor`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_group_rel_user`
--

CREATE TABLE IF NOT EXISTS `c_group_rel_user` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `role` char(50) NOT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_group_rel_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_item_property`
--

CREATE TABLE IF NOT EXISTS `c_item_property` (
  `c_id` int(11) NOT NULL,
  `tool` varchar(100) NOT NULL DEFAULT '',
  `insert_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastedit_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ref` int(11) NOT NULL DEFAULT '0',
  `lastedit_type` varchar(100) NOT NULL DEFAULT '',
  `lastedit_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `to_group_id` int(10) unsigned DEFAULT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `visibility` tinyint(4) NOT NULL DEFAULT '1',
  `start_visible` datetime DEFAULT NULL,
  `end_visible` datetime DEFAULT NULL,
  `id_session` int(11) DEFAULT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `idx_item_property_toolref` (`tool`,`ref`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=208 ;

--
-- Dumping data for table `c_item_property`
--

INSERT INTO `c_item_property` (`c_id`, `tool`, `insert_user_id`, `insert_date`, `lastedit_date`, `ref`, `lastedit_type`, `lastedit_user_id`, `to_group_id`, `to_user_id`, `visibility`, `start_visible`, `end_visible`, `id_session`, `id`) VALUES
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 1, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 1),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 2, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 2),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 3, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 3),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 4, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 4),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 5, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 5),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 6, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 6),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 7, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 7),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 8, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 8),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 9, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 9),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 10, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 10),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 11, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 11),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 12, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 12),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 13, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 13),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 14, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 14),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 15, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 15),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 16, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 16),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 17, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 17),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 18, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 18),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 19, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 19),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 20, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 20),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 21, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 21),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 22, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 22),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 23, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 23),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 24, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 24),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 25, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 25),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 26, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 26),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 27, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 27),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 28, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 28),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 29, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 29),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 30, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 30),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 31, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 31),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 32, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 32),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 33, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 33),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 34, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 34),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 35, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 35),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 36, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 36),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 37, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 37),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 38, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 38),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 39, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 39),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 40, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 40),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 41, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 41),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 42, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 42),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 43, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 43),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 44, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 44),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 45, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 45),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 46, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 46),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 47, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 47),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 48, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 48),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 49, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 49),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 50, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 50),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 51, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 51),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 52, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 52),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 53, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 53),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 54, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 54),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 55, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 55),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 56, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 56),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 57, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 57),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 58, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 58),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 59, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 59),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 60, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 60),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 61, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 61),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 62, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 62),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 63, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 63),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 64, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 64),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 65, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 65),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 66, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 66),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 67, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 67),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 68, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 68),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 69, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 69),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 70, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 70),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 71, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 71),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 72, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 72),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 73, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 73),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 74, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 74),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 75, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 75),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 76, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 76),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 77, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 77),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 78, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 78),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 79, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 79),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 80, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 80),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 81, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 81),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 82, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 82),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 83, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 83),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 84, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 84),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 85, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 85),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 86, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 86),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 87, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 87),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 88, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 88),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 89, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 89),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 90, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 90),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 91, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 91),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 92, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 92),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 93, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 93),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 94, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 94),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 95, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 95),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 96, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 96),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 97, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 97),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 98, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 98),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 99, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 99),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 100, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 100),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 101, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 101),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 102, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 102),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 103, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 103),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 104, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 104),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 105, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 105),
(1, 'document', 1, '2013-05-25 00:56:51', '2013-05-25 00:56:51', 106, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 106),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 107, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 107),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 108, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 108),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 109, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 109),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 110, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 110),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 111, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 111),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 112, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 112),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 113, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 113),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 114, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 114),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 115, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 115),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 116, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 116),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 117, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 117),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 118, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 118),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 119, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 119),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 120, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 120),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 121, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 121),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 122, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 122),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 123, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 123),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 124, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 124),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 125, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 125),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 126, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 126),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 127, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 127),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 128, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 128),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 129, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 129),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 130, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 130),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 131, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 131),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 132, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 132),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 133, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 133),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 134, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 134),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 135, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 135),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 136, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 136),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 137, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 137),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 138, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 138),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 139, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 139),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 140, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 140),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 141, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 141),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 142, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 142),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 143, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 143),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 144, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 144),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 145, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 145),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 146, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 146),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 147, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 147),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 148, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 148),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 149, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 149),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 150, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 150),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 151, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 151),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 152, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 152),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 153, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 153),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 154, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 154),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 155, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 155),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 156, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 156),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 157, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 157),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 158, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 158),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 159, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 159),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 160, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 160),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 161, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 161),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 162, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 162),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 163, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 163),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 164, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 164),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 165, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 165),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 166, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 166),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 167, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 167),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 168, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 168),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 169, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 169),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 170, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 170),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 171, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 171),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 172, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 172),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 173, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 173),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 174, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 174),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 175, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 175),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 176, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 176),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 177, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 177),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 178, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 178),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 179, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 179),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 180, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 180),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 181, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 181),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 182, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 182),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 183, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 183),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 184, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 184),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 185, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 185),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 186, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 186),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 187, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 187),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 188, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 188),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 189, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 189),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 190, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 190),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 191, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 191),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 192, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 192),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 193, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 193),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 194, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 194),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 195, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 195),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 196, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 196),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 197, 'DocumentAdded', 1, 0, NULL, 0, NULL, NULL, NULL, 197),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 198, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 198),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 199, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 199),
(1, 'document', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 200, 'DocumentAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 200),
(1, 'calendar_event', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 1, 'AgendaAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 201),
(1, 'link', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 1, 'LinkAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 202),
(0, 'link', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 2, 'LinkAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 203),
(1, 'announcement', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 1, 'AnnouncementAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 204),
(1, 'forum_category', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 1, 'ForumCategoryAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 205),
(1, 'forum', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 1, 'ForumAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 206),
(1, 'forum_thread', 1, '2013-05-25 00:56:52', '2013-05-25 00:56:52', 1, 'ForumThreadAdded', 1, 0, NULL, 1, NULL, NULL, NULL, 207);

-- --------------------------------------------------------

--
-- Table structure for table `c_link`
--

CREATE TABLE IF NOT EXISTS `c_link` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` text,
  `category_id` int(10) unsigned DEFAULT NULL,
  `display_order` int(10) unsigned NOT NULL DEFAULT '0',
  `on_homepage` enum('0','1') NOT NULL DEFAULT '0',
  `target` char(10) DEFAULT '_self',
  `session_id` int(11) DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_link`
--

INSERT INTO `c_link` (`c_id`, `id`, `url`, `title`, `description`, `category_id`, `display_order`, `on_homepage`, `target`, `session_id`) VALUES
(1, 1, 'http://www.google.com', 'Google', 'Quick and powerful search engine', 0, 0, '0', '_self', 0),
(1, 2, 'http://www.wikipedia.org', 'Wikipedia', 'Free online encyclopedia', 0, 1, '0', '_self', 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_link_category`
--

CREATE TABLE IF NOT EXISTS `c_link_category` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) NOT NULL,
  `description` text,
  `display_order` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `session_id` int(11) DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_link_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_lp`
--

CREATE TABLE IF NOT EXISTS `c_lp` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lp_type` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `ref` tinytext,
  `description` text,
  `path` text NOT NULL,
  `force_commit` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `default_view_mod` char(32) NOT NULL DEFAULT 'embedded',
  `default_encoding` char(32) NOT NULL DEFAULT 'UTF-8',
  `display_order` int(10) unsigned NOT NULL DEFAULT '0',
  `content_maker` tinytext NOT NULL,
  `content_local` varchar(32) NOT NULL DEFAULT 'local',
  `content_license` text NOT NULL,
  `prevent_reinit` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `js_lib` tinytext NOT NULL,
  `debug` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `theme` varchar(255) NOT NULL DEFAULT '',
  `preview_image` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL DEFAULT '',
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  `prerequisite` int(10) unsigned NOT NULL DEFAULT '0',
  `hide_toc_frame` tinyint(4) NOT NULL DEFAULT '0',
  `seriousgame_mode` tinyint(4) NOT NULL DEFAULT '0',
  `use_max_score` int(10) unsigned NOT NULL DEFAULT '1',
  `autolunch` int(10) unsigned NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publicated_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `max_attempts` int(11) NOT NULL DEFAULT '0',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subscribe_users` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_lp`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_lp_category`
--

CREATE TABLE IF NOT EXISTS `c_lp_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(10) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_lp_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_lp_item`
--

CREATE TABLE IF NOT EXISTS `c_lp_item` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lp_id` int(10) unsigned NOT NULL,
  `item_type` char(32) NOT NULL DEFAULT 'dokeos_document',
  `ref` tinytext NOT NULL,
  `title` varchar(511) NOT NULL,
  `description` varchar(511) NOT NULL DEFAULT '',
  `path` text NOT NULL,
  `min_score` float unsigned NOT NULL DEFAULT '0',
  `max_score` float unsigned DEFAULT '100',
  `mastery_score` float unsigned DEFAULT NULL,
  `parent_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `previous_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `next_item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `display_order` int(10) unsigned NOT NULL DEFAULT '0',
  `prerequisite` text,
  `parameters` text,
  `launch_data` text NOT NULL,
  `max_time_allowed` char(13) DEFAULT '',
  `terms` text,
  `search_did` int(11) DEFAULT NULL,
  `audio` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`c_id`,`id`),
  KEY `lp_id` (`lp_id`),
  KEY `idx_c_lp_item_cid_lp_id` (`c_id`,`lp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_lp_item`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_lp_item_view`
--

CREATE TABLE IF NOT EXISTS `c_lp_item_view` (
  `c_id` int(11) NOT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lp_item_id` int(10) unsigned NOT NULL,
  `lp_view_id` int(10) unsigned NOT NULL,
  `view_count` int(10) unsigned NOT NULL DEFAULT '0',
  `start_time` int(10) unsigned NOT NULL,
  `total_time` int(10) unsigned NOT NULL DEFAULT '0',
  `score` float unsigned NOT NULL DEFAULT '0',
  `status` char(32) NOT NULL DEFAULT 'not attempted',
  `suspend_data` longtext,
  `lesson_location` text,
  `core_exit` varchar(32) NOT NULL DEFAULT 'none',
  `max_score` varchar(8) DEFAULT '',
  PRIMARY KEY (`c_id`,`id`),
  KEY `lp_item_id` (`lp_item_id`),
  KEY `lp_view_id` (`lp_view_id`),
  KEY `idx_c_lp_item_view_cid_lp_view_id_lp_item_id` (`c_id`,`lp_view_id`,`lp_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_lp_item_view`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_lp_iv_interaction`
--

CREATE TABLE IF NOT EXISTS `c_lp_iv_interaction` (
  `c_id` int(11) NOT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lp_iv_id` bigint(20) unsigned NOT NULL,
  `interaction_id` varchar(255) NOT NULL DEFAULT '',
  `interaction_type` varchar(255) NOT NULL DEFAULT '',
  `weighting` double NOT NULL DEFAULT '0',
  `completion_time` varchar(16) NOT NULL DEFAULT '',
  `correct_responses` text NOT NULL,
  `student_response` text NOT NULL,
  `result` varchar(255) NOT NULL DEFAULT '',
  `latency` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`c_id`,`id`),
  KEY `lp_iv_id` (`lp_iv_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_lp_iv_interaction`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_lp_iv_objective`
--

CREATE TABLE IF NOT EXISTS `c_lp_iv_objective` (
  `c_id` int(11) NOT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lp_iv_id` bigint(20) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `objective_id` varchar(255) NOT NULL DEFAULT '',
  `score_raw` float unsigned NOT NULL DEFAULT '0',
  `score_max` float unsigned NOT NULL DEFAULT '0',
  `score_min` float unsigned NOT NULL DEFAULT '0',
  `status` char(32) NOT NULL DEFAULT 'not attempted',
  PRIMARY KEY (`c_id`,`id`),
  KEY `lp_iv_id` (`lp_iv_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_lp_iv_objective`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_lp_view`
--

CREATE TABLE IF NOT EXISTS `c_lp_view` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lp_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `view_count` int(10) unsigned NOT NULL DEFAULT '0',
  `last_item` int(10) unsigned NOT NULL DEFAULT '0',
  `progress` int(10) unsigned DEFAULT '0',
  `session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `lp_id` (`lp_id`),
  KEY `user_id` (`user_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_lp_view`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_metadata`
--

CREATE TABLE IF NOT EXISTS `c_metadata` (
  `c_id` int(11) NOT NULL,
  `eid` varchar(250) NOT NULL,
  `mdxmltext` text,
  `md5` char(32) DEFAULT '',
  `htmlcache1` text,
  `htmlcache2` text,
  `indexabletext` text,
  PRIMARY KEY (`c_id`,`eid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_metadata`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_notebook`
--

CREATE TABLE IF NOT EXISTS `c_notebook` (
  `c_id` int(11) NOT NULL,
  `notebook_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `course` varchar(40) NOT NULL,
  `session_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`c_id`,`notebook_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_notebook`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_online_connected`
--

CREATE TABLE IF NOT EXISTS `c_online_connected` (
  `c_id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `last_connection` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`c_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_online_connected`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_online_link`
--

CREATE TABLE IF NOT EXISTS `c_online_link` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL DEFAULT '',
  `url` char(100) NOT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_online_link`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_permission_group`
--

CREATE TABLE IF NOT EXISTS `c_permission_group` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `tool` varchar(250) NOT NULL DEFAULT '',
  `action` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_permission_group`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_permission_task`
--

CREATE TABLE IF NOT EXISTS `c_permission_task` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL DEFAULT '0',
  `tool` varchar(250) NOT NULL DEFAULT '',
  `action` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_permission_task`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_permission_user`
--

CREATE TABLE IF NOT EXISTS `c_permission_user` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `tool` varchar(250) NOT NULL DEFAULT '',
  `action` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_permission_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_quiz`
--

CREATE TABLE IF NOT EXISTS `c_quiz` (
  `iid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `sound` varchar(255) DEFAULT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `random` int(11) NOT NULL DEFAULT '0',
  `random_answers` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `results_disabled` int(10) unsigned NOT NULL DEFAULT '0',
  `access_condition` text,
  `max_attempt` int(11) NOT NULL DEFAULT '0',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `feedback_type` int(11) NOT NULL DEFAULT '0',
  `expired_time` int(11) NOT NULL DEFAULT '0',
  `session_id` int(11) DEFAULT '0',
  `propagate_neg` int(11) NOT NULL DEFAULT '0',
  `review_answers` int(11) NOT NULL DEFAULT '0',
  `random_by_category` int(11) NOT NULL DEFAULT '0',
  `text_when_finished` text,
  `display_category_name` int(11) NOT NULL DEFAULT '1',
  `pass_percentage` int(11) DEFAULT NULL,
  `autolaunch` int(11) DEFAULT '0',
  `end_button` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `c_quiz`
--

INSERT INTO `c_quiz` (`iid`, `c_id`, `title`, `description`, `sound`, `type`, `random`, `random_answers`, `active`, `results_disabled`, `access_condition`, `max_attempt`, `start_time`, `end_time`, `feedback_type`, `expired_time`, `session_id`, `propagate_neg`, `review_answers`, `random_by_category`, `text_when_finished`, `display_category_name`, `pass_percentage`, `autolaunch`, `end_button`) VALUES
(1, 1, 'Sample test', '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="110" valign="top" align="left"><img src="http://localhost/www/new_lms/chamilo-lms/main/default_course_document/images/mr_dokeos/thinking.jpg"></td><td valign="top" align="left">Irony</td></tr></table>', NULL, 1, 0, 0, 1, 0, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, NULL, 1, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_quiz_answer`
--

CREATE TABLE IF NOT EXISTS `c_quiz_answer` (
  `iid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `answer` text NOT NULL,
  `correct` mediumint(8) unsigned DEFAULT NULL,
  `comment` text,
  `ponderation` float(6,2) NOT NULL DEFAULT '0.00',
  `position` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `hotspot_coordinates` text,
  `hotspot_type` enum('square','circle','poly','delineation','oar') DEFAULT NULL,
  `destination` text NOT NULL,
  `answer_code` char(10) DEFAULT '',
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_quiz_answer`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_quiz_category`
--

CREATE TABLE IF NOT EXISTS `c_quiz_category` (
  `iid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `c_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `lvl` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `root` int(11) DEFAULT NULL,
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_quiz_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_quiz_order`
--

CREATE TABLE IF NOT EXISTS `c_quiz_order` (
  `iid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `exercise_id` int(11) NOT NULL,
  `exercise_order` int(11) NOT NULL,
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_quiz_order`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_quiz_question`
--

CREATE TABLE IF NOT EXISTS `c_quiz_question` (
  `iid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `description` text,
  `ponderation` float(6,2) NOT NULL DEFAULT '0.00',
  `position` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `picture` varchar(50) DEFAULT NULL,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `extra` varchar(255) DEFAULT NULL,
  `question_code` char(10) DEFAULT '',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_quiz_question`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_quiz_question_option`
--

CREATE TABLE IF NOT EXISTS `c_quiz_question_option` (
  `iid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` int(10) unsigned NOT NULL,
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_quiz_question_option`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_quiz_question_rel_category`
--

CREATE TABLE IF NOT EXISTS `c_quiz_question_rel_category` (
  `iid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_quiz_question_rel_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_quiz_rel_category`
--

CREATE TABLE IF NOT EXISTS `c_quiz_rel_category` (
  `iid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `exercise_id` int(10) unsigned NOT NULL,
  `count_questions` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_quiz_rel_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_quiz_rel_question`
--

CREATE TABLE IF NOT EXISTS `c_quiz_rel_question` (
  `iid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `exercice_id` int(10) unsigned NOT NULL,
  `question_order` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `c_quiz_rel_question`
--

INSERT INTO `c_quiz_rel_question` (`iid`, `c_id`, `question_id`, `exercice_id`, `question_order`) VALUES
(1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_resource`
--

CREATE TABLE IF NOT EXISTS `c_resource` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(50) DEFAULT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  `resource_type` varchar(50) DEFAULT NULL,
  `resource_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_resource`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_role`
--

CREATE TABLE IF NOT EXISTS `c_role` (
  `c_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(250) NOT NULL DEFAULT '',
  `role_comment` text,
  `default_role` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`c_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_role`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_role_group`
--

CREATE TABLE IF NOT EXISTS `c_role_group` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `scope` varchar(20) NOT NULL DEFAULT 'course',
  `group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`c_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_role_group`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_role_permissions`
--

CREATE TABLE IF NOT EXISTS `c_role_permissions` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `tool` varchar(250) NOT NULL DEFAULT '',
  `action` varchar(50) NOT NULL DEFAULT '',
  `default_perm` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`c_id`,`role_id`,`tool`,`action`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_role_permissions`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_role_user`
--

CREATE TABLE IF NOT EXISTS `c_role_user` (
  `c_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `scope` varchar(20) NOT NULL DEFAULT 'course',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`role_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_role_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_student_publication`
--

CREATE TABLE IF NOT EXISTS `c_student_publication` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `author` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `accepted` tinyint(4) DEFAULT '0',
  `post_group_id` int(11) NOT NULL DEFAULT '0',
  `sent_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filetype` set('file','folder') NOT NULL DEFAULT 'file',
  `has_properties` int(10) unsigned NOT NULL DEFAULT '0',
  `view_properties` tinyint(4) DEFAULT NULL,
  `qualification` float(6,2) unsigned NOT NULL DEFAULT '0.00',
  `date_of_qualification` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `qualificator_id` int(10) unsigned NOT NULL DEFAULT '0',
  `weight` float(6,2) unsigned NOT NULL DEFAULT '0.00',
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `allow_text_assignment` int(11) NOT NULL DEFAULT '0',
  `contains_file` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_student_publication`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_student_publication_assignment`
--

CREATE TABLE IF NOT EXISTS `c_student_publication_assignment` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expires_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ends_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `add_to_calendar` tinyint(4) NOT NULL,
  `enable_qualification` tinyint(4) NOT NULL,
  `publication_id` int(11) NOT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_student_publication_assignment`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_survey`
--

CREATE TABLE IF NOT EXISTS `c_survey` (
  `c_id` int(11) NOT NULL,
  `survey_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `title` text,
  `subtitle` text,
  `author` varchar(20) DEFAULT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `avail_from` date DEFAULT NULL,
  `avail_till` date DEFAULT NULL,
  `is_shared` char(1) DEFAULT '1',
  `template` varchar(20) DEFAULT NULL,
  `intro` text,
  `surveythanks` text,
  `creation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `invited` int(11) NOT NULL,
  `answered` int(11) NOT NULL,
  `invite_mail` text NOT NULL,
  `reminder_mail` text NOT NULL,
  `mail_subject` varchar(255) NOT NULL,
  `anonymous` enum('0','1') NOT NULL DEFAULT '0',
  `access_condition` text,
  `shuffle` tinyint(1) NOT NULL DEFAULT '0',
  `one_question_per_page` tinyint(1) NOT NULL DEFAULT '0',
  `survey_version` varchar(255) NOT NULL DEFAULT '',
  `parent_id` int(10) unsigned NOT NULL,
  `survey_type` int(11) NOT NULL DEFAULT '0',
  `show_form_profile` int(11) NOT NULL DEFAULT '0',
  `form_fields` text NOT NULL,
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`survey_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_survey`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_survey_answer`
--

CREATE TABLE IF NOT EXISTS `c_survey_answer` (
  `c_id` int(11) NOT NULL,
  `answer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `survey_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `option_id` text NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `user` varchar(250) NOT NULL,
  PRIMARY KEY (`c_id`,`answer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_survey_answer`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_survey_group`
--

CREATE TABLE IF NOT EXISTS `c_survey_group` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `survey_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_survey_group`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_survey_invitation`
--

CREATE TABLE IF NOT EXISTS `c_survey_invitation` (
  `c_id` int(11) NOT NULL,
  `survey_invitation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `survey_code` varchar(20) NOT NULL,
  `user` varchar(250) NOT NULL,
  `invitation_code` varchar(250) NOT NULL,
  `invitation_date` datetime NOT NULL,
  `reminder_date` datetime NOT NULL,
  `answered` int(11) NOT NULL DEFAULT '0',
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`survey_invitation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_survey_invitation`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_survey_question`
--

CREATE TABLE IF NOT EXISTS `c_survey_question` (
  `c_id` int(11) NOT NULL,
  `question_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `survey_id` int(10) unsigned NOT NULL,
  `survey_question` text NOT NULL,
  `survey_question_comment` text NOT NULL,
  `type` varchar(250) NOT NULL,
  `display` varchar(10) NOT NULL,
  `sort` int(11) NOT NULL,
  `shared_question_id` int(11) DEFAULT NULL,
  `max_value` int(11) DEFAULT NULL,
  `survey_group_pri` int(10) unsigned NOT NULL DEFAULT '0',
  `survey_group_sec1` int(10) unsigned NOT NULL DEFAULT '0',
  `survey_group_sec2` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_survey_question`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_survey_question_option`
--

CREATE TABLE IF NOT EXISTS `c_survey_question_option` (
  `c_id` int(11) NOT NULL,
  `question_option_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `survey_id` int(10) unsigned NOT NULL,
  `option_text` text NOT NULL,
  `sort` int(11) NOT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`question_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_survey_question_option`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_thematic`
--

CREATE TABLE IF NOT EXISTS `c_thematic` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `display_order` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `active` (`active`,`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_thematic`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_thematic_advance`
--

CREATE TABLE IF NOT EXISTS `c_thematic_advance` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thematic_id` int(11) NOT NULL,
  `attendance_id` int(11) NOT NULL DEFAULT '0',
  `content` text,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duration` int(11) NOT NULL DEFAULT '0',
  `done_advance` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `thematic_id` (`thematic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_thematic_advance`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_thematic_plan`
--

CREATE TABLE IF NOT EXISTS `c_thematic_plan` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thematic_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `description_type` int(11) NOT NULL,
  PRIMARY KEY (`c_id`,`id`),
  KEY `thematic_id` (`thematic_id`,`description_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_thematic_plan`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_tool`
--

CREATE TABLE IF NOT EXISTS `c_tool` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `visibility` tinyint(3) unsigned DEFAULT '0',
  `admin` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `added_tool` tinyint(3) unsigned DEFAULT '1',
  `target` enum('_self','_blank') NOT NULL DEFAULT '_self',
  `category` varchar(20) NOT NULL DEFAULT 'authoring',
  `session_id` int(11) DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_tool`
--

INSERT INTO `c_tool` (`c_id`, `id`, `name`, `link`, `image`, `visibility`, `admin`, `address`, `added_tool`, `target`, `category`, `session_id`) VALUES
(1, 1, 'course_description', 'course_description/', 'info.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 2, 'calendar_event', 'calendar/agenda.php', 'agenda.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 3, 'document', 'document/document.php', 'folder_document.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 4, 'learnpath', 'newscorm/lp_controller.php', 'scorms.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 5, 'link', 'link/link.php', 'links.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 6, 'quiz', 'exercice/exercice.php', 'quiz.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 7, 'announcement', 'announcements/announcements.php', 'valves.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 8, 'forum', 'forum/index.php', 'forum.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 9, 'dropbox', 'dropbox/index.php', 'dropbox.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 10, 'user', 'user/user.php', 'members.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 11, 'group', 'group/group.php', 'group.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 12, 'chat', 'chat/chat.php', 'chat.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 13, 'student_publication', 'work/work.php', 'works.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 14, 'survey', 'survey/survey_list.php', 'survey.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 15, 'wiki', 'wiki/index.php', 'wiki.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 16, 'gradebook', 'gradebook/index.php', 'gradebook.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 17, 'glossary', 'glossary/index.php', 'glossary.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 18, 'notebook', 'notebook/index.php', 'notebook.gif', 1, '0', 'squaregrey.gif', 0, '_self', 'interaction', 0),
(1, 19, 'attendance', 'attendance/index.php', 'attendance.gif', 0, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 20, 'course_progress', 'course_progress/index.php', 'course_progress.gif', 0, '0', 'squaregrey.gif', 0, '_self', 'authoring', 0),
(1, 21, 'blog_management', 'blog/blog_admin.php', 'blog_admin.gif', 0, '1', 'squaregrey.gif', 0, '_self', 'admin', 0),
(1, 22, 'tracking', 'tracking/courseLog.php', 'statistics.gif', 0, '1', '', 0, '_self', 'admin', 0),
(1, 23, 'course_setting', 'course_info/infocours.php', 'reference.gif', 0, '1', '', 0, '_self', 'admin', 0),
(1, 24, 'course_maintenance', 'course_info/maintenance.php', 'backup.gif', 0, '1', '', 0, '_self', 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_tool_intro`
--

CREATE TABLE IF NOT EXISTS `c_tool_intro` (
  `c_id` int(11) NOT NULL,
  `id` varchar(50) NOT NULL,
  `intro_text` mediumtext NOT NULL,
  `session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`,`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_tool_intro`
--

INSERT INTO `c_tool_intro` (`c_id`, `id`, `intro_text`, `session_id`) VALUES
(1, 'course_homepage', '<p style="text-align: center;">\r\n                            <img src="/chamilo-lms/main/img/mascot.png" alt="Mr. Chamilo" title="Mr. Chamilo" />\r\n                            <h2>Welcome to this training!</h2>\r\n                         </p>', 0),
(1, 'student_publication', 'This page allows users and groups to publish documents.', 0),
(1, 'wiki', '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="110" valign="top" align="left"></td><td valign="top" align="left">The word Wiki is short for WikiWikiWeb. Wikiwiki is a Hawaiian word, meaning "fast" or "speed". In a wiki, people write pages together. If one person writes something wrong, the next person can correct it. The next person can also add something new to the page. Because of this, the pages improve continuously.</td></tr></table>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_userinfo_content`
--

CREATE TABLE IF NOT EXISTS `c_userinfo_content` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `definition_id` int(10) unsigned NOT NULL,
  `editor_ip` varchar(39) DEFAULT NULL,
  `edition_time` datetime DEFAULT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`c_id`,`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_userinfo_content`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_userinfo_def`
--

CREATE TABLE IF NOT EXISTS `c_userinfo_def` (
  `c_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL DEFAULT '',
  `comment` text,
  `line_count` tinyint(3) unsigned NOT NULL DEFAULT '5',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_userinfo_def`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_wiki`
--

CREATE TABLE IF NOT EXISTS `c_wiki` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `reflink` varchar(255) NOT NULL DEFAULT 'index',
  `title` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) DEFAULT NULL,
  `dtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `addlock` int(11) NOT NULL DEFAULT '1',
  `editlock` int(11) NOT NULL DEFAULT '0',
  `visibility` int(11) NOT NULL DEFAULT '1',
  `addlock_disc` int(11) NOT NULL DEFAULT '1',
  `visibility_disc` int(11) NOT NULL DEFAULT '1',
  `ratinglock_disc` int(11) NOT NULL DEFAULT '1',
  `assignment` int(11) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `progress` text NOT NULL,
  `score` int(11) DEFAULT '0',
  `version` int(11) DEFAULT NULL,
  `is_editing` int(11) NOT NULL DEFAULT '0',
  `time_edit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) DEFAULT '0',
  `linksto` text NOT NULL,
  `tag` text NOT NULL,
  `user_ip` varchar(39) NOT NULL,
  `session_id` int(11) DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`),
  KEY `reflink` (`reflink`),
  KEY `group_id` (`group_id`),
  KEY `page_id` (`page_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_wiki`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_wiki_conf`
--

CREATE TABLE IF NOT EXISTS `c_wiki_conf` (
  `c_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `task` text NOT NULL,
  `feedback1` text NOT NULL,
  `feedback2` text NOT NULL,
  `feedback3` text NOT NULL,
  `fprogress1` varchar(3) NOT NULL,
  `fprogress2` varchar(3) NOT NULL,
  `fprogress3` varchar(3) NOT NULL,
  `max_size` int(11) DEFAULT NULL,
  `max_text` int(11) DEFAULT NULL,
  `max_version` int(11) DEFAULT NULL,
  `startdate_assig` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enddate_assig` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `delayedsubmit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c_id`,`page_id`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_wiki_conf`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_wiki_discuss`
--

CREATE TABLE IF NOT EXISTS `c_wiki_discuss` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_id` int(11) NOT NULL DEFAULT '0',
  `userc_id` int(11) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `p_score` varchar(255) DEFAULT NULL,
  `dtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `c_wiki_discuss`
--


-- --------------------------------------------------------

--
-- Table structure for table `c_wiki_mailcue`
--

CREATE TABLE IF NOT EXISTS `c_wiki_mailcue` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` text NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT '0',
  PRIMARY KEY (`c_id`,`id`,`user_id`),
  KEY `c_id` (`c_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_wiki_mailcue`
--


-- --------------------------------------------------------

--
-- Table structure for table `event_email_template`
--

CREATE TABLE IF NOT EXISTS `event_email_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text,
  `subject` varchar(255) DEFAULT NULL,
  `event_type_name` varchar(255) DEFAULT NULL,
  `activated` tinyint(4) NOT NULL DEFAULT '0',
  `language_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_name_index` (`event_type_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `event_email_template`
--


-- --------------------------------------------------------

--
-- Table structure for table `event_sent`
--

CREATE TABLE IF NOT EXISTS `event_sent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) DEFAULT NULL,
  `event_type_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_name_index` (`event_type_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `event_sent`
--


-- --------------------------------------------------------

--
-- Table structure for table `extra_field_option_rel_field_option`
--

CREATE TABLE IF NOT EXISTS `extra_field_option_rel_field_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `field_id` int(11) DEFAULT NULL,
  `field_option_id` int(11) DEFAULT NULL,
  `related_field_option_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `extra_field_option_rel_field_option`
--


-- --------------------------------------------------------

--
-- Table structure for table `ext_log_entries`
--

CREATE TABLE IF NOT EXISTS `ext_log_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) DEFAULT NULL,
  `logged_at` datetime DEFAULT NULL,
  `object_id` varchar(64) DEFAULT NULL,
  `object_class` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ext_log_entries`
--


-- --------------------------------------------------------

--
-- Table structure for table `gradebook_category`
--

CREATE TABLE IF NOT EXISTS `gradebook_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text,
  `user_id` int(11) NOT NULL,
  `course_code` varchar(40) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `weight` float NOT NULL,
  `visible` tinyint(4) NOT NULL,
  `certif_min_score` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `document_id` int(10) unsigned DEFAULT NULL,
  `locked` int(11) NOT NULL DEFAULT '0',
  `default_lowest_eval_exclude` tinyint(4) DEFAULT NULL,
  `grade_model_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `gradebook_category`
--

INSERT INTO `gradebook_category` (`id`, `name`, `description`, `user_id`, `course_code`, `parent_id`, `weight`, `visible`, `certif_min_score`, `session_id`, `document_id`, `locked`, `default_lowest_eval_exclude`, `grade_model_id`) VALUES
(1, 'A00001', '', 1, 'A00001', 0, 100, 3, NULL, NULL, 200, 0, NULL, 0),
(2, 'A00001', '', 1, 'A00001', 1, 100, 3, 75, NULL, 200, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gradebook_certificate`
--

CREATE TABLE IF NOT EXISTS `gradebook_certificate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `score_certificate` float unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `path_certificate` text,
  PRIMARY KEY (`id`),
  KEY `idx_gradebook_certificate_category_id` (`cat_id`),
  KEY `idx_gradebook_certificate_user_id` (`user_id`),
  KEY `idx_gradebook_certificate_category_id_user_id` (`cat_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gradebook_certificate`
--


-- --------------------------------------------------------

--
-- Table structure for table `gradebook_evaluation`
--

CREATE TABLE IF NOT EXISTS `gradebook_evaluation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text,
  `user_id` int(11) NOT NULL,
  `course_code` varchar(40) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `weight` float NOT NULL,
  `max` float unsigned NOT NULL,
  `visible` int(11) NOT NULL,
  `type` varchar(40) NOT NULL DEFAULT 'evaluation',
  `locked` int(11) NOT NULL DEFAULT '0',
  `evaluation_type_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gradebook_evaluation`
--


-- --------------------------------------------------------

--
-- Table structure for table `gradebook_evaluation_type`
--

CREATE TABLE IF NOT EXISTS `gradebook_evaluation_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `external_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gradebook_evaluation_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `gradebook_link`
--

CREATE TABLE IF NOT EXISTS `gradebook_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_code` varchar(40) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `weight` float NOT NULL,
  `visible` int(11) NOT NULL,
  `locked` int(11) NOT NULL DEFAULT '0',
  `evaluation_type_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gradebook_link`
--

INSERT INTO `gradebook_link` (`id`, `type`, `ref_id`, `user_id`, `course_code`, `category_id`, `created_at`, `weight`, `visible`, `locked`, `evaluation_type_id`) VALUES
(1, 1, 1, 1, 'A00001', 2, '2013-05-24 17:56:51', 100, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gradebook_linkeval_log`
--

CREATE TABLE IF NOT EXISTS `gradebook_linkeval_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_linkeval_log` int(11) NOT NULL,
  `name` text,
  `description` text,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `weight` smallint(6) DEFAULT NULL,
  `visible` tinyint(4) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `user_id_log` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gradebook_linkeval_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `gradebook_result`
--

CREATE TABLE IF NOT EXISTS `gradebook_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `evaluation_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `score` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gradebook_result`
--


-- --------------------------------------------------------

--
-- Table structure for table `gradebook_result_log`
--

CREATE TABLE IF NOT EXISTS `gradebook_result_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_result` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `evaluation_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `score` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gradebook_result_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `gradebook_score_display`
--

CREATE TABLE IF NOT EXISTS `gradebook_score_display` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` float unsigned NOT NULL,
  `display` varchar(40) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `score_color_percent` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gradebook_score_display`
--


-- --------------------------------------------------------

--
-- Table structure for table `grade_components`
--

CREATE TABLE IF NOT EXISTS `grade_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `percentage` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `acronym` varchar(255) NOT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `count_elements` int(11) DEFAULT '0',
  `exclusions` int(11) DEFAULT '0',
  `grade_model_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `grade_components`
--


-- --------------------------------------------------------

--
-- Table structure for table `grade_model`
--

CREATE TABLE IF NOT EXISTS `grade_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `default_lowest_eval_exclude` tinyint(4) DEFAULT NULL,
  `default_external_eval` tinyint(4) DEFAULT NULL,
  `default_external_eval_prefix` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `grade_model`
--


-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `original_name` varchar(255) DEFAULT NULL,
  `english_name` varchar(255) DEFAULT NULL,
  `isocode` varchar(10) DEFAULT NULL,
  `dokeos_folder` varchar(250) DEFAULT NULL,
  `available` tinyint(4) NOT NULL DEFAULT '1',
  `parent_id` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_language_dokeos_folder` (`dokeos_folder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `original_name`, `english_name`, `isocode`, `dokeos_folder`, `available`, `parent_id`) VALUES
(1, '&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;', 'arabic', 'ar', 'arabic', 0, NULL),
(2, 'Asturianu', 'asturian', 'ast', 'asturian', 0, NULL),
(3, '&#2476;&#2494;&#2434;&#2482;&#2494;', 'bengali', 'bn', 'bengali', 0, NULL),
(4, '&#1041;&#1098;&#1083;&#1075;&#1072;&#1088;&#1089;&#1082;&#1080;', 'bulgarian', 'bg', 'bulgarian', 1, NULL),
(5, 'Bosanski', 'bosnian', 'bs', 'bosnian', 1, NULL),
(6, 'Catal&agrave;', 'catalan', 'ca', 'catalan', 0, NULL),
(7, '&#20013;&#25991;&#65288;&#31616;&#20307;&#65289;', 'simpl_chinese', 'zh', 'simpl_chinese', 0, NULL),
(8, '&#32321;&#39636;&#20013;&#25991;', 'trad_chinese', 'zh-TW', 'trad_chinese', 0, NULL),
(9, '&#268;esky', 'czech', 'cs', 'czech', 0, NULL),
(10, 'Dansk', 'danish', 'da', 'danish', 0, NULL),
(11, '&#1583;&#1585;&#1740;', 'dari', 'prs', 'dari', 0, NULL),
(12, 'Deutsch', 'german', 'de', 'german', 1, NULL),
(13, '&Epsilon;&lambda;&lambda;&eta;&nu;&iota;&kappa;&#940;', 'greek', 'el', 'greek', 0, NULL),
(14, 'English', 'english', 'en', 'english', 1, NULL),
(15, 'Espa&ntilde;ol', 'spanish', 'es', 'spanish', 1, NULL),
(16, 'Esperanto', 'esperanto', 'eo', 'esperanto', 0, NULL),
(17, 'Euskara', 'basque', 'eu', 'basque', 0, NULL),
(18, '&#1601;&#1575;&#1585;&#1587;&#1740;', 'persian', 'fa', 'persian', 0, NULL),
(19, 'Fran&ccedil;ais', 'french', 'fr', 'french', 1, NULL),
(20, 'Furlan', 'friulian', 'fur', 'friulian', 0, NULL),
(21, 'Galego', 'galician', 'gl', 'galician', 0, NULL),
(22, '&#4325;&#4304;&#4320;&#4311;&#4323;&#4314;&#4312;', 'georgian', 'ka', 'georgian', 0, NULL),
(23, 'Hrvatski', 'croatian', 'hr', 'croatian', 0, NULL),
(24, '&#1506;&#1489;&#1512;&#1497;&#1514;', 'hebrew', 'he', 'hebrew', 0, NULL),
(25, '&#2361;&#2367;&#2344;&#2381;&#2342;&#2368;', 'hindi', 'hi', 'hindi', 0, NULL),
(26, 'Bahasa Indonesia', 'indonesian', 'id', 'indonesian', 1, NULL),
(27, 'Italiano', 'italian', 'it', 'italian', 1, NULL),
(28, '&#54620;&#44397;&#50612;', 'korean', 'ko', 'korean', 0, NULL),
(29, 'Latvie&scaron;u', 'latvian', 'lv', 'latvian', 0, NULL),
(30, 'Lietuvi&#371;', 'lithuanian', 'lt', 'lithuanian', 0, NULL),
(31, '&#1052;&#1072;&#1082;&#1077;&#1076;&#1086;&#1085;&#1089;&#1082;&#1080;', 'macedonian', 'mk', 'macedonian', 0, NULL),
(32, 'Magyar', 'hungarian', 'hu', 'hungarian', 1, NULL),
(33, 'Bahasa Melayu', 'malay', 'ms', 'malay', 0, NULL),
(34, 'Nederlands', 'dutch', 'nl', 'dutch', 1, NULL),
(35, '&#26085;&#26412;&#35486;', 'japanese', 'ja', 'japanese', 0, NULL),
(36, 'Norsk', 'norwegian', 'no', 'norwegian', 0, NULL),
(37, 'Occitan', 'occitan', 'oc', 'occitan', 0, NULL),
(38, '&#1662;&#1690;&#1578;&#1608;', 'pashto', 'ps', 'pashto', 0, NULL),
(39, 'Polski', 'polish', 'pl', 'polish', 0, NULL),
(40, 'Portugu&ecirc;s europeu', 'portuguese', 'pt', 'portuguese', 1, NULL),
(41, 'Portugu&ecirc;s do Brasil', 'brazilian', 'pt-BR', 'brazilian', 1, NULL),
(42, 'Rom&acirc;n&#259;', 'romanian', 'ro', 'romanian', 0, NULL),
(43, 'Runasimi', 'quechua_cusco', 'qu', 'quechua_cusco', 0, NULL),
(44, '&#1056;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;', 'russian', 'ru', 'russian', 0, NULL),
(45, 'Sloven&#269;ina', 'slovak', 'sk', 'slovak', 0, NULL),
(46, 'Sloven&scaron;&#269;ina', 'slovenian', 'sl', 'slovenian', 1, NULL),
(47, '&#1575;&#1604;&#1589;&#1608;&#1605;&#1575;&#1604;&#1610;&#1577;', 'somali', 'so', 'somali', 0, NULL),
(48, 'Srpski', 'serbian', 'sr', 'serbian', 0, NULL),
(49, 'Suomi', 'finnish', 'fi', 'finnish', 0, NULL),
(50, 'Svenska', 'swedish', 'sv', 'swedish', 0, NULL),
(51, '&#3652;&#3607;&#3618;', 'thai', 'th', 'thai', 0, NULL),
(52, 'T&uuml;rk&ccedil;e', 'turkish', 'tr', 'turkish', 0, NULL),
(53, '&#1059;&#1082;&#1088;&#1072;&#1111;&#1085;&#1089;&#1100;&#1082;&#1072;', 'ukrainian', 'uk', 'ukrainian', 0, NULL),
(54, 'Ti&#7871;ng Vi&#7879;t', 'vietnamese', 'vi', 'vietnamese', 1, NULL),
(55, 'Kiswahili', 'swahili', 'sw', 'swahili', 0, NULL),
(56, 'Yor&ugrave;b&aacute;', 'yoruba', 'yo', 'yoruba', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `legal`
--

CREATE TABLE IF NOT EXISTS `legal` (
  `legal_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `date` int(11) NOT NULL DEFAULT '0',
  `content` text,
  `type` int(11) NOT NULL,
  `changes` text NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`legal_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `legal`
--


-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_sender_id` int(10) unsigned NOT NULL,
  `user_receiver_id` int(10) unsigned NOT NULL,
  `msg_status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `send_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_message_user_sender` (`user_sender_id`),
  KEY `idx_message_user_receiver` (`user_receiver_id`),
  KEY `idx_message_user_sender_user_receiver` (`user_sender_id`,`user_receiver_id`),
  KEY `idx_message_group` (`group_id`),
  KEY `idx_message_parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `message`
--


-- --------------------------------------------------------

--
-- Table structure for table `message_attachment`
--

CREATE TABLE IF NOT EXISTS `message_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `comment` text,
  `size` int(11) NOT NULL DEFAULT '0',
  `message_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `message_attachment`
--


-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dest_user_id` int(11) NOT NULL,
  `dest_mail` char(255) DEFAULT NULL,
  `title` char(255) DEFAULT NULL,
  `sender_id` int(11) NOT NULL DEFAULT '0',
  `content` char(255) DEFAULT NULL,
  `send_freq` smallint(6) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `sent_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mail_notify_sent_index` (`sent_at`),
  KEY `mail_notify_freq_index` (`sent_at`,`send_freq`,`created_at`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `notification`
--


-- --------------------------------------------------------

--
-- Table structure for table `openid_association`
--

CREATE TABLE IF NOT EXISTS `openid_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idp_endpoint_uri` text NOT NULL,
  `session_type` varchar(30) NOT NULL,
  `assoc_handle` text NOT NULL,
  `assoc_type` text NOT NULL,
  `expires_in` bigint(20) NOT NULL,
  `mac_key` text NOT NULL,
  `created` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `openid_association`
--


-- --------------------------------------------------------

--
-- Table structure for table `personal_agenda`
--

CREATE TABLE IF NOT EXISTS `personal_agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned DEFAULT NULL,
  `title` text,
  `text` text,
  `date` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `course` varchar(255) DEFAULT NULL,
  `parent_event_id` int(11) DEFAULT NULL,
  `all_day` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_personal_agenda_user` (`user`),
  KEY `idx_personal_agenda_parent` (`parent_event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `personal_agenda`
--


-- --------------------------------------------------------

--
-- Table structure for table `personal_agenda_repeat`
--

CREATE TABLE IF NOT EXISTS `personal_agenda_repeat` (
  `cal_id` int(11) NOT NULL DEFAULT '0',
  `cal_type` varchar(20) DEFAULT NULL,
  `cal_end` int(11) DEFAULT NULL,
  `cal_frequency` int(11) DEFAULT '1',
  `cal_days` char(7) DEFAULT NULL,
  PRIMARY KEY (`cal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personal_agenda_repeat`
--


-- --------------------------------------------------------

--
-- Table structure for table `personal_agenda_repeat_not`
--

CREATE TABLE IF NOT EXISTS `personal_agenda_repeat_not` (
  `cal_id` int(11) NOT NULL,
  `cal_date` int(11) NOT NULL,
  PRIMARY KEY (`cal_id`,`cal_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personal_agenda_repeat_not`
--


-- --------------------------------------------------------

--
-- Table structure for table `php_session`
--

CREATE TABLE IF NOT EXISTS `php_session` (
  `session_id` varchar(32) NOT NULL DEFAULT '',
  `session_name` varchar(10) NOT NULL DEFAULT '',
  `session_time` int(11) NOT NULL DEFAULT '0',
  `session_start` int(11) NOT NULL DEFAULT '0',
  `session_value` mediumtext NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `php_session`
--


-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE IF NOT EXISTS `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `career_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `promotion`
--


-- --------------------------------------------------------

--
-- Table structure for table `question_field`
--

CREATE TABLE IF NOT EXISTS `question_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_type` int(11) NOT NULL DEFAULT '1',
  `field_variable` varchar(64) NOT NULL,
  `field_display_text` varchar(64) DEFAULT NULL,
  `field_default_value` text,
  `field_order` int(11) DEFAULT NULL,
  `field_visible` tinyint(4) DEFAULT '0',
  `field_changeable` tinyint(4) DEFAULT '0',
  `field_filter` tinyint(4) DEFAULT '0',
  `field_loggeable` int(11) DEFAULT '0',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `question_field`
--


-- --------------------------------------------------------

--
-- Table structure for table `question_field_options`
--

CREATE TABLE IF NOT EXISTS `question_field_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `option_value` text,
  `option_display_text` varchar(255) DEFAULT NULL,
  `option_order` int(11) DEFAULT NULL,
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_question_field_options_field_id` (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `question_field_options`
--


-- --------------------------------------------------------

--
-- Table structure for table `question_field_values`
--

CREATE TABLE IF NOT EXISTS `question_field_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` text,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(100) DEFAULT '',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_question_field_values_question_id` (`question_id`),
  KEY `idx_question_field_values_field_id` (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `question_field_values`
--


-- --------------------------------------------------------

--
-- Table structure for table `reservation_category`
--

CREATE TABLE IF NOT EXISTS `reservation_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `reservation_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `reservation_category_rights`
--

CREATE TABLE IF NOT EXISTS `reservation_category_rights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `m_items` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `reservation_category_rights`
--


-- --------------------------------------------------------

--
-- Table structure for table `reservation_item`
--

CREATE TABLE IF NOT EXISTS `reservation_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `course_code` varchar(40) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `blackout` tinyint(4) NOT NULL DEFAULT '0',
  `creator` int(10) unsigned NOT NULL DEFAULT '0',
  `always_available` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `reservation_item`
--


-- --------------------------------------------------------

--
-- Table structure for table `reservation_item_rights`
--

CREATE TABLE IF NOT EXISTS `reservation_item_rights` (
  `item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_right` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `delete_right` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `m_reservation` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `view_right` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`,`class_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservation_item_rights`
--


-- --------------------------------------------------------

--
-- Table structure for table `reservation_main`
--

CREATE TABLE IF NOT EXISTS `reservation_main` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subid` int(10) unsigned NOT NULL DEFAULT '0',
  `item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `auto_accept` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `max_users` int(10) unsigned NOT NULL DEFAULT '1',
  `start_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscribe_from` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscribe_until` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscribers` int(10) unsigned NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `timepicker` tinyint(4) NOT NULL DEFAULT '0',
  `timepicker_min` int(11) NOT NULL DEFAULT '0',
  `timepicker_max` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `reservation_main`
--


-- --------------------------------------------------------

--
-- Table structure for table `reservation_subscription`
--

CREATE TABLE IF NOT EXISTS `reservation_subscription` (
  `dummy` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `reservation_id` int(10) unsigned NOT NULL DEFAULT '0',
  `accepted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `start_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`dummy`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `reservation_subscription`
--


-- --------------------------------------------------------

--
-- Table structure for table `search_engine_ref`
--

CREATE TABLE IF NOT EXISTS `search_engine_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_code` varchar(40) NOT NULL,
  `tool_id` varchar(100) NOT NULL,
  `ref_id_high_level` int(11) NOT NULL,
  `ref_id_second_level` int(11) DEFAULT NULL,
  `search_did` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `search_engine_ref`
--


-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_coach` int(10) unsigned NOT NULL DEFAULT '0',
  `name` char(150) NOT NULL DEFAULT '',
  `nbr_courses` int(10) unsigned NOT NULL DEFAULT '0',
  `nbr_users` int(10) unsigned NOT NULL DEFAULT '0',
  `nbr_classes` int(10) unsigned NOT NULL DEFAULT '0',
  `session_admin_id` int(10) unsigned NOT NULL,
  `visibility` int(11) NOT NULL DEFAULT '1',
  `session_category_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `display_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `display_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coach_access_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coach_access_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `idx_id_coach` (`id_coach`),
  KEY `idx_id_session_admin_id` (`session_admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `session`
--


-- --------------------------------------------------------

--
-- Table structure for table `session_category`
--

CREATE TABLE IF NOT EXISTS `session_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `access_url_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `session_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `session_field`
--

CREATE TABLE IF NOT EXISTS `session_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_type` int(11) NOT NULL DEFAULT '1',
  `field_variable` varchar(64) NOT NULL,
  `field_display_text` varchar(64) DEFAULT NULL,
  `field_default_value` text,
  `field_order` int(11) DEFAULT NULL,
  `field_visible` tinyint(4) DEFAULT '0',
  `field_changeable` tinyint(4) DEFAULT '0',
  `field_filter` tinyint(4) DEFAULT '0',
  `field_loggeable` int(11) DEFAULT '0',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `session_field`
--


-- --------------------------------------------------------

--
-- Table structure for table `session_field_options`
--

CREATE TABLE IF NOT EXISTS `session_field_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `option_value` text,
  `option_display_text` varchar(255) DEFAULT NULL,
  `option_order` int(11) DEFAULT NULL,
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_session_field_options_field_id` (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `session_field_options`
--


-- --------------------------------------------------------

--
-- Table structure for table `session_field_values`
--

CREATE TABLE IF NOT EXISTS `session_field_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` text,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(100) DEFAULT '',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_session_field_values_session_id` (`session_id`),
  KEY `idx_session_field_values_field_id` (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `session_field_values`
--


-- --------------------------------------------------------

--
-- Table structure for table `session_rel_course`
--

CREATE TABLE IF NOT EXISTS `session_rel_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_session` int(10) unsigned NOT NULL DEFAULT '0',
  `c_id` int(11) NOT NULL DEFAULT '0',
  `nbr_users` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_session_rel_course_course_id` (`c_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `session_rel_course`
--


-- --------------------------------------------------------

--
-- Table structure for table `session_rel_course_rel_user`
--

CREATE TABLE IF NOT EXISTS `session_rel_course_rel_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_session` int(10) unsigned NOT NULL DEFAULT '0',
  `c_id` int(11) NOT NULL DEFAULT '0',
  `id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `visibility` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '0',
  `legal_agreement` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_session_rel_course_rel_user_id_user` (`id_user`),
  KEY `idx_session_rel_course_rel_user_course_id` (`c_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `session_rel_course_rel_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `session_rel_user`
--

CREATE TABLE IF NOT EXISTS `session_rel_user` (
  `id_session` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `relation_type` int(11) NOT NULL DEFAULT '0',
  `moved_to` int(11) DEFAULT '0',
  `moved_status` int(11) DEFAULT '0',
  `moved_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_session`,`id_user`,`relation_type`),
  KEY `idx_session_rel_user_id_user_moved` (`id_user`,`moved_to`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session_rel_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `settings_current`
--

CREATE TABLE IF NOT EXISTS `settings_current` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `variable` varchar(255) DEFAULT NULL,
  `subkey` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `selected_value` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `comment` varchar(255) DEFAULT NULL,
  `scope` varchar(50) DEFAULT NULL,
  `subkeytext` varchar(255) DEFAULT NULL,
  `access_url` int(10) unsigned NOT NULL DEFAULT '1',
  `access_url_changeable` int(10) unsigned NOT NULL DEFAULT '0',
  `access_url_locked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_setting` (`variable`(110),`subkey`(110),`category`(110),`access_url`),
  KEY `access_url` (`access_url`),
  KEY `idx_settings_current_au_cat` (`access_url`,`category`(5))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=306 ;

--
-- Dumping data for table `settings_current`
--

INSERT INTO `settings_current` (`id`, `variable`, `subkey`, `type`, `category`, `selected_value`, `title`, `comment`, `scope`, `subkeytext`, `access_url`, `access_url_changeable`, `access_url_locked`) VALUES
(1, 'Institution', NULL, 'textfield', 'Platform', 'Schoolaf', 'InstitutionTitle', 'InstitutionComment', 'platform', NULL, 1, 1, 0),
(2, 'InstitutionUrl', NULL, 'textfield', 'Platform', 'http://www.schoolaf.com', 'InstitutionUrlTitle', 'InstitutionUrlComment', NULL, NULL, 1, 1, 0),
(3, 'siteName', NULL, 'textfield', 'Platform', 'Schoolaf', 'SiteNameTitle', 'SiteNameComment', NULL, NULL, 1, 1, 0),
(4, 'emailAdministrator', NULL, 'textfield', 'Platform', 'qtoantin5@gmail.com', 'emailAdministratorTitle', 'emailAdministratorComment', NULL, NULL, 1, 1, 0),
(5, 'administratorSurname', NULL, 'textfield', 'Platform', 'Toan', 'administratorSurnameTitle', 'administratorSurnameComment', NULL, NULL, 1, 1, 0),
(6, 'administratorName', NULL, 'textfield', 'Platform', 'Tran', 'administratorNameTitle', 'administratorNameComment', NULL, NULL, 1, 1, 0),
(7, 'show_administrator_data', NULL, 'radio', 'Platform', 'true', 'ShowAdministratorDataTitle', 'ShowAdministratorDataComment', NULL, NULL, 1, 1, 0),
(8, 'show_tutor_data', NULL, 'radio', 'Session', 'true', 'ShowTutorDataTitle', 'ShowTutorDataComment', NULL, NULL, 1, 1, 0),
(9, 'show_teacher_data', NULL, 'radio', 'Platform', 'true', 'ShowTeacherDataTitle', 'ShowTeacherDataComment', NULL, NULL, 1, 1, 0),
(10, 'homepage_view', NULL, 'radio', 'Course', 'activity_big', 'HomepageViewTitle', 'HomepageViewComment', NULL, NULL, 1, 1, 0),
(11, 'show_toolshortcuts', NULL, 'radio', 'Course', 'false', 'ShowToolShortcutsTitle', 'ShowToolShortcutsComment', NULL, NULL, 1, 0, 0),
(12, 'allow_group_categories', NULL, 'radio', 'Course', 'false', 'AllowGroupCategories', 'AllowGroupCategoriesComment', NULL, NULL, 1, 0, 0),
(13, 'server_type', NULL, 'radio', 'Platform', 'production', 'ServerStatusTitle', 'ServerStatusComment', NULL, NULL, 1, 0, 0),
(14, 'platformLanguage', NULL, 'link', 'Languages', 'vietnamese', 'PlatformLanguageTitle', 'PlatformLanguageComment', NULL, NULL, 1, 0, 0),
(15, 'showonline', 'world', 'checkbox', 'Platform', 'true', 'ShowOnlineTitle', 'ShowOnlineComment', NULL, 'ShowOnlineWorld', 1, 0, 0),
(16, 'showonline', 'users', 'checkbox', 'Platform', 'true', 'ShowOnlineTitle', 'ShowOnlineComment', NULL, 'ShowOnlineUsers', 1, 0, 0),
(17, 'showonline', 'course', 'checkbox', 'Platform', 'true', 'ShowOnlineTitle', 'ShowOnlineComment', NULL, 'ShowOnlineCourse', 1, 0, 0),
(18, 'profile', 'name', 'checkbox', 'User', 'false', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'name', 1, 0, 0),
(19, 'profile', 'officialcode', 'checkbox', 'User', 'false', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'officialcode', 1, 0, 0),
(20, 'profile', 'email', 'checkbox', 'User', 'false', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'Email', 1, 0, 0),
(21, 'profile', 'picture', 'checkbox', 'User', 'true', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'UserPicture', 1, 0, 0),
(22, 'profile', 'login', 'checkbox', 'User', 'false', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'Login', 1, 0, 0),
(23, 'profile', 'password', 'checkbox', 'User', 'true', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'UserPassword', 1, 0, 0),
(24, 'profile', 'language', 'checkbox', 'User', 'true', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'Language', 1, 0, 0),
(25, 'default_document_quotum', NULL, 'textfield', 'Course', '100000000', 'DefaultDocumentQuotumTitle', 'DefaultDocumentQuotumComment', NULL, NULL, 1, 0, 0),
(26, 'registration', 'officialcode', 'checkbox', 'User', 'false', 'RegistrationRequiredFormsTitle', 'RegistrationRequiredFormsComment', NULL, 'OfficialCode', 1, 0, 0),
(27, 'registration', 'email', 'checkbox', 'User', 'true', 'RegistrationRequiredFormsTitle', 'RegistrationRequiredFormsComment', NULL, 'Email', 1, 0, 0),
(28, 'registration', 'language', 'checkbox', 'User', 'true', 'RegistrationRequiredFormsTitle', 'RegistrationRequiredFormsComment', NULL, 'Language', 1, 0, 0),
(29, 'default_group_quotum', NULL, 'textfield', 'Course', '5000000', 'DefaultGroupQuotumTitle', 'DefaultGroupQuotumComment', NULL, NULL, 1, 0, 0),
(30, 'allow_registration', NULL, 'radio', 'Platform', 'true', 'AllowRegistrationTitle', 'AllowRegistrationComment', NULL, NULL, 1, 0, 0),
(31, 'allow_registration_as_teacher', NULL, 'radio', 'Platform', 'true', 'AllowRegistrationAsTeacherTitle', 'AllowRegistrationAsTeacherComment', NULL, NULL, 1, 0, 0),
(32, 'allow_lostpassword', NULL, 'radio', 'Platform', 'true', 'AllowLostPasswordTitle', 'AllowLostPasswordComment', NULL, NULL, 1, 0, 0),
(33, 'allow_user_headings', NULL, 'radio', 'Course', 'false', 'AllowUserHeadings', 'AllowUserHeadingsComment', NULL, NULL, 1, 0, 0),
(34, 'course_create_active_tools', 'course_description', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'CourseDescription', 1, 0, 0),
(35, 'course_create_active_tools', 'agenda', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Agenda', 1, 0, 0),
(36, 'course_create_active_tools', 'documents', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Documents', 1, 0, 0),
(37, 'course_create_active_tools', 'learning_path', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'LearningPath', 1, 0, 0),
(38, 'course_create_active_tools', 'links', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Links', 1, 0, 0),
(39, 'course_create_active_tools', 'announcements', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Announcements', 1, 0, 0),
(40, 'course_create_active_tools', 'forums', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Forums', 1, 0, 0),
(41, 'course_create_active_tools', 'dropbox', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Dropbox', 1, 0, 0),
(42, 'course_create_active_tools', 'quiz', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Quiz', 1, 0, 0),
(43, 'course_create_active_tools', 'users', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Users', 1, 0, 0),
(44, 'course_create_active_tools', 'groups', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Groups', 1, 0, 0),
(45, 'course_create_active_tools', 'chat', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Chat', 1, 0, 0),
(46, 'course_create_active_tools', 'online_conference', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'OnlineConference', 1, 0, 0),
(47, 'course_create_active_tools', 'student_publications', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'StudentPublications', 1, 0, 0),
(48, 'allow_personal_agenda', NULL, 'radio', 'User', 'true', 'AllowPersonalAgendaTitle', 'AllowPersonalAgendaComment', NULL, NULL, 1, 0, 0),
(49, 'display_coursecode_in_courselist', NULL, 'radio', 'Platform', 'false', 'DisplayCourseCodeInCourselistTitle', 'DisplayCourseCodeInCourselistComment', NULL, NULL, 1, 0, 0),
(50, 'display_teacher_in_courselist', NULL, 'radio', 'Platform', 'true', 'DisplayTeacherInCourselistTitle', 'DisplayTeacherInCourselistComment', NULL, NULL, 1, 0, 0),
(51, 'permanently_remove_deleted_files', NULL, 'radio', 'Tools', 'false', 'PermanentlyRemoveFilesTitle', 'PermanentlyRemoveFilesComment', NULL, NULL, 1, 0, 0),
(52, 'dropbox_allow_overwrite', NULL, 'radio', 'Tools', 'true', 'DropboxAllowOverwriteTitle', 'DropboxAllowOverwriteComment', NULL, NULL, 1, 0, 0),
(53, 'dropbox_max_filesize', NULL, 'textfield', 'Tools', '100000000', 'DropboxMaxFilesizeTitle', 'DropboxMaxFilesizeComment', NULL, NULL, 1, 0, 0),
(54, 'dropbox_allow_just_upload', NULL, 'radio', 'Tools', 'true', 'DropboxAllowJustUploadTitle', 'DropboxAllowJustUploadComment', NULL, NULL, 1, 0, 0),
(55, 'dropbox_allow_student_to_student', NULL, 'radio', 'Tools', 'true', 'DropboxAllowStudentToStudentTitle', 'DropboxAllowStudentToStudentComment', NULL, NULL, 1, 0, 0),
(56, 'dropbox_allow_group', NULL, 'radio', 'Tools', 'true', 'DropboxAllowGroupTitle', 'DropboxAllowGroupComment', NULL, NULL, 1, 0, 0),
(57, 'dropbox_allow_mailing', NULL, 'radio', 'Tools', 'false', 'DropboxAllowMailingTitle', 'DropboxAllowMailingComment', NULL, NULL, 1, 0, 0),
(58, 'administratorTelephone', NULL, 'textfield', 'Platform', '(000) 001 02 03', 'administratorTelephoneTitle', 'administratorTelephoneComment', NULL, NULL, 1, 1, 0),
(59, 'extended_profile', NULL, 'radio', 'User', 'false', 'ExtendedProfileTitle', 'ExtendedProfileComment', NULL, NULL, 1, 0, 0),
(60, 'student_view_enabled', NULL, 'radio', 'Platform', 'true', 'StudentViewEnabledTitle', 'StudentViewEnabledComment', NULL, NULL, 1, 0, 0),
(61, 'show_navigation_menu', NULL, 'radio', 'Course', 'false', 'ShowNavigationMenuTitle', 'ShowNavigationMenuComment', NULL, NULL, 1, 0, 0),
(62, 'enable_tool_introduction', NULL, 'radio', 'course', 'false', 'EnableToolIntroductionTitle', 'EnableToolIntroductionComment', NULL, NULL, 1, 0, 0),
(63, 'page_after_login', NULL, 'radio', 'Platform', 'user_portal.php', 'PageAfterLoginTitle', 'PageAfterLoginComment', NULL, NULL, 1, 0, 0),
(64, 'time_limit_whosonline', NULL, 'textfield', 'Platform', '30', 'TimeLimitWhosonlineTitle', 'TimeLimitWhosonlineComment', NULL, NULL, 1, 0, 0),
(65, 'breadcrumbs_course_homepage', NULL, 'radio', 'Course', 'course_title', 'BreadCrumbsCourseHomepageTitle', 'BreadCrumbsCourseHomepageComment', NULL, NULL, 1, 0, 0),
(66, 'example_material_course_creation', NULL, 'radio', 'Platform', 'true', 'ExampleMaterialCourseCreationTitle', 'ExampleMaterialCourseCreationComment', NULL, NULL, 1, 0, 0),
(67, 'account_valid_duration', NULL, 'textfield', 'Platform', '3660', 'AccountValidDurationTitle', 'AccountValidDurationComment', NULL, NULL, 1, 0, 0),
(68, 'use_session_mode', NULL, 'radio', 'Session', 'true', 'UseSessionModeTitle', 'UseSessionModeComment', NULL, NULL, 1, 0, 0),
(69, 'allow_email_editor', NULL, 'radio', 'Tools', 'false', 'AllowEmailEditorTitle', 'AllowEmailEditorComment', NULL, NULL, 1, 0, 0),
(70, 'registered', NULL, 'textfield', NULL, 'false', '', NULL, NULL, NULL, 1, 0, 0),
(71, 'donotlistcampus', NULL, 'textfield', NULL, 'false', '', NULL, NULL, NULL, 1, 0, 0),
(72, 'show_email_addresses', NULL, 'radio', 'Platform', 'false', 'ShowEmailAddresses', 'ShowEmailAddressesComment', NULL, NULL, 1, 1, 0),
(73, 'profile', 'phone', 'checkbox', 'User', 'false', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'phone', 1, 0, 0),
(74, 'service_visio', 'active', 'radio', NULL, 'false', 'VisioEnable', '', NULL, NULL, 1, 0, 0),
(75, 'service_visio', 'visio_host', 'textfield', NULL, '', 'VisioHost', '', NULL, NULL, 1, 0, 0),
(76, 'service_visio', 'visio_port', 'textfield', NULL, '1935', 'VisioPort', '', NULL, NULL, 1, 0, 0),
(77, 'service_visio', 'visio_pass', 'textfield', NULL, '', 'VisioPassword', '', NULL, NULL, 1, 0, 0),
(78, 'service_ppt2lp', 'active', 'radio', NULL, 'false', 'ppt2lp_actived', '', NULL, NULL, 1, 0, 0),
(79, 'service_ppt2lp', 'host', 'textfield', NULL, NULL, 'Host', NULL, NULL, NULL, 1, 0, 0),
(80, 'service_ppt2lp', 'port', 'textfield', NULL, '2002', 'Port', NULL, NULL, NULL, 1, 0, 0),
(81, 'service_ppt2lp', 'user', 'textfield', NULL, NULL, 'UserOnHost', NULL, NULL, NULL, 1, 0, 0),
(82, 'service_ppt2lp', 'ftp_password', 'textfield', NULL, NULL, 'FtpPassword', NULL, NULL, NULL, 1, 0, 0),
(83, 'service_ppt2lp', 'path_to_lzx', 'textfield', NULL, NULL, '', NULL, NULL, NULL, 1, 0, 0),
(84, 'service_ppt2lp', 'size', 'radio', NULL, '720x540', '', NULL, NULL, NULL, 1, 0, 0),
(85, 'wcag_anysurfer_public_pages', NULL, 'radio', 'Editor', 'false', 'PublicPagesComplyToWAITitle', 'PublicPagesComplyToWAIComment', NULL, NULL, 1, 0, 0),
(86, 'stylesheets', NULL, 'textfield', 'stylesheets', 'chamilo', '', NULL, NULL, NULL, 1, 1, 0),
(87, 'upload_extensions_list_type', NULL, 'radio', 'Security', 'blacklist', 'UploadExtensionsListType', 'UploadExtensionsListTypeComment', NULL, NULL, 1, 0, 0),
(88, 'upload_extensions_blacklist', NULL, 'textfield', 'Security', '', 'UploadExtensionsBlacklist', 'UploadExtensionsBlacklistComment', NULL, NULL, 1, 0, 0),
(89, 'upload_extensions_whitelist', NULL, 'textfield', 'Security', 'htm;html;jpg;jpeg;gif;png;swf;avi;mpg;mpeg;mov;flv;doc;docx;xls;xlsx;ppt;pptx;odt;odp;ods;pdf', 'UploadExtensionsWhitelist', 'UploadExtensionsWhitelistComment', NULL, NULL, 1, 0, 0),
(90, 'upload_extensions_skip', NULL, 'radio', 'Security', 'true', 'UploadExtensionsSkip', 'UploadExtensionsSkipComment', NULL, NULL, 1, 0, 0),
(91, 'upload_extensions_replace_by', NULL, 'textfield', 'Security', 'dangerous', 'UploadExtensionsReplaceBy', 'UploadExtensionsReplaceByComment', NULL, NULL, 1, 0, 0),
(92, 'show_number_of_courses', NULL, 'radio', 'Platform', 'false', 'ShowNumberOfCourses', 'ShowNumberOfCoursesComment', NULL, NULL, 1, 0, 0),
(93, 'show_empty_course_categories', NULL, 'radio', 'Platform', 'true', 'ShowEmptyCourseCategories', 'ShowEmptyCourseCategoriesComment', NULL, NULL, 1, 0, 0),
(94, 'show_back_link_on_top_of_tree', NULL, 'radio', 'Platform', 'false', 'ShowBackLinkOnTopOfCourseTree', 'ShowBackLinkOnTopOfCourseTreeComment', NULL, NULL, 1, 0, 0),
(95, 'show_different_course_language', NULL, 'radio', 'Platform', 'true', 'ShowDifferentCourseLanguage', 'ShowDifferentCourseLanguageComment', NULL, NULL, 1, 1, 0),
(96, 'split_users_upload_directory', NULL, 'radio', 'Tuning', 'true', 'SplitUsersUploadDirectory', 'SplitUsersUploadDirectoryComment', NULL, NULL, 1, 0, 0),
(97, 'hide_dltt_markup', NULL, 'radio', 'Languages', 'true', 'HideDLTTMarkup', 'HideDLTTMarkupComment', NULL, NULL, 1, 0, 0),
(98, 'display_categories_on_homepage', NULL, 'radio', 'Platform', 'false', 'DisplayCategoriesOnHomepageTitle', 'DisplayCategoriesOnHomepageComment', NULL, NULL, 1, 1, 0),
(99, 'permissions_for_new_directories', NULL, 'textfield', 'Security', '0777', 'PermissionsForNewDirs', 'PermissionsForNewDirsComment', NULL, NULL, 1, 0, 0),
(100, 'permissions_for_new_files', NULL, 'textfield', 'Security', '0666', 'PermissionsForNewFiles', 'PermissionsForNewFilesComment', NULL, NULL, 1, 0, 0),
(101, 'show_tabs', 'campus_homepage', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsCampusHomepage', 1, 1, 0),
(102, 'show_tabs', 'my_courses', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsMyCourses', 1, 1, 0),
(103, 'show_tabs', 'reporting', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsReporting', 1, 1, 0),
(104, 'show_tabs', 'platform_administration', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsPlatformAdministration', 1, 1, 0),
(105, 'show_tabs', 'my_agenda', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsMyAgenda', 1, 1, 0),
(106, 'show_tabs', 'my_profile', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsMyProfile', 1, 1, 0),
(107, 'default_forum_view', NULL, 'radio', 'Course', 'flat', 'DefaultForumViewTitle', 'DefaultForumViewComment', NULL, NULL, 1, 0, 0),
(108, 'platform_charset', NULL, 'textfield', 'Languages', 'UTF-8', 'PlatformCharsetTitle', 'PlatformCharsetComment', 'platform', NULL, 1, 0, 0),
(109, 'noreply_email_address', '', 'textfield', 'Platform', '', 'NoReplyEmailAddress', 'NoReplyEmailAddressComment', NULL, NULL, 1, 0, 0),
(110, 'survey_email_sender_noreply', '', 'radio', 'Course', 'coach', 'SurveyEmailSenderNoReply', 'SurveyEmailSenderNoReplyComment', NULL, NULL, 1, 0, 0),
(111, 'openid_authentication', NULL, 'radio', 'Security', 'false', 'OpenIdAuthentication', 'OpenIdAuthenticationComment', NULL, NULL, 1, 0, 0),
(112, 'profile', 'openid', 'checkbox', 'User', 'false', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'OpenIDURL', 1, 0, 0),
(113, 'gradebook_enable', NULL, 'radio', 'Gradebook', 'false', 'GradebookActivation', 'GradebookActivationComment', NULL, NULL, 1, 0, 0),
(114, 'show_tabs', 'my_gradebook', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsMyGradebook', 1, 1, 0),
(115, 'gradebook_score_display_coloring', 'my_display_coloring', 'checkbox', 'Gradebook', 'false', 'GradebookScoreDisplayColoring', 'GradebookScoreDisplayColoringComment', NULL, 'TabsGradebookEnableColoring', 1, 0, 0),
(116, 'gradebook_score_display_custom', 'my_display_custom', 'checkbox', 'Gradebook', 'false', 'GradebookScoreDisplayCustom', 'GradebookScoreDisplayCustomComment', NULL, 'TabsGradebookEnableCustom', 1, 0, 0),
(117, 'gradebook_score_display_colorsplit', NULL, 'textfield', 'Gradebook', '50', 'GradebookScoreDisplayColorSplit', 'GradebookScoreDisplayColorSplitComment', NULL, NULL, 1, 0, 0),
(118, 'gradebook_score_display_upperlimit', 'my_display_upperlimit', 'checkbox', 'Gradebook', 'false', 'GradebookScoreDisplayUpperLimit', 'GradebookScoreDisplayUpperLimitComment', NULL, 'TabsGradebookEnableUpperLimit', 1, 0, 0),
(119, 'gradebook_number_decimals', NULL, 'select', 'Gradebook', '0', 'GradebookNumberDecimals', 'GradebookNumberDecimalsComment', NULL, NULL, 1, 0, 0),
(120, 'user_selected_theme', NULL, 'radio', 'Platform', 'false', 'UserThemeSelection', 'UserThemeSelectionComment', NULL, NULL, 1, 0, 0),
(121, 'profile', 'theme', 'checkbox', 'User', 'false', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'UserTheme', 1, 0, 0),
(122, 'allow_course_theme', NULL, 'radio', 'Course', 'true', 'AllowCourseThemeTitle', 'AllowCourseThemeComment', NULL, NULL, 1, 0, 0),
(123, 'display_mini_month_calendar', NULL, 'radio', 'Tools', 'true', 'DisplayMiniMonthCalendarTitle', 'DisplayMiniMonthCalendarComment', NULL, NULL, 1, 0, 0),
(124, 'display_upcoming_events', NULL, 'radio', 'Tools', 'true', 'DisplayUpcomingEventsTitle', 'DisplayUpcomingEventsComment', NULL, NULL, 1, 0, 0),
(125, 'number_of_upcoming_events', NULL, 'textfield', 'Tools', '1', 'NumberOfUpcomingEventsTitle', 'NumberOfUpcomingEventsComment', NULL, NULL, 1, 0, 0),
(126, 'show_closed_courses', NULL, 'radio', 'Platform', 'false', 'ShowClosedCoursesTitle', 'ShowClosedCoursesComment', NULL, NULL, 1, 0, 0),
(127, 'service_visio', 'visio_use_rtmpt', 'radio', NULL, 'false', 'VisioUseRtmptTitle', 'VisioUseRtmptComment', NULL, NULL, 1, 0, 0),
(128, 'extendedprofile_registration', 'mycomptetences', 'checkbox', 'User', 'false', 'ExtendedProfileRegistrationTitle', 'ExtendedProfileRegistrationComment', NULL, 'MyCompetences', 1, 0, 0),
(129, 'extendedprofile_registration', 'mydiplomas', 'checkbox', 'User', 'false', 'ExtendedProfileRegistrationTitle', 'ExtendedProfileRegistrationComment', NULL, 'MyDiplomas', 1, 0, 0),
(130, 'extendedprofile_registration', 'myteach', 'checkbox', 'User', 'false', 'ExtendedProfileRegistrationTitle', 'ExtendedProfileRegistrationComment', NULL, 'MyTeach', 1, 0, 0),
(131, 'extendedprofile_registration', 'mypersonalopenarea', 'checkbox', 'User', 'false', 'ExtendedProfileRegistrationTitle', 'ExtendedProfileRegistrationComment', NULL, 'MyPersonalOpenArea', 1, 0, 0),
(132, 'extendedprofile_registrationrequired', 'mycomptetences', 'checkbox', 'User', 'false', 'ExtendedProfileRegistrationRequiredTitle', 'ExtendedProfileRegistrationRequiredComment', NULL, 'MyCompetences', 1, 0, 0),
(133, 'extendedprofile_registrationrequired', 'mydiplomas', 'checkbox', 'User', 'false', 'ExtendedProfileRegistrationRequiredTitle', 'ExtendedProfileRegistrationRequiredComment', NULL, 'MyDiplomas', 1, 0, 0),
(134, 'extendedprofile_registrationrequired', 'myteach', 'checkbox', 'User', 'false', 'ExtendedProfileRegistrationRequiredTitle', 'ExtendedProfileRegistrationRequiredComment', NULL, 'MyTeach', 1, 0, 0),
(135, 'extendedprofile_registrationrequired', 'mypersonalopenarea', 'checkbox', 'User', 'false', 'ExtendedProfileRegistrationRequiredTitle', 'ExtendedProfileRegistrationRequiredComment', NULL, 'MyPersonalOpenArea', 1, 0, 0),
(136, 'registration', 'phone', 'textfield', 'User', 'false', 'RegistrationRequiredFormsTitle', 'RegistrationRequiredFormsComment', NULL, 'Phone', 1, 0, 0),
(137, 'add_users_by_coach', NULL, 'radio', 'Session', 'false', 'AddUsersByCoachTitle', 'AddUsersByCoachComment', NULL, NULL, 1, 0, 0),
(138, 'extend_rights_for_coach', NULL, 'radio', 'Security', 'false', 'ExtendRightsForCoachTitle', 'ExtendRightsForCoachComment', NULL, NULL, 1, 0, 0),
(139, 'extend_rights_for_coach_on_survey', NULL, 'radio', 'Security', 'true', 'ExtendRightsForCoachOnSurveyTitle', 'ExtendRightsForCoachOnSurveyComment', NULL, NULL, 1, 0, 0),
(140, 'course_create_active_tools', 'wiki', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Wiki', 1, 0, 0),
(141, 'show_session_coach', NULL, 'radio', 'Session', 'false', 'ShowSessionCoachTitle', 'ShowSessionCoachComment', NULL, NULL, 1, 0, 0),
(142, 'course_create_active_tools', 'gradebook', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Gradebook', 1, 0, 0),
(143, 'allow_users_to_create_courses', NULL, 'radio', 'Platform', 'true', 'AllowUsersToCreateCoursesTitle', 'AllowUsersToCreateCoursesComment', NULL, NULL, 1, 0, 0),
(144, 'course_create_active_tools', 'survey', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Survey', 1, 0, 0),
(145, 'course_create_active_tools', 'glossary', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Glossary', 1, 0, 0),
(146, 'course_create_active_tools', 'notebook', 'checkbox', 'Tools', 'true', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Notebook', 1, 0, 0),
(147, 'course_create_active_tools', 'attendances', 'checkbox', 'Tools', 'false', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Attendances', 1, 0, 0),
(148, 'course_create_active_tools', 'course_progress', 'checkbox', 'Tools', 'false', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'CourseProgress', 1, 0, 0),
(149, 'advanced_filemanager', NULL, 'radio', 'Editor', 'true', 'AdvancedFileManagerTitle', 'AdvancedFileManagerComment', NULL, NULL, 1, 1, 0),
(150, 'allow_reservation', NULL, 'radio', 'Tools', 'false', 'AllowReservationTitle', 'AllowReservationComment', NULL, NULL, 1, 0, 0),
(151, 'profile', 'apikeys', 'checkbox', 'User', 'false', 'ProfileChangesTitle', 'ProfileChangesComment', NULL, 'ApiKeys', 1, 0, 0),
(152, 'allow_message_tool', NULL, 'radio', 'Tools', 'true', 'AllowMessageToolTitle', 'AllowMessageToolComment', NULL, NULL, 1, 1, 0),
(153, 'allow_social_tool', NULL, 'radio', 'Tools', 'true', 'AllowSocialToolTitle', 'AllowSocialToolComment', NULL, NULL, 1, 1, 0),
(154, 'allow_students_to_browse_courses', NULL, 'radio', 'Platform', 'true', 'AllowStudentsToBrowseCoursesTitle', 'AllowStudentsToBrowseCoursesComment', NULL, NULL, 1, 1, 0),
(155, 'show_session_data', NULL, 'radio', 'Session', 'false', 'ShowSessionDataTitle', 'ShowSessionDataComment', NULL, NULL, 1, 1, 0),
(156, 'allow_use_sub_language', NULL, 'radio', 'Languages', 'false', 'AllowUseSubLanguageTitle', 'AllowUseSubLanguageComment', NULL, NULL, 1, 0, 0),
(157, 'show_glossary_in_documents', NULL, 'radio', 'Course', 'none', 'ShowGlossaryInDocumentsTitle', 'ShowGlossaryInDocumentsComment', NULL, NULL, 1, 1, 0),
(158, 'allow_terms_conditions', NULL, 'radio', 'Platform', 'false', 'AllowTermsAndConditionsTitle', 'AllowTermsAndConditionsComment', NULL, NULL, 1, 0, 0),
(159, 'course_create_active_tools', 'enable_search', 'checkbox', 'Tools', 'false', 'CourseCreateActiveToolsTitle', 'CourseCreateActiveToolsComment', NULL, 'Search', 1, 0, 0),
(160, 'search_enabled', NULL, 'radio', 'Search', 'false', 'EnableSearchTitle', 'EnableSearchComment', NULL, NULL, 1, 1, 0),
(161, 'search_prefilter_prefix', NULL, NULL, 'Search', '', 'SearchPrefilterPrefix', 'SearchPrefilterPrefixComment', NULL, NULL, 1, 0, 0),
(162, 'search_show_unlinked_results', NULL, 'radio', 'Search', 'true', 'SearchShowUnlinkedResultsTitle', 'SearchShowUnlinkedResultsComment', NULL, NULL, 1, 1, 0),
(163, 'show_courses_descriptions_in_catalog', NULL, 'radio', 'Course', 'true', 'ShowCoursesDescriptionsInCatalogTitle', 'ShowCoursesDescriptionsInCatalogComment', NULL, NULL, 1, 1, 0),
(164, 'allow_coach_to_edit_course_session', NULL, 'radio', 'Session', 'true', 'AllowCoachsToEditInsideTrainingSessions', 'AllowCoachsToEditInsideTrainingSessionsComment', NULL, NULL, 1, 0, 0),
(165, 'show_glossary_in_extra_tools', NULL, 'radio', 'Course', 'false', 'ShowGlossaryInExtraToolsTitle', 'ShowGlossaryInExtraToolsComment', NULL, NULL, 1, 1, 0),
(166, 'send_email_to_admin_when_create_course', NULL, 'radio', 'Platform', 'false', 'SendEmailToAdminTitle', 'SendEmailToAdminComment', NULL, NULL, 1, 1, 0),
(167, 'go_to_course_after_login', NULL, 'radio', 'Course', 'false', 'GoToCourseAfterLoginTitle', 'GoToCourseAfterLoginComment', NULL, NULL, 1, 0, 0),
(168, 'math_mimetex', NULL, 'radio', 'Editor', 'false', 'MathMimetexTitle', 'MathMimetexComment', NULL, NULL, 1, 0, 0),
(169, 'math_asciimathML', NULL, 'radio', 'Editor', 'false', 'MathASCIImathMLTitle', 'MathASCIImathMLComment', NULL, NULL, 1, 0, 0),
(170, 'enabled_asciisvg', NULL, 'radio', 'Editor', 'false', 'AsciiSvgTitle', 'AsciiSvgComment', NULL, NULL, 1, 0, 0),
(171, 'include_asciimathml_script', NULL, 'radio', 'Editor', 'false', 'IncludeAsciiMathMlTitle', 'IncludeAsciiMathMlComment', NULL, NULL, 1, 0, 0),
(172, 'youtube_for_students', NULL, 'radio', 'Editor', 'true', 'YoutubeForStudentsTitle', 'YoutubeForStudentsComment', NULL, NULL, 1, 0, 0),
(173, 'block_copy_paste_for_students', NULL, 'radio', 'Editor', 'false', 'BlockCopyPasteForStudentsTitle', 'BlockCopyPasteForStudentsComment', NULL, NULL, 1, 0, 0),
(174, 'more_buttons_maximized_mode', NULL, 'radio', 'Editor', 'true', 'MoreButtonsForMaximizedModeTitle', 'MoreButtonsForMaximizedModeComment', NULL, NULL, 1, 0, 0),
(175, 'students_download_folders', NULL, 'radio', 'Tools', 'true', 'AllowStudentsDownloadFoldersTitle', 'AllowStudentsDownloadFoldersComment', NULL, NULL, 1, 0, 0),
(176, 'users_copy_files', NULL, 'radio', 'Tools', 'true', 'AllowUsersCopyFilesTitle', 'AllowUsersCopyFilesComment', NULL, NULL, 1, 1, 0),
(177, 'show_tabs', 'social', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsSocial', 1, 0, 0),
(178, 'allow_students_to_create_groups_in_social', NULL, 'radio', 'Tools', 'false', 'AllowStudentsToCreateGroupsInSocialTitle', 'AllowStudentsToCreateGroupsInSocialComment', NULL, NULL, 1, 0, 0),
(179, 'allow_send_message_to_all_platform_users', NULL, 'radio', 'Tools', 'true', 'AllowSendMessageToAllPlatformUsersTitle', 'AllowSendMessageToAllPlatformUsersComment', NULL, NULL, 1, 0, 0),
(180, 'message_max_upload_filesize', NULL, 'textfield', 'Tools', '20971520', 'MessageMaxUploadFilesizeTitle', 'MessageMaxUploadFilesizeComment', NULL, NULL, 1, 0, 0),
(181, 'show_tabs', 'dashboard', 'checkbox', 'Platform', 'true', 'ShowTabsTitle', 'ShowTabsComment', NULL, 'TabsDashboard', 1, 1, 0),
(182, 'use_users_timezone', 'timezones', 'radio', 'Timezones', 'true', 'UseUsersTimezoneTitle', 'UseUsersTimezoneComment', NULL, 'Timezones', 1, 1, 0),
(183, 'timezone_value', 'timezones', 'select', 'Timezones', '', 'TimezoneValueTitle', 'TimezoneValueComment', NULL, 'Timezones', 1, 1, 0),
(184, 'allow_user_course_subscription_by_course_admin', NULL, 'radio', 'Security', 'true', 'AllowUserCourseSubscriptionByCourseAdminTitle', 'AllowUserCourseSubscriptionByCourseAdminComment', NULL, NULL, 1, 1, 0),
(185, 'show_link_bug_notification', NULL, 'radio', 'Platform', 'true', 'ShowLinkBugNotificationTitle', 'ShowLinkBugNotificationComment', NULL, NULL, 1, 0, 0),
(186, 'course_validation', NULL, 'radio', 'Platform', 'false', 'EnableCourseValidation', 'EnableCourseValidationComment', NULL, NULL, 1, 1, 0),
(187, 'course_validation_terms_and_conditions_url', NULL, 'textfield', 'Platform', '', 'CourseValidationTermsAndConditionsLink', 'CourseValidationTermsAndConditionsLinkComment', NULL, NULL, 1, 1, 0),
(188, 'sso_authentication', NULL, 'radio', 'Security', 'false', 'EnableSSOTitle', 'EnableSSOComment', NULL, NULL, 1, 1, 0),
(189, 'sso_authentication_domain', NULL, 'textfield', 'Security', '', 'SSOServerDomainTitle', 'SSOServerDomainComment', NULL, NULL, 1, 1, 0),
(190, 'sso_authentication_auth_uri', NULL, 'textfield', 'Security', '/?q=user', 'SSOServerAuthURITitle', 'SSOServerAuthURIComment', NULL, NULL, 1, 1, 0),
(191, 'sso_authentication_unauth_uri', NULL, 'textfield', 'Security', '/?q=logout', 'SSOServerUnAuthURITitle', 'SSOServerUnAuthURIComment', NULL, NULL, 1, 1, 0),
(192, 'sso_authentication_protocol', NULL, 'radio', 'Security', 'http://', 'SSOServerProtocolTitle', 'SSOServerProtocolComment', NULL, NULL, 1, 1, 0),
(193, 'enabled_wiris', NULL, 'radio', 'Editor', 'false', 'EnabledWirisTitle', 'EnabledWirisComment', NULL, NULL, 1, 0, 0),
(194, 'allow_spellcheck', NULL, 'radio', 'Editor', 'false', 'AllowSpellCheckTitle', 'AllowSpellCheckComment', NULL, NULL, 1, 0, 0),
(195, 'force_wiki_paste_as_plain_text', NULL, 'radio', 'Editor', 'false', 'ForceWikiPasteAsPlainTextTitle', 'ForceWikiPasteAsPlainTextComment', NULL, NULL, 1, 0, 0),
(196, 'enabled_googlemaps', NULL, 'radio', 'Editor', 'false', 'EnabledGooglemapsTitle', 'EnabledGooglemapsComment', NULL, NULL, 1, 0, 0),
(197, 'enabled_imgmap', NULL, 'radio', 'Editor', 'true', 'EnabledImageMapsTitle', 'EnabledImageMapsComment', NULL, NULL, 1, 0, 0),
(198, 'enabled_support_svg', NULL, 'radio', 'Tools', 'true', 'EnabledSVGTitle', 'EnabledSVGComment', NULL, NULL, 1, 0, 0),
(199, 'pdf_export_watermark_enable', NULL, 'radio', 'Platform', 'false', 'PDFExportWatermarkEnableTitle', 'PDFExportWatermarkEnableComment', 'platform', NULL, 1, 1, 0),
(200, 'pdf_export_watermark_by_course', NULL, 'radio', 'Platform', 'false', 'PDFExportWatermarkByCourseTitle', 'PDFExportWatermarkByCourseComment', 'platform', NULL, 1, 1, 0),
(201, 'pdf_export_watermark_text', NULL, 'textfield', 'Platform', '', 'PDFExportWatermarkTextTitle', 'PDFExportWatermarkTextComment', 'platform', NULL, 1, 1, 0),
(202, 'enabled_insertHtml', NULL, 'radio', 'Editor', 'true', 'EnabledInsertHtmlTitle', 'EnabledInsertHtmlComment', NULL, NULL, 1, 0, 0),
(203, 'students_export2pdf', NULL, 'radio', 'Tools', 'true', 'EnabledStudentExport2PDFTitle', 'EnabledStudentExport2PDFComment', NULL, NULL, 1, 0, 0),
(204, 'exercise_min_score', NULL, 'textfield', 'Course', '', 'ExerciseMinScoreTitle', 'ExerciseMinScoreComment', 'platform', NULL, 1, 1, 0),
(205, 'exercise_max_score', NULL, 'textfield', 'Course', '', 'ExerciseMaxScoreTitle', 'ExerciseMaxScoreComment', 'platform', NULL, 1, 1, 0),
(206, 'show_users_folders', NULL, 'radio', 'Tools', 'true', 'ShowUsersFoldersTitle', 'ShowUsersFoldersComment', NULL, NULL, 1, 0, 0),
(207, 'show_default_folders', NULL, 'radio', 'Tools', 'true', 'ShowDefaultFoldersTitle', 'ShowDefaultFoldersComment', NULL, NULL, 1, 0, 0),
(208, 'show_chat_folder', NULL, 'radio', 'Tools', 'true', 'ShowChatFolderTitle', 'ShowChatFolderComment', NULL, NULL, 1, 0, 0),
(209, 'enabled_text2audio', NULL, 'radio', 'Tools', 'false', 'Text2AudioTitle', 'Text2AudioComment', NULL, NULL, 1, 0, 0),
(210, 'course_hide_tools', 'course_description', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'CourseDescription', 1, 1, 0),
(211, 'course_hide_tools', 'calendar_event', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Agenda', 1, 1, 0),
(212, 'course_hide_tools', 'document', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Documents', 1, 1, 0),
(213, 'course_hide_tools', 'learnpath', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'LearningPath', 1, 1, 0),
(214, 'course_hide_tools', 'link', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Links', 1, 1, 0),
(215, 'course_hide_tools', 'announcement', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Announcements', 1, 1, 0),
(216, 'course_hide_tools', 'forum', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Forums', 1, 1, 0),
(217, 'course_hide_tools', 'dropbox', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Dropbox', 1, 1, 0),
(218, 'course_hide_tools', 'quiz', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Quiz', 1, 1, 0),
(219, 'course_hide_tools', 'user', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Users', 1, 1, 0),
(220, 'course_hide_tools', 'group', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Groups', 1, 1, 0),
(221, 'course_hide_tools', 'chat', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Chat', 1, 1, 0),
(222, 'course_hide_tools', 'student_publication', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'StudentPublications', 1, 1, 0),
(223, 'course_hide_tools', 'wiki', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Wiki', 1, 1, 0),
(224, 'course_hide_tools', 'gradebook', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Gradebook', 1, 1, 0),
(225, 'course_hide_tools', 'survey', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Survey', 1, 1, 0),
(226, 'course_hide_tools', 'glossary', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Glossary', 1, 1, 0),
(227, 'course_hide_tools', 'notebook', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Notebook', 1, 1, 0),
(228, 'course_hide_tools', 'attendance', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Attendances', 1, 1, 0),
(229, 'course_hide_tools', 'course_progress', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'CourseProgress', 1, 1, 0),
(230, 'course_hide_tools', 'blog_management', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Blog', 1, 1, 0),
(231, 'course_hide_tools', 'tracking', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Stats', 1, 1, 0),
(232, 'course_hide_tools', 'course_maintenance', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'Maintenance', 1, 1, 0),
(233, 'course_hide_tools', 'course_setting', 'checkbox', 'Tools', 'false', 'CourseHideToolsTitle', 'CourseHideToolsComment', NULL, 'CourseSettings', 1, 1, 0),
(234, 'enabled_support_pixlr', NULL, 'radio', 'Tools', 'false', 'EnabledPixlrTitle', 'EnabledPixlrComment', NULL, NULL, 1, 0, 0),
(235, 'show_groups_to_users', NULL, 'radio', 'Session', 'false', 'ShowGroupsToUsersTitle', 'ShowGroupsToUsersComment', NULL, NULL, 1, 0, 0),
(236, 'accessibility_font_resize', NULL, 'radio', 'Platform', 'false', 'EnableAccessibilityFontResizeTitle', 'EnableAccessibilityFontResizeComment', NULL, NULL, 1, 1, 0),
(237, 'hide_courses_in_sessions', NULL, 'radio', 'Session', 'false', 'HideCoursesInSessionsTitle', 'HideCoursesInSessionsComment', 'platform', NULL, 1, 1, 0),
(238, 'enable_quiz_scenario', NULL, 'radio', 'Course', 'false', 'EnableQuizScenarioTitle', 'EnableQuizScenarioComment', NULL, NULL, 1, 1, 0),
(239, 'enable_nanogong', NULL, 'radio', 'Tools', 'false', 'EnableNanogongTitle', 'EnableNanogongComment', NULL, NULL, 1, 0, 0),
(240, 'filter_terms', NULL, 'textarea', 'Security', '', 'FilterTermsTitle', 'FilterTermsComment', NULL, NULL, 1, 0, 0),
(241, 'header_extra_content', NULL, 'textarea', 'Tracking', '', 'HeaderExtraContentTitle', 'HeaderExtraContentComment', NULL, NULL, 1, 1, 0),
(242, 'footer_extra_content', NULL, 'textarea', 'Tracking', '', 'FooterExtraContentTitle', 'FooterExtraContentComment', NULL, NULL, 1, 1, 0),
(243, 'show_documents_preview', NULL, 'radio', 'Tools', 'false', 'ShowDocumentPreviewTitle', 'ShowDocumentPreviewComment', NULL, NULL, 1, 1, 0),
(244, 'htmlpurifier_wiki', NULL, 'radio', 'Editor', 'false', 'HtmlPurifierWikiTitle', 'HtmlPurifierWikiComment', NULL, NULL, 1, 0, 0),
(245, 'cas_activate', NULL, 'radio', 'CAS', 'false', 'CasMainActivateTitle', 'CasMainActivateComment', NULL, NULL, 1, 0, 0),
(246, 'cas_server', NULL, 'textfield', 'CAS', '', 'CasMainServerTitle', 'CasMainServerComment', NULL, NULL, 1, 0, 0),
(247, 'cas_server_uri', NULL, 'textfield', 'CAS', '', 'CasMainServerURITitle', 'CasMainServerURIComment', NULL, NULL, 1, 0, 0),
(248, 'cas_port', NULL, 'textfield', 'CAS', '', 'CasMainPortTitle', 'CasMainPortComment', NULL, NULL, 1, 0, 0),
(249, 'cas_protocol', NULL, 'radio', 'CAS', '', 'CasMainProtocolTitle', 'CasMainProtocolComment', NULL, NULL, 1, 0, 0),
(250, 'cas_add_user_activate', NULL, 'radio', 'CAS', 'false', 'CasUserAddActivateTitle', 'CasUserAddActivateComment', NULL, NULL, 1, 0, 0),
(251, 'update_user_info_cas_with_ldap', NULL, 'radio', 'CAS', 'true', 'UpdateUserInfoCasWithLdapTitle', 'UpdateUserInfoCasWithLdapComment', NULL, NULL, 1, 0, 0),
(252, 'student_page_after_login', NULL, 'textfield', 'Platform', '', 'StudentPageAfterLoginTitle', 'StudentPageAfterLoginComment', NULL, NULL, 1, 0, 0),
(253, 'teacher_page_after_login', NULL, 'textfield', 'Platform', '', 'TeacherPageAfterLoginTitle', 'TeacherPageAfterLoginComment', NULL, NULL, 1, 0, 0),
(254, 'drh_page_after_login', NULL, 'textfield', 'Platform', '', 'DRHPageAfterLoginTitle', 'DRHPageAfterLoginComment', NULL, NULL, 1, 0, 0),
(255, 'sessionadmin_page_after_login', NULL, 'textfield', 'Session', '', 'SessionAdminPageAfterLoginTitle', 'SessionAdminPageAfterLoginComment', NULL, NULL, 1, 0, 0),
(256, 'student_autosubscribe', NULL, 'textfield', 'Platform', '', 'StudentAutosubscribeTitle', 'StudentAutosubscribeComment', NULL, NULL, 1, 0, 0),
(257, 'teacher_autosubscribe', NULL, 'textfield', 'Platform', '', 'TeacherAutosubscribeTitle', 'TeacherAutosubscribeComment', NULL, NULL, 1, 0, 0),
(258, 'drh_autosubscribe', NULL, 'textfield', 'Platform', '', 'DRHAutosubscribeTitle', 'DRHAutosubscribeComment', NULL, NULL, 1, 0, 0),
(259, 'sessionadmin_autosubscribe', NULL, 'textfield', 'Session', '', 'SessionadminAutosubscribeTitle', 'SessionadminAutosubscribeComment', NULL, NULL, 1, 0, 0),
(260, 'scorm_cumulative_session_time', NULL, 'radio', 'Course', 'true', 'ScormCumulativeSessionTimeTitle', 'ScormCumulativeSessionTimeComment', NULL, NULL, 1, 0, 0),
(261, 'allow_hr_skills_management', NULL, 'radio', 'Gradebook', 'true', 'AllowHRSkillsManagementTitle', 'AllowHRSkillsManagementComment', NULL, NULL, 1, 1, 0),
(262, 'enable_help_link', NULL, 'radio', 'Platform', 'true', 'EnableHelpLinkTitle', 'EnableHelpLinkComment', NULL, NULL, 1, 0, 0),
(263, 'teachers_can_change_score_settings', NULL, 'radio', 'Gradebook', 'true', 'TeachersCanChangeScoreSettingsTitle', 'TeachersCanChangeScoreSettingsComment', NULL, NULL, 1, 1, 0),
(264, 'allow_users_to_change_email_with_no_password', NULL, 'radio', 'User', 'false', 'AllowUsersToChangeEmailWithNoPasswordTitle', 'AllowUsersToChangeEmailWithNoPasswordComment', NULL, NULL, 1, 0, 0),
(265, 'show_admin_toolbar', NULL, 'radio', 'Platform', 'show_to_admin', 'ShowAdminToolbarTitle', 'ShowAdminToolbarComment', NULL, NULL, 1, 1, 0),
(266, 'allow_global_chat', NULL, 'radio', 'Platform', 'true', 'AllowGlobalChatTitle', 'AllowGlobalChatComment', NULL, NULL, 1, 1, 0),
(267, 'languagePriority1', NULL, 'radio', 'Languages', 'course_lang', 'LanguagePriority1Title', 'LanguagePriority1Comment', NULL, NULL, 1, 0, 0),
(268, 'languagePriority2', NULL, 'radio', 'Languages', 'user_profil_lang', 'LanguagePriority2Title', 'LanguagePriority2Comment', NULL, NULL, 1, 0, 0),
(269, 'languagePriority3', NULL, 'radio', 'Languages', 'user_selected_lang', 'LanguagePriority3Title', 'LanguagePriority3Comment', NULL, NULL, 1, 0, 0),
(270, 'languagePriority4', NULL, 'radio', 'Languages', 'platform_lang', 'LanguagePriority4Title', 'LanguagePriority4Comment', NULL, NULL, 1, 0, 0),
(271, 'login_is_email', NULL, 'radio', 'Platform', 'false', 'LoginIsEmailTitle', 'LoginIsEmailComment', NULL, NULL, 1, 0, 0),
(272, 'courses_default_creation_visibility', NULL, 'radio', 'Course', '2', 'CoursesDefaultCreationVisibilityTitle', 'CoursesDefaultCreationVisibilityComment', NULL, NULL, 1, 1, 0),
(273, 'allow_browser_sniffer', NULL, 'radio', 'Tuning', 'false', 'AllowBrowserSnifferTitle', 'AllowBrowserSnifferComment', NULL, NULL, 1, 0, 0),
(274, 'enable_wami_record', NULL, 'radio', 'Tools', 'false', 'EnableWamiRecordTitle', 'EnableWamiRecordComment', NULL, NULL, 1, 0, 0),
(275, 'gradebook_enable_grade_model', NULL, 'radio', 'Gradebook', 'false', 'GradebookEnableGradeModelTitle', 'GradebookEnableGradeModelComment', NULL, NULL, 1, 1, 0),
(276, 'teachers_can_change_grade_model_settings', NULL, 'radio', 'Gradebook', 'true', 'TeachersCanChangeGradeModelSettingsTitle', 'TeachersCanChangeGradeModelSettingsComment', NULL, NULL, 1, 1, 0),
(277, 'gradebook_default_weight', NULL, 'textfield', 'Gradebook', '100', 'GradebookDefaultWeightTitle', 'GradebookDefaultWeightComment', NULL, NULL, 1, 0, 0),
(278, 'ldap_description', NULL, 'radio', 'LDAP', NULL, 'LdapDescriptionTitle', 'LdapDescriptionComment', NULL, NULL, 1, 0, 0),
(279, 'shibboleth_description', NULL, 'radio', 'Shibboleth', 'false', 'ShibbolethMainActivateTitle', 'ShibbolethMainActivateComment', NULL, NULL, 1, 0, 0),
(280, 'facebook_description', NULL, 'radio', 'Facebook', 'false', 'FacebookMainActivateTitle', 'FacebookMainActivateComment', NULL, NULL, 1, 0, 0),
(281, 'gradebook_locking_enabled', NULL, 'radio', 'Gradebook', 'false', 'GradebookEnableLockingTitle', 'GradebookEnableLockingComment', NULL, NULL, 1, 0, 0),
(282, 'gradebook_default_grade_model_id', NULL, 'select', 'Gradebook', '', 'GradebookDefaultGradeModelTitle', 'GradebookDefaultGradeModelComment', NULL, NULL, 1, 1, 0),
(283, 'allow_session_admins_to_manage_all_sessions', NULL, 'radio', 'Session', 'false', 'AllowSessionAdminsToSeeAllSessionsTitle', 'AllowSessionAdminsToSeeAllSessionsComment', NULL, NULL, 1, 1, 0),
(284, 'allow_skills_tool', NULL, 'radio', 'Platform', 'false', 'AllowSkillsToolTitle', 'AllowSkillsToolComment', NULL, NULL, 1, 1, 0),
(285, 'allow_public_certificates', NULL, 'radio', 'Course', 'false', 'AllowPublicCertificatesTitle', 'AllowPublicCertificatesComment', NULL, NULL, 1, 1, 0),
(286, 'platform_unsubscribe_allowed', NULL, 'radio', 'Platform', 'false', 'PlatformUnsubscribeTitle', 'PlatformUnsubscribeComment', NULL, NULL, 1, 1, 0),
(287, 'activate_email_template', NULL, 'radio', 'Platform', 'false', 'ActivateEmailTemplateTitle', 'ActivateEmailTemplateComment', NULL, NULL, 1, 0, 0),
(288, 'enable_iframe_inclusion', NULL, 'radio', 'Editor', 'false', 'EnableIframeInclusionTitle', 'EnableIframeInclusionComment', NULL, NULL, 1, 1, 0),
(289, 'show_hot_courses', NULL, 'radio', 'Platform', 'true', 'ShowHotCoursesTitle', 'ShowHotCoursesComment', NULL, NULL, 1, 1, 0),
(290, 'enable_webcam_clip', NULL, 'radio', 'Tools', 'false', 'EnableWebCamClipTitle', 'EnableWebCamClipComment', NULL, NULL, 1, 0, 0),
(291, 'use_custom_pages', NULL, 'radio', 'Platform', 'false', 'UseCustomPagesTitle', 'UseCustomPagesComment', NULL, NULL, 1, 1, 0),
(292, 'tool_visible_by_default_at_creation', 'documents', 'checkbox', 'Tools', 'true', 'ToolVisibleByDefaultAtCreationTitle', 'ToolVisibleByDefaultAtCreationComment', NULL, 'Documents', 1, 1, 0),
(293, 'tool_visible_by_default_at_creation', 'learning_path', 'checkbox', 'Tools', 'true', 'ToolVisibleByDefaultAtCreationTitle', 'ToolVisibleByDefaultAtCreationComment', NULL, 'LearningPath', 1, 1, 0),
(294, 'tool_visible_by_default_at_creation', 'links', 'checkbox', 'Tools', 'true', 'ToolVisibleByDefaultAtCreationTitle', 'ToolVisibleByDefaultAtCreationComment', NULL, 'Links', 1, 1, 0),
(295, 'tool_visible_by_default_at_creation', 'announcements', 'checkbox', 'Tools', 'true', 'ToolVisibleByDefaultAtCreationTitle', 'ToolVisibleByDefaultAtCreationComment', NULL, 'Announcements', 1, 1, 0),
(296, 'tool_visible_by_default_at_creation', 'forums', 'checkbox', 'Tools', 'true', 'ToolVisibleByDefaultAtCreationTitle', 'ToolVisibleByDefaultAtCreationComment', NULL, 'Forums', 1, 1, 0),
(297, 'tool_visible_by_default_at_creation', 'quiz', 'checkbox', 'Tools', 'true', 'ToolVisibleByDefaultAtCreationTitle', 'ToolVisibleByDefaultAtCreationComment', NULL, 'Quiz', 1, 1, 0),
(298, 'tool_visible_by_default_at_creation', 'gradebook', 'checkbox', 'Tools', 'true', 'ToolVisibleByDefaultAtCreationTitle', 'ToolVisibleByDefaultAtCreationComment', NULL, 'Gradebook', 1, 1, 0),
(299, 'session_tutor_reports_visibility', NULL, 'radio', 'Session', 'true', 'SessionTutorsCanSeeExpiredSessionsResultsTitle', 'SessionTutorsCanSeeExpiredSessionsResultsComment', NULL, NULL, 1, 1, 0),
(300, 'gradebook_show_percentage_in_reports', NULL, 'radio', 'Gradebook', 'true', 'GradebookShowPercentageInReportsTitle', 'GradebookShowPercentageInReportsComment', NULL, NULL, 1, 0, 0),
(301, 'session_page_enabled', NULL, 'radio', 'Session', 'true', 'SessionPageEnabledTitle', 'SessionPageEnabledComment', NULL, NULL, 1, 1, 0),
(302, 'settings_latest_update', NULL, NULL, NULL, '', '', '', NULL, NULL, 1, 0, 0),
(303, 'user_name_order', NULL, 'textfield', 'Platform', '', 'UserNameOrderTitle', 'UserNameOrderComment', NULL, NULL, 1, 1, 0),
(304, 'allow_teachers_to_create_sessions', NULL, 'radio', 'Session', 'false', 'AllowTeachersToCreateSessionsTitle', 'AllowTeachersToCreateSessionsComment', NULL, NULL, 1, 0, 0),
(305, 'chamilo_database_version', NULL, 'textfield', NULL, '1.10.0.013', 'DatabaseVersion', '', NULL, NULL, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings_options`
--

CREATE TABLE IF NOT EXISTS `settings_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `variable` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `display_text` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `unique_setting_option` (`variable`(165),`value`(165))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=324 ;

--
-- Dumping data for table `settings_options`
--

INSERT INTO `settings_options` (`id`, `variable`, `value`, `display_text`) VALUES
(1, 'show_administrator_data', 'true', 'Yes'),
(2, 'show_administrator_data', 'false', 'No'),
(3, 'show_tutor_data', 'true', 'Yes'),
(4, 'show_tutor_data', 'false', 'No'),
(5, 'show_teacher_data', 'true', 'Yes'),
(6, 'show_teacher_data', 'false', 'No'),
(7, 'homepage_view', 'activity', 'HomepageViewActivity'),
(8, 'homepage_view', '2column', 'HomepageView2column'),
(9, 'homepage_view', '3column', 'HomepageView3column'),
(10, 'homepage_view', 'vertical_activity', 'HomepageViewVerticalActivity'),
(11, 'homepage_view', 'activity_big', 'HomepageViewActivityBig'),
(12, 'show_toolshortcuts', 'true', 'Yes'),
(13, 'show_toolshortcuts', 'false', 'No'),
(14, 'allow_group_categories', 'true', 'Yes'),
(15, 'allow_group_categories', 'false', 'No'),
(16, 'server_type', 'production', 'ProductionServer'),
(17, 'server_type', 'test', 'TestServer'),
(18, 'allow_name_change', 'true', 'Yes'),
(19, 'allow_name_change', 'false', 'No'),
(20, 'allow_officialcode_change', 'true', 'Yes'),
(21, 'allow_officialcode_change', 'false', 'No'),
(22, 'allow_registration', 'true', 'Yes'),
(23, 'allow_registration', 'false', 'No'),
(24, 'allow_registration', 'approval', 'AfterApproval'),
(25, 'allow_registration_as_teacher', 'true', 'Yes'),
(26, 'allow_registration_as_teacher', 'false', 'No'),
(27, 'allow_lostpassword', 'true', 'Yes'),
(28, 'allow_lostpassword', 'false', 'No'),
(29, 'allow_user_headings', 'true', 'Yes'),
(30, 'allow_user_headings', 'false', 'No'),
(31, 'allow_personal_agenda', 'true', 'Yes'),
(32, 'allow_personal_agenda', 'false', 'No'),
(33, 'display_coursecode_in_courselist', 'true', 'Yes'),
(34, 'display_coursecode_in_courselist', 'false', 'No'),
(35, 'display_teacher_in_courselist', 'true', 'Yes'),
(36, 'display_teacher_in_courselist', 'false', 'No'),
(37, 'permanently_remove_deleted_files', 'true', 'YesWillDeletePermanently'),
(38, 'permanently_remove_deleted_files', 'false', 'NoWillDeletePermanently'),
(39, 'dropbox_allow_overwrite', 'true', 'Yes'),
(40, 'dropbox_allow_overwrite', 'false', 'No'),
(41, 'dropbox_allow_just_upload', 'true', 'Yes'),
(42, 'dropbox_allow_just_upload', 'false', 'No'),
(43, 'dropbox_allow_student_to_student', 'true', 'Yes'),
(44, 'dropbox_allow_student_to_student', 'false', 'No'),
(45, 'dropbox_allow_group', 'true', 'Yes'),
(46, 'dropbox_allow_group', 'false', 'No'),
(47, 'dropbox_allow_mailing', 'true', 'Yes'),
(48, 'dropbox_allow_mailing', 'false', 'No'),
(49, 'extended_profile', 'true', 'Yes'),
(50, 'extended_profile', 'false', 'No'),
(51, 'student_view_enabled', 'true', 'Yes'),
(52, 'student_view_enabled', 'false', 'No'),
(53, 'show_navigation_menu', 'false', 'No'),
(54, 'show_navigation_menu', 'icons', 'IconsOnly'),
(55, 'show_navigation_menu', 'text', 'TextOnly'),
(56, 'show_navigation_menu', 'iconstext', 'IconsText'),
(57, 'enable_tool_introduction', 'true', 'Yes'),
(58, 'enable_tool_introduction', 'false', 'No'),
(59, 'page_after_login', 'index.php', 'CampusHomepage'),
(60, 'page_after_login', 'user_portal.php', 'MyCourses'),
(61, 'page_after_login', 'main/auth/courses.php', 'CourseCatalog'),
(62, 'breadcrumbs_course_homepage', 'get_lang', 'CourseHomepage'),
(63, 'breadcrumbs_course_homepage', 'course_code', 'CourseCode'),
(64, 'breadcrumbs_course_homepage', 'course_title', 'CourseTitle'),
(65, 'example_material_course_creation', 'true', 'Yes'),
(66, 'example_material_course_creation', 'false', 'No'),
(67, 'use_session_mode', 'true', 'Yes'),
(68, 'use_session_mode', 'false', 'No'),
(69, 'allow_email_editor', 'true', 'Yes'),
(70, 'allow_email_editor', 'false', 'No'),
(71, 'show_email_addresses', 'true', 'Yes'),
(72, 'show_email_addresses', 'false', 'No'),
(73, 'wcag_anysurfer_public_pages', 'true', 'Yes'),
(74, 'wcag_anysurfer_public_pages', 'false', 'No'),
(75, 'upload_extensions_list_type', 'blacklist', 'Blacklist'),
(76, 'upload_extensions_list_type', 'whitelist', 'Whitelist'),
(77, 'upload_extensions_skip', 'true', 'Remove'),
(78, 'upload_extensions_skip', 'false', 'Rename'),
(79, 'show_number_of_courses', 'true', 'Yes'),
(80, 'show_number_of_courses', 'false', 'No'),
(81, 'show_empty_course_categories', 'true', 'Yes'),
(82, 'show_empty_course_categories', 'false', 'No'),
(83, 'show_back_link_on_top_of_tree', 'true', 'Yes'),
(84, 'show_back_link_on_top_of_tree', 'false', 'No'),
(85, 'show_different_course_language', 'true', 'Yes'),
(86, 'show_different_course_language', 'false', 'No'),
(87, 'split_users_upload_directory', 'true', 'Yes'),
(88, 'split_users_upload_directory', 'false', 'No'),
(89, 'hide_dltt_markup', 'false', 'No'),
(90, 'hide_dltt_markup', 'true', 'Yes'),
(91, 'display_categories_on_homepage', 'true', 'Yes'),
(92, 'display_categories_on_homepage', 'false', 'No'),
(93, 'default_forum_view', 'flat', 'Flat'),
(94, 'default_forum_view', 'threaded', 'Threaded'),
(95, 'default_forum_view', 'nested', 'Nested'),
(96, 'survey_email_sender_noreply', 'coach', 'CourseCoachEmailSender'),
(97, 'survey_email_sender_noreply', 'noreply', 'NoReplyEmailSender'),
(98, 'openid_authentication', 'true', 'Yes'),
(99, 'openid_authentication', 'false', 'No'),
(100, 'gradebook_enable', 'true', 'Yes'),
(101, 'gradebook_enable', 'false', 'No'),
(102, 'user_selected_theme', 'true', 'Yes'),
(103, 'user_selected_theme', 'false', 'No'),
(104, 'allow_course_theme', 'true', 'Yes'),
(105, 'allow_course_theme', 'false', 'No'),
(106, 'display_mini_month_calendar', 'true', 'Yes'),
(107, 'display_mini_month_calendar', 'false', 'No'),
(108, 'display_upcoming_events', 'true', 'Yes'),
(109, 'display_upcoming_events', 'false', 'No'),
(110, 'show_closed_courses', 'true', 'Yes'),
(111, 'show_closed_courses', 'false', 'No'),
(112, 'ldap_version', '2', 'LDAPVersion2'),
(113, 'ldap_version', '3', 'LDAPVersion3'),
(114, 'visio_use_rtmpt', 'true', 'Yes'),
(115, 'visio_use_rtmpt', 'false', 'No'),
(116, 'add_users_by_coach', 'true', 'Yes'),
(117, 'add_users_by_coach', 'false', 'No'),
(118, 'extend_rights_for_coach', 'true', 'Yes'),
(119, 'extend_rights_for_coach', 'false', 'No'),
(120, 'extend_rights_for_coach_on_survey', 'true', 'Yes'),
(121, 'extend_rights_for_coach_on_survey', 'false', 'No'),
(122, 'show_session_coach', 'true', 'Yes'),
(123, 'show_session_coach', 'false', 'No'),
(124, 'allow_users_to_create_courses', 'true', 'Yes'),
(125, 'allow_users_to_create_courses', 'false', 'No'),
(126, 'breadcrumbs_course_homepage', 'session_name_and_course_title', 'SessionNameAndCourseTitle'),
(127, 'advanced_filemanager', 'true', 'Yes'),
(128, 'advanced_filemanager', 'false', 'No'),
(129, 'allow_reservation', 'true', 'Yes'),
(130, 'allow_reservation', 'false', 'No'),
(131, 'allow_message_tool', 'true', 'Yes'),
(132, 'allow_message_tool', 'false', 'No'),
(133, 'allow_social_tool', 'true', 'Yes'),
(134, 'allow_social_tool', 'false', 'No'),
(135, 'allow_students_to_browse_courses', 'true', 'Yes'),
(136, 'allow_students_to_browse_courses', 'false', 'No'),
(137, 'show_email_of_teacher_or_tutor ', 'true', 'Yes'),
(138, 'show_email_of_teacher_or_tutor ', 'false', 'No'),
(139, 'show_session_data ', 'true', 'Yes'),
(140, 'show_session_data ', 'false', 'No'),
(141, 'allow_use_sub_language', 'true', 'Yes'),
(142, 'allow_use_sub_language', 'false', 'No'),
(143, 'show_glossary_in_documents', 'none', 'ShowGlossaryInDocumentsIsNone'),
(144, 'show_glossary_in_documents', 'ismanual', 'ShowGlossaryInDocumentsIsManual'),
(145, 'show_glossary_in_documents', 'isautomatic', 'ShowGlossaryInDocumentsIsAutomatic'),
(146, 'allow_terms_conditions', 'true', 'Yes'),
(147, 'allow_terms_conditions', 'false', 'No'),
(148, 'search_enabled', 'true', 'Yes'),
(149, 'search_enabled', 'false', 'No'),
(150, 'search_show_unlinked_results', 'true', 'SearchShowUnlinkedResults'),
(151, 'search_show_unlinked_results', 'false', 'SearchHideUnlinkedResults'),
(152, 'show_courses_descriptions_in_catalog', 'true', 'Yes'),
(153, 'show_courses_descriptions_in_catalog', 'false', 'No'),
(154, 'allow_coach_to_edit_course_session', 'true', 'Yes'),
(155, 'allow_coach_to_edit_course_session', 'false', 'No'),
(156, 'show_glossary_in_extra_tools', 'true', 'Yes'),
(157, 'show_glossary_in_extra_tools', 'false', 'No'),
(158, 'send_email_to_admin_when_create_course', 'true', 'Yes'),
(159, 'send_email_to_admin_when_create_course', 'false', 'No'),
(160, 'go_to_course_after_login', 'true', 'Yes'),
(161, 'go_to_course_after_login', 'false', 'No'),
(162, 'math_mimetex', 'true', 'Yes'),
(163, 'math_mimetex', 'false', 'No'),
(164, 'math_asciimathML', 'true', 'Yes'),
(165, 'math_asciimathML', 'false', 'No'),
(166, 'enabled_asciisvg', 'true', 'Yes'),
(167, 'enabled_asciisvg', 'false', 'No'),
(168, 'include_asciimathml_script', 'true', 'Yes'),
(169, 'include_asciimathml_script', 'false', 'No'),
(170, 'youtube_for_students', 'true', 'Yes'),
(171, 'youtube_for_students', 'false', 'No'),
(172, 'block_copy_paste_for_students', 'true', 'Yes'),
(173, 'block_copy_paste_for_students', 'false', 'No'),
(174, 'more_buttons_maximized_mode', 'true', 'Yes'),
(175, 'more_buttons_maximized_mode', 'false', 'No'),
(176, 'students_download_folders', 'true', 'Yes'),
(177, 'students_download_folders', 'false', 'No'),
(178, 'users_copy_files', 'true', 'Yes'),
(179, 'users_copy_files', 'false', 'No'),
(180, 'allow_students_to_create_groups_in_social', 'true', 'Yes'),
(181, 'allow_students_to_create_groups_in_social', 'false', 'No'),
(182, 'allow_send_message_to_all_platform_users', 'true', 'Yes'),
(183, 'allow_send_message_to_all_platform_users', 'false', 'No'),
(184, 'use_users_timezone', 'true', 'Yes'),
(185, 'use_users_timezone', 'false', 'No'),
(186, 'allow_user_course_subscription_by_course_admin', 'true', 'Yes'),
(187, 'allow_user_course_subscription_by_course_admin', 'false', 'No'),
(188, 'show_link_bug_notification', 'true', 'Yes'),
(189, 'show_link_bug_notification', 'false', 'No'),
(190, 'course_validation', 'true', 'Yes'),
(191, 'course_validation', 'false', 'No'),
(192, 'sso_authentication', 'true', 'Yes'),
(193, 'sso_authentication', 'false', 'No'),
(194, 'sso_authentication_protocol', 'http://', 'http://'),
(195, 'sso_authentication_protocol', 'https://', 'https://'),
(196, 'enabled_wiris', 'true', 'Yes'),
(197, 'enabled_wiris', 'false', 'No'),
(198, 'allow_spellcheck', 'true', 'Yes'),
(199, 'allow_spellcheck', 'false', 'No'),
(200, 'force_wiki_paste_as_plain_text', 'true', 'Yes'),
(201, 'force_wiki_paste_as_plain_text', 'false', 'No'),
(202, 'enabled_googlemaps', 'true', 'Yes'),
(203, 'enabled_googlemaps', 'false', 'No'),
(204, 'enabled_imgmap', 'true', 'Yes'),
(205, 'enabled_imgmap', 'false', 'No'),
(206, 'enabled_support_svg', 'true', 'Yes'),
(207, 'enabled_support_svg', 'false', 'No'),
(208, 'pdf_export_watermark_enable', 'true', 'Yes'),
(209, 'pdf_export_watermark_enable', 'false', 'No'),
(210, 'pdf_export_watermark_by_course', 'true', 'Yes'),
(211, 'pdf_export_watermark_by_course', 'false', 'No'),
(212, 'enabled_insertHtml', 'true', 'Yes'),
(213, 'enabled_insertHtml', 'false', 'No'),
(214, 'students_export2pdf', 'true', 'Yes'),
(215, 'students_export2pdf', 'false', 'No'),
(216, 'show_users_folders', 'true', 'Yes'),
(217, 'show_users_folders', 'false', 'No'),
(218, 'show_default_folders', 'true', 'Yes'),
(219, 'show_default_folders', 'false', 'No'),
(220, 'show_chat_folder', 'true', 'Yes'),
(221, 'show_chat_folder', 'false', 'No'),
(222, 'enabled_text2audio', 'true', 'Yes'),
(223, 'enabled_text2audio', 'false', 'No'),
(224, 'enabled_support_pixlr', 'true', 'Yes'),
(225, 'enabled_support_pixlr', 'false', 'No'),
(226, 'show_groups_to_users', 'true', 'Yes'),
(227, 'show_groups_to_users', 'false', 'No'),
(228, 'accessibility_font_resize', 'true', 'Yes'),
(229, 'accessibility_font_resize', 'false', 'No'),
(230, 'hide_courses_in_sessions', 'true', 'Yes'),
(231, 'hide_courses_in_sessions', 'false', 'No'),
(232, 'enable_quiz_scenario', 'true', 'Yes'),
(233, 'enable_quiz_scenario', 'false', 'No'),
(234, 'enable_nanogong', 'true', 'Yes'),
(235, 'enable_nanogong', 'false', 'No'),
(236, 'show_documents_preview', 'true', 'Yes'),
(237, 'show_documents_preview', 'false', 'No'),
(238, 'htmlpurifier_wiki', 'true', 'Yes'),
(239, 'htmlpurifier_wiki', 'false', 'No'),
(240, 'cas_activate', 'true', 'Yes'),
(241, 'cas_activate', 'false', 'No'),
(242, 'cas_protocol', 'CAS1', 'CAS1Text'),
(243, 'cas_protocol', 'CAS2', 'CAS2Text'),
(244, 'cas_protocol', 'SAML', 'SAMLText'),
(245, 'cas_add_user_activate', 'false', 'No'),
(246, 'cas_add_user_activate', 'platform', 'casAddUserActivatePlatform'),
(247, 'cas_add_user_activate', 'extldap', 'casAddUserActivateLDAP'),
(248, 'update_user_info_cas_with_ldap', 'true', 'Yes'),
(249, 'update_user_info_cas_with_ldap', 'false', 'No'),
(250, 'scorm_cumulative_session_time', 'true', 'Yes'),
(251, 'scorm_cumulative_session_time', 'false', 'No'),
(252, 'allow_hr_skills_management', 'true', 'Yes'),
(253, 'allow_hr_skills_management', 'false', 'No'),
(254, 'enable_help_link', 'true', 'Yes'),
(255, 'enable_help_link', 'false', 'No'),
(256, 'allow_users_to_change_email_with_no_password', 'true', 'Yes'),
(257, 'allow_users_to_change_email_with_no_password', 'false', 'No'),
(258, 'show_admin_toolbar', 'do_not_show', 'DoNotShow'),
(259, 'show_admin_toolbar', 'show_to_admin', 'ShowToAdminsOnly'),
(260, 'show_admin_toolbar', 'show_to_admin_and_teachers', 'ShowToAdminsAndTeachers'),
(261, 'show_admin_toolbar', 'show_to_all', 'ShowToAllUsers'),
(262, 'use_custom_pages', 'true', 'Yes'),
(263, 'use_custom_pages', 'false', 'No'),
(264, 'languagePriority1', 'platform_lang', 'PlatformLanguage'),
(265, 'languagePriority1', 'user_profil_lang', 'UserLanguage'),
(266, 'languagePriority1', 'user_selected_lang', 'UserSelectedLanguage'),
(267, 'languagePriority1', 'course_lang', 'CourseLanguage'),
(268, 'languagePriority2', 'platform_lang', 'PlatformLanguage'),
(269, 'languagePriority2', 'user_profil_lang', 'UserLanguage'),
(270, 'languagePriority2', 'user_selected_lang', 'UserSelectedLanguage'),
(271, 'languagePriority2', 'course_lang', 'CourseLanguage'),
(272, 'languagePriority3', 'platform_lang', 'PlatformLanguage'),
(273, 'languagePriority3', 'user_profil_lang', 'UserLanguage'),
(274, 'languagePriority3', 'user_selected_lang', 'UserSelectedLanguage'),
(275, 'languagePriority3', 'course_lang', 'CourseLanguage'),
(276, 'languagePriority4', 'platform_lang', 'PlatformLanguage'),
(277, 'languagePriority4', 'user_profil_lang', 'UserLanguage'),
(278, 'languagePriority4', 'user_selected_lang', 'UserSelectedLanguage'),
(279, 'languagePriority4', 'course_lang', 'CourseLanguage'),
(280, 'allow_global_chat', 'true', 'Yes'),
(281, 'allow_global_chat', 'false', 'No'),
(282, 'login_is_email', 'true', 'Yes'),
(283, 'login_is_email', 'false', 'No'),
(284, 'courses_default_creation_visibility', '3', 'OpenToTheWorld'),
(285, 'courses_default_creation_visibility', '2', 'OpenToThePlatform'),
(286, 'courses_default_creation_visibility', '1', 'Private'),
(287, 'courses_default_creation_visibility', '0', 'CourseVisibilityClosed'),
(288, 'allow_browser_sniffer', 'true', 'Yes'),
(289, 'allow_browser_sniffer', 'false', 'No'),
(290, 'enable_wami_record', 'true', 'Yes'),
(291, 'enable_wami_record', 'false', 'No'),
(292, 'teachers_can_change_score_settings', 'true', 'Yes'),
(293, 'teachers_can_change_score_settings', 'false', 'No'),
(294, 'teachers_can_change_grade_model_settings', 'true', 'Yes'),
(295, 'teachers_can_change_grade_model_settings', 'false', 'No'),
(296, 'gradebook_locking_enabled', 'true', 'Yes'),
(297, 'gradebook_locking_enabled', 'false', 'No'),
(298, 'gradebook_enable_grade_model', 'true', 'Yes'),
(299, 'gradebook_enable_grade_model', 'false', 'No'),
(300, 'allow_session_admins_to_manage_all_sessions', 'true', 'Yes'),
(301, 'allow_session_admins_to_manage_all_sessions', 'false', 'No'),
(302, 'allow_skills_tool', 'true', 'Yes'),
(303, 'allow_skills_tool', 'false', 'No'),
(304, 'allow_public_certificates', 'true', 'Yes'),
(305, 'allow_public_certificates', 'false', 'No'),
(306, 'platform_unsubscribe_allowed', 'true', 'Yes'),
(307, 'platform_unsubscribe_allowed', 'false', 'No'),
(308, 'activate_email_template', 'true', 'Yes'),
(309, 'activate_email_template', 'false', 'No'),
(310, 'enable_iframe_inclusion', 'true', 'Yes'),
(311, 'enable_iframe_inclusion', 'false', 'No'),
(312, 'show_hot_courses', 'true', 'Yes'),
(313, 'show_hot_courses', 'false', 'No'),
(314, 'enable_webcam_clip', 'true', 'Yes'),
(315, 'enable_webcam_clip', 'false', 'No'),
(316, 'session_tutor_reports_visibility', 'true', 'Yes'),
(317, 'session_tutor_reports_visibility', 'false', 'No'),
(318, 'gradebook_show_percentage_in_reports', 'true', 'Yes'),
(319, 'gradebook_show_percentage_in_reports', 'false', 'No'),
(320, 'session_page_enabled', 'true', 'Yes'),
(321, 'session_page_enabled', 'false', 'No'),
(322, 'allow_teachers_to_create_sessions', 'true', 'Yes'),
(323, 'allow_teachers_to_create_sessions', 'false', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `shared_survey`
--

CREATE TABLE IF NOT EXISTS `shared_survey` (
  `survey_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `title` text,
  `subtitle` text,
  `author` varchar(250) DEFAULT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `template` varchar(20) DEFAULT NULL,
  `intro` text,
  `surveythanks` text,
  `creation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `course_code` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`survey_id`),
  UNIQUE KEY `id` (`survey_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `shared_survey`
--


-- --------------------------------------------------------

--
-- Table structure for table `shared_survey_question`
--

CREATE TABLE IF NOT EXISTS `shared_survey_question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL DEFAULT '0',
  `survey_question` text NOT NULL,
  `survey_question_comment` text NOT NULL,
  `type` varchar(250) NOT NULL DEFAULT '',
  `display` varchar(10) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0',
  `code` varchar(40) NOT NULL DEFAULT '',
  `max_value` int(11) NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `shared_survey_question`
--


-- --------------------------------------------------------

--
-- Table structure for table `shared_survey_question_option`
--

CREATE TABLE IF NOT EXISTS `shared_survey_question_option` (
  `question_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL DEFAULT '0',
  `survey_id` int(11) NOT NULL DEFAULT '0',
  `option_text` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `shared_survey_question_option`
--


-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

CREATE TABLE IF NOT EXISTS `skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `short_code` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `access_url_id` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `skill`
--

INSERT INTO `skill` (`id`, `name`, `short_code`, `description`, `access_url_id`, `icon`) VALUES
(1, 'Root', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `skill_profile`
--

CREATE TABLE IF NOT EXISTS `skill_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `skill_profile`
--


-- --------------------------------------------------------

--
-- Table structure for table `skill_rel_gradebook`
--

CREATE TABLE IF NOT EXISTS `skill_rel_gradebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gradebook_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `skill_rel_gradebook`
--


-- --------------------------------------------------------

--
-- Table structure for table `skill_rel_profile`
--

CREATE TABLE IF NOT EXISTS `skill_rel_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `skill_rel_profile`
--


-- --------------------------------------------------------

--
-- Table structure for table `skill_rel_skill`
--

CREATE TABLE IF NOT EXISTS `skill_rel_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `relation_type` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `skill_rel_skill`
--

INSERT INTO `skill_rel_skill` (`id`, `skill_id`, `parent_id`, `relation_type`, `level`) VALUES
(1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `skill_rel_user`
--

CREATE TABLE IF NOT EXISTS `skill_rel_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `acquired_skill_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `assigned_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `skill_rel_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `specific_field`
--

CREATE TABLE IF NOT EXISTS `specific_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(1) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_specific_field__code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `specific_field`
--


-- --------------------------------------------------------

--
-- Table structure for table `specific_field_values`
--

CREATE TABLE IF NOT EXISTS `specific_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_code` varchar(40) NOT NULL,
  `tool_id` varchar(100) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `value` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `specific_field_values`
--


-- --------------------------------------------------------

--
-- Table structure for table `system_template`
--

CREATE TABLE IF NOT EXISTS `system_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `comment` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `system_template`
--

INSERT INTO `system_template` (`id`, `title`, `comment`, `image`, `content`) VALUES
(1, 'TemplateTitleCourseTitle', 'TemplateTitleCourseTitleDescription', 'coursetitle.gif', '\r\n<head>\r\n                {CSS}\r\n                <style type="text/css">\r\n                .gris_title         	{\r\n                    color: silver;\r\n                }\r\n                h1\r\n                {\r\n                    text-align: right;\r\n                }\r\n                </style>\r\n\r\n            </head>\r\n            <body>\r\n            <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n            <tbody>\r\n            <tr>\r\n            <td style="vertical-align: middle; width: 50%;" colspan="1" rowspan="1">\r\n                <h1>TITULUS 1<br>\r\n                <span class="gris_title">TITULUS 2</span><br>\r\n                </h1>\r\n            </td>\r\n            <td style="width: 50%;">\r\n                <img style="width: 100px; height: 100px;" alt="dokeos logo" src="{COURSE_DIR}images/logo_dokeos.png"></td>\r\n            </tr>\r\n            </tbody>\r\n            </table>\r\n            <p><br>\r\n            <br>\r\n            </p>\r\n            </body>\r\n'),
(2, 'TemplateTitleTeacher', 'TemplateTitleTeacherDescription', 'yourinstructor.gif', '\r\n<head>\r\n                   {CSS}\r\n                   <style type="text/css">\r\n                    .text\r\n                    {\r\n                        font-weight: normal;\r\n                    }\r\n                    </style>\r\n                </head>\r\n                <body>\r\n                    <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n                    <tbody>\r\n                    <tr>\r\n                    <td></td>\r\n                    <td style="height: 33%;"></td>\r\n                    <td></td>\r\n                    </tr>\r\n                    <tr>\r\n                    <td style="width: 25%;"></td>\r\n                    <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 33%; text-align: right; font-weight: bold;" colspan="1" rowspan="1">\r\n                    <span class="text">\r\n                    <br>\r\n                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Duis pellentesque.</span>\r\n                    </td>\r\n                    <td style="width: 25%; font-weight: bold;">\r\n                    <img style="width: 180px; height: 241px;" alt="trainer" src="{COURSE_DIR}images/trainer/trainer_case.png "></td>\r\n                    </tr>\r\n                    </tbody>\r\n                    </table>\r\n                    <p><br>\r\n                    <br>\r\n                    </p>\r\n                </body>\r\n'),
(3, 'TemplateTitleLeftList', 'TemplateTitleListLeftListDescription', 'leftlist.gif', '\r\n<head>\r\n               {CSS}\r\n           </head>\r\n            <body>\r\n                <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n                <tbody>\r\n                <tr>\r\n                <td style="width: 66%;"></td>\r\n                <td style="vertical-align: bottom; width: 33%;" colspan="1" rowspan="4">&nbsp;<img style="width: 180px; height: 248px;" alt="trainer" src="{COURSE_DIR}images/trainer/trainer_reads.png "><br>\r\n                </td>\r\n                </tr>\r\n                <tr align="right">\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 66%;">Lorem\r\n                ipsum dolor sit amet.\r\n                </td>\r\n                </tr>\r\n                <tr align="right">\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 66%;">\r\n                Vivamus\r\n                a quam.&nbsp;<br>\r\n                </td>\r\n                </tr>\r\n                <tr align="right">\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 66%;">\r\n                Proin\r\n                a est stibulum ante ipsum.</td>\r\n                </tr>\r\n                </tbody>\r\n                </table>\r\n            <p><br>\r\n            <br>\r\n            </p>\r\n            </body>\r\n'),
(4, 'TemplateTitleLeftRightList', 'TemplateTitleLeftRightListDescription', 'leftrightlist.gif', '\r\n\r\n<head>\r\n               {CSS}\r\n            </head>\r\n            <body>\r\n                <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; height: 400px; width: 720px;" border="0" cellpadding="15" cellspacing="6">\r\n                <tbody>\r\n                <tr>\r\n                <td></td>\r\n                <td style="vertical-align: top;" colspan="1" rowspan="4">&nbsp;<img style="width: 180px; height: 294px;" alt="Trainer" src="{COURSE_DIR}images/trainer/trainer_join_hands.png "><br>\r\n                </td>\r\n                <td></td>\r\n                </tr>\r\n                <tr>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 33%; text-align: right;">Lorem\r\n                ipsum dolor sit amet.\r\n                </td>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 33%; text-align: left;">\r\n                Convallis\r\n                ut.&nbsp;Cras dui magna.</td>\r\n                </tr>\r\n                <tr>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 33%; text-align: right;">\r\n                Vivamus\r\n                a quam.&nbsp;<br>\r\n                </td>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 33%; text-align: left;">\r\n                Etiam\r\n                lacinia stibulum ante.<br>\r\n                </td>\r\n                </tr>\r\n                <tr>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 33%; text-align: right;">\r\n                Proin\r\n                a est stibulum ante ipsum.</td>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 33%; text-align: left;">\r\n                Consectetuer\r\n                adipiscing elit. <br>\r\n                </td>\r\n                </tr>\r\n                </tbody>\r\n                </table>\r\n            <p><br>\r\n            <br>\r\n            </p>\r\n            </body>\r\n\r\n'),
(5, 'TemplateTitleRightList', 'TemplateTitleRightListDescription', 'rightlist.gif', '\r\n    <head>\r\n               {CSS}\r\n            </head>\r\n            <body style="direction: ltr;">\r\n                <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n                <tbody>\r\n                <tr>\r\n                <td style="vertical-align: bottom; width: 50%;" colspan="1" rowspan="4"><img style="width: 300px; height: 199px;" alt="trainer" src="{COURSE_DIR}images/trainer/trainer_points_right.png"><br>\r\n                </td>\r\n                <td style="width: 50%;"></td>\r\n                </tr>\r\n                <tr>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; text-align: left; width: 50%;">\r\n                Convallis\r\n                ut.&nbsp;Cras dui magna.</td>\r\n                </tr>\r\n                <tr>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; text-align: left; width: 50%;">\r\n                Etiam\r\n                lacinia.<br>\r\n                </td>\r\n                </tr>\r\n                <tr>\r\n                <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; text-align: left; width: 50%;">\r\n                Consectetuer\r\n                adipiscing elit. <br>\r\n                </td>\r\n                </tr>\r\n                </tbody>\r\n                </table>\r\n            <p><br>\r\n            <br>\r\n            </p>\r\n            </body>\r\n'),
(6, 'TemplateTitleDiagram', 'TemplateTitleDiagramDescription', 'diagram.gif', '\r\n    <head>\r\n                       {CSS}\r\n                    </head>\r\n\r\n                    <body>\r\n                    <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n                    <tbody>\r\n                    <tr>\r\n                    <td style="background: transparent url({IMG_DIR}faded_grey.png ) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; text-align: left; height: 33%; width: 33%;">\r\n                    <br>\r\n                    Etiam\r\n                    lacinia stibulum ante.\r\n                    Convallis\r\n                    ut.&nbsp;Cras dui magna.</td>\r\n                    <td colspan="1" rowspan="3">\r\n                        <img style="width: 350px; height: 267px;" alt="Alaska chart" src="{COURSE_DIR}images/diagrams/alaska_chart.png "></td>\r\n                    </tr>\r\n                    <tr>\r\n                    <td colspan="1" rowspan="1">\r\n                    <img style="width: 300px; height: 199px;" alt="trainer" src="{COURSE_DIR}images/trainer/trainer_points_right.png "></td>\r\n                    </tr>\r\n                    <tr>\r\n                    </tr>\r\n                    </tbody>\r\n                    </table>\r\n                    <p><br>\r\n                    <br>\r\n                    </p>\r\n                    </body>\r\n'),
(7, 'TemplateTitleDesc', 'TemplateTitleCheckListDescription', 'description.gif', '\r\n<head>\r\n                       {CSS}\r\n                    </head>\r\n                    <body>\r\n                        <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n                        <tbody>\r\n                        <tr>\r\n                        <td style="width: 50%; vertical-align: top;">\r\n                            <img style="width: 48px; height: 49px; float: left;" alt="01" src="{COURSE_DIR}images/small/01.png " hspace="5"><br>Lorem ipsum dolor sit amet<br><br><br>\r\n                            <img style="width: 48px; height: 49px; float: left;" alt="02" src="{COURSE_DIR}images/small/02.png " hspace="5">\r\n                            <br>Ut enim ad minim veniam<br><br><br>\r\n                            <img style="width: 48px; height: 49px; float: left;" alt="03" src="{COURSE_DIR}images/small/03.png " hspace="5">Duis aute irure dolor in reprehenderit<br><br><br>\r\n                            <img style="width: 48px; height: 49px; float: left;" alt="04" src="{COURSE_DIR}images/small/04.png " hspace="5">Neque porro quisquam est</td>\r\n\r\n                        <td style="vertical-align: top; width: 50%; text-align: right;" colspan="1" rowspan="1">\r\n                            <img style="width: 300px; height: 291px;" alt="Gearbox" src="{COURSE_DIR}images/diagrams/gearbox.jpg "><br></td>\r\n                        </tr><tr></tr>\r\n                        </tbody>\r\n                        </table>\r\n                        <p><br>\r\n                        <br>\r\n                        </p>\r\n                    </body>\r\n'),
(8, 'TemplateTitleCycle', 'TemplateTitleCycleDescription', 'cyclechart.gif', '\r\n<head>\r\n                   {CSS}\r\n                   <style>\r\n                   .title\r\n                   {\r\n                       color: white; font-weight: bold;\r\n                   }\r\n                   </style>\r\n                </head>\r\n\r\n\r\n                <body>\r\n                <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="8" cellspacing="6">\r\n                <tbody>\r\n                <tr>\r\n                    <td style="text-align: center; vertical-align: bottom; height: 10%;" colspan="3" rowspan="1">\r\n                        <img style="width: 250px; height: 76px;" alt="arrow" src="{COURSE_DIR}images/diagrams/top_arrow.png ">\r\n                    </td>\r\n                </tr>\r\n                <tr>\r\n                    <td style="height: 5%; width: 45%; vertical-align: top; background-color: rgb(153, 153, 153); text-align: center;">\r\n                        <span class="title">Lorem ipsum</span>\r\n                    </td>\r\n\r\n                    <td style="height: 5%; width: 10%;"></td>\r\n                    <td style="height: 5%; vertical-align: top; background-color: rgb(153, 153, 153); text-align: center;">\r\n                        <span class="title">Sed ut perspiciatis</span>\r\n                    </td>\r\n                </tr>\r\n                    <tr>\r\n                        <td style="background-color: rgb(204, 204, 255); width: 45%; vertical-align: top;">\r\n                            <ul>\r\n                                <li>dolor sit amet</li>\r\n                                <li>consectetur adipisicing elit</li>\r\n                                <li>sed do eiusmod tempor&nbsp;</li>\r\n                                <li>adipisci velit, sed quia non numquam</li>\r\n                                <li>eius modi tempora incidunt ut labore et dolore magnam</li>\r\n                            </ul>\r\n                </td>\r\n                <td style="width: 10%;"></td>\r\n                <td style="background-color: rgb(204, 204, 255); width: 45%; vertical-align: top;">\r\n                    <ul>\r\n                    <li>ut enim ad minim veniam</li>\r\n                    <li>quis nostrud exercitation</li><li>ullamco laboris nisi ut</li>\r\n                    <li> Quis autem vel eum iure reprehenderit qui in ea</li>\r\n                    <li>voluptate velit esse quam nihil molestiae consequatur,</li>\r\n                    </ul>\r\n                    </td>\r\n                    </tr>\r\n                    <tr align="center">\r\n                    <td style="height: 10%; vertical-align: top;" colspan="3" rowspan="1">\r\n                    <img style="width: 250px; height: 76px;" alt="arrow" src="{COURSE_DIR}images/diagrams/bottom_arrow.png ">&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;\r\n                </td>\r\n                </tr>\r\n                </tbody>\r\n                </table>\r\n                <p><br>\r\n                <br>\r\n                </p>\r\n                </body>\r\n'),
(9, 'TemplateTitleTimeline', 'TemplateTitleTimelineDescription', 'phasetimeline.gif', '\r\n<head>\r\n               {CSS}\r\n                <style>\r\n                .title\r\n                {\r\n                    font-weight: bold; text-align: center;\r\n                }\r\n                </style>\r\n            </head>\r\n\r\n            <body>\r\n                <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="8" cellspacing="5">\r\n                <tbody>\r\n                <tr class="title">\r\n                    <td style="vertical-align: top; height: 3%; background-color: rgb(224, 224, 224);">Lorem ipsum</td>\r\n                    <td style="height: 3%;"></td>\r\n                    <td style="vertical-align: top; height: 3%; background-color: rgb(237, 237, 237);">Perspiciatis</td>\r\n                    <td style="height: 3%;"></td>\r\n                    <td style="vertical-align: top; height: 3%; background-color: rgb(245, 245, 245);">Nemo enim</td>\r\n                </tr>\r\n\r\n                <tr>\r\n                    <td style="vertical-align: top; width: 30%; background-color: rgb(224, 224, 224);">\r\n                        <ul>\r\n                        <li>dolor sit amet</li>\r\n                        <li>consectetur</li>\r\n                        <li>adipisicing elit</li>\r\n                    </ul>\r\n                    <br>\r\n                    </td>\r\n                    <td>\r\n                        <img style="width: 32px; height: 32px;" alt="arrow" src="{COURSE_DIR}images/small/arrow.png ">\r\n                    </td>\r\n\r\n                    <td style="vertical-align: top; width: 30%; background-color: rgb(237, 237, 237);">\r\n                        <ul>\r\n                            <li>ut labore</li>\r\n                            <li>et dolore</li>\r\n                            <li>magni dolores</li>\r\n                        </ul>\r\n                    </td>\r\n                    <td>\r\n                        <img style="width: 32px; height: 32px;" alt="arrow" src="{COURSE_DIR}images/small/arrow.png ">\r\n                    </td>\r\n\r\n                    <td style="vertical-align: top; background-color: rgb(245, 245, 245); width: 30%;">\r\n                        <ul>\r\n                            <li>neque porro</li>\r\n                            <li>quisquam est</li>\r\n                            <li>qui dolorem&nbsp;&nbsp;</li>\r\n                        </ul>\r\n                        <br><br>\r\n                    </td>\r\n                </tr>\r\n                </tbody>\r\n                </table>\r\n            <p><br>\r\n            <br>\r\n            </p>\r\n            </body>\r\n'),
(10, 'TemplateTitleTable', 'TemplateTitleCheckListDescription', 'table.gif', '\r\n<head>\r\n                   {CSS}\r\n                   <style type="text/css">\r\n                .title\r\n                {\r\n                    font-weight: bold; text-align: center;\r\n                }\r\n\r\n                .items\r\n                {\r\n                    text-align: right;\r\n                }\r\n\r\n\r\n                    </style>\r\n\r\n                </head>\r\n                <body>\r\n                <br />\r\n               <h2>A table</h2>\r\n                <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px;" border="1" cellpadding="5" cellspacing="0">\r\n                <tbody>\r\n                <tr class="title">\r\n                    <td>City</td>\r\n                    <td>2005</td>\r\n                    <td>2006</td>\r\n                    <td>2007</td>\r\n                    <td>2008</td>\r\n                </tr>\r\n                <tr class="items">\r\n                    <td>Lima</td>\r\n                    <td>10,40</td>\r\n                    <td>8,95</td>\r\n                    <td>9,19</td>\r\n                    <td>9,76</td>\r\n                </tr>\r\n                <tr class="items">\r\n                <td>New York</td>\r\n                    <td>18,39</td>\r\n                    <td>17,52</td>\r\n                    <td>16,57</td>\r\n                    <td>16,60</td>\r\n                </tr>\r\n                <tr class="items">\r\n                <td>Barcelona</td>\r\n                    <td>0,10</td>\r\n                    <td>0,10</td>\r\n                    <td>0,05</td>\r\n                    <td>0,05</td>\r\n                </tr>\r\n                <tr class="items">\r\n                <td>Paris</td>\r\n                    <td>3,38</td>\r\n                    <td >3,63</td>\r\n                    <td>3,63</td>\r\n                    <td>3,54</td>\r\n                </tr>\r\n                </tbody>\r\n                </table>\r\n                <br>\r\n                </body>\r\n'),
(11, 'TemplateTitleAudio', 'TemplateTitleAudioDescription', 'audiocomment.gif', '\r\n<head>\r\n               {CSS}\r\n            </head>\r\n                   <body>\r\n                    <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n                    <tbody>\r\n                    <tr>\r\n                    <td>\r\n                    <div align="center">\r\n                    <span style="text-align: center;">\r\n                        <embed  type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" width="300" height="20" bgcolor="#FFFFFF" src="{REL_PATH}main/inc/lib/mediaplayer/player.swf" allowfullscreen="false" allowscriptaccess="always" flashvars="file={COURSE_DIR}audio/ListeningComprehension.mp3&amp;autostart=true"></embed>\r\n                    </span></div>\r\n\r\n                    <br>\r\n                    </td>\r\n                    <td colspan="1" rowspan="3"><br>\r\n                        <img style="width: 300px; height: 341px; float: right;" alt="image" src="{COURSE_DIR}images/diagrams/head_olfactory_nerve.png "><br></td>\r\n                    </tr>\r\n                    <tr>\r\n                    <td colspan="1" rowspan="1">\r\n                        <img style="width: 180px; height: 271px;" alt="trainer" src="{COURSE_DIR}images/trainer/trainer_glasses.png"><br></td>\r\n                    </tr>\r\n                    <tr>\r\n                    </tr>\r\n                    </tbody>\r\n                    </table>\r\n                    <p><br>\r\n                    <br>\r\n                    </p>\r\n                    </body>\r\n'),
(12, 'TemplateTitleVideo', 'TemplateTitleVideoDescription', 'video.gif', '\r\n<head>\r\n                {CSS}\r\n            </head>\r\n\r\n            <body>\r\n            <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 720px; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n            <tbody>\r\n            <tr>\r\n            <td style="width: 50%; vertical-align: top;">\r\n\r\n                 <div style="text-align: center;" id="player810625-parent">\r\n                    <div style="border-style: none; overflow: hidden; width: 320px; height: 240px; background-color: rgb(220, 220, 220);">\r\n\r\n                        <div id="player810625">\r\n                            <div id="player810625-config" style="overflow: hidden; display: none; visibility: hidden; width: 0px; height: 0px;">url={REL_PATH}main/default_course_document/video/flv/example.flv width=320 height=240 loop=false play=false downloadable=false fullscreen=true displayNavigation=true displayDigits=true align=left dispPlaylist=none playlistThumbs=false</div>\r\n                        </div>\r\n\r\n                        <embed\r\n                            type="application/x-shockwave-flash"\r\n                            src="{REL_PATH}main/inc/lib/mediaplayer/player.swf"\r\n                            width="320"\r\n                            height="240"\r\n                            id="single"\r\n                            name="single"\r\n                            quality="high"\r\n                            allowfullscreen="true"\r\n                            flashvars="width=320&height=240&autostart=false&file={REL_PATH}main/default_course_document/video/flv/example.flv&repeat=false&image=&showdownload=false&link={REL_PATH}main/default_course_document/video/flv/example.flv&showdigits=true&shownavigation=true&logo="\r\n                        />\r\n\r\n                    </div>\r\n                </div>\r\n\r\n            </td>\r\n            <td style="background: transparent url({IMG_DIR}faded_grey.png) repeat scroll center top; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; vertical-align: top; width: 50%;">\r\n            <h3><br>\r\n            </h3>\r\n            <h3>Lorem ipsum dolor sit amet</h3>\r\n                <ul>\r\n                <li>consectetur adipisicing elit</li>\r\n                <li>sed do eiusmod tempor incididunt</li>\r\n                <li>ut labore et dolore magna aliqua</li>\r\n                </ul>\r\n            <h3>Ut enim ad minim veniam</h3>\r\n                <ul>\r\n                <li>quis nostrud exercitation ullamco</li>\r\n                <li>laboris nisi ut aliquip ex ea commodo consequat</li>\r\n                <li>Excepteur sint occaecat cupidatat non proident</li>\r\n                </ul>\r\n            </td>\r\n            </tr>\r\n            </tbody>\r\n            </table>\r\n            <p><br>\r\n            <br>\r\n            </p>\r\n             <style type="text/css">body{}</style><!-- to fix a strange bug appearing with firefox when editing this template -->\r\n            </body>\r\n'),
(13, 'TemplateTitleFlash', 'TemplateTitleFlashDescription', 'flash.gif', '\r\n<head>\r\n               {CSS}\r\n            </head>\r\n            <body>\r\n            <center>\r\n                <table style="background: transparent url({IMG_DIR}faded_blue_horizontal.png ) repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; text-align: left; width: 100%; height: 400px;" border="0" cellpadding="15" cellspacing="6">\r\n                <tbody>\r\n                    <tr>\r\n                    <td align="center">\r\n                    <embed width="700" height="300" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="{COURSE_DIR}flash/SpinEchoSequence.swf" play="true" loop="true" menu="true"></embed></span><br />\r\n                    </td>\r\n                    </tr>\r\n                </tbody>\r\n                </table>\r\n                <p><br>\r\n                <br>\r\n                </p>\r\n            </center>\r\n            </body>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `sys_announcement`
--

CREATE TABLE IF NOT EXISTS `sys_announcement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `visible_teacher` tinyint(4) NOT NULL DEFAULT '0',
  `visible_student` tinyint(4) NOT NULL DEFAULT '0',
  `visible_guest` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `lang` varchar(70) DEFAULT NULL,
  `access_url_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sys_announcement`
--


-- --------------------------------------------------------

--
-- Table structure for table `sys_calendar`
--

CREATE TABLE IF NOT EXISTS `sys_calendar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_url_id` int(11) NOT NULL DEFAULT '1',
  `all_day` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sys_calendar`
--


-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` char(255) NOT NULL,
  `field_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tag`
--


-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `course_code` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ref_doc` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `templates`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_course_ranking`
--

CREATE TABLE IF NOT EXISTS `track_course_ranking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  `url_id` int(10) unsigned NOT NULL DEFAULT '0',
  `accesses` int(10) unsigned NOT NULL DEFAULT '0',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `users` int(10) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tcc_cid` (`c_id`),
  KEY `idx_tcc_sid` (`session_id`),
  KEY `idx_tcc_urlid` (`url_id`),
  KEY `idx_tcc_creation_date` (`creation_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `track_course_ranking`
--

INSERT INTO `track_course_ranking` (`id`, `c_id`, `session_id`, `url_id`, `accesses`, `total_score`, `users`, `creation_date`) VALUES
(1, 0, 0, 1, 34, 0, 0, '2013-05-24 17:51:03'),
(2, 1, 0, 1, 10, 0, 0, '2013-05-29 14:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `track_c_browsers`
--

CREATE TABLE IF NOT EXISTS `track_c_browsers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `browser` varchar(255) NOT NULL DEFAULT '',
  `counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_c_browsers`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_c_countries`
--

CREATE TABLE IF NOT EXISTS `track_c_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=265 ;

--
-- Dumping data for table `track_c_countries`
--

INSERT INTO `track_c_countries` (`id`, `code`, `country`, `counter`) VALUES
(1, '''ac''', '''Ascension (ile)''', 0),
(2, '''ad''', '''Andorre''', 0),
(3, '''ae''', '''Emirats  Arabes Unis''', 0),
(4, '''af''', '''Afghanistan''', 0),
(5, '''ag''', '''Antigua et Barbuda''', 0),
(6, '''ai''', '''Anguilla''', 0),
(7, '''al''', '''Albanie''', 0),
(8, '''am''', '''Arm', 0),
(9, '''an''', '''Antilles Neerlandaises''', 0),
(10, '''ao''', '''Angola''', 0),
(11, '''aq''', '''Antarctique''', 0),
(12, '''ar''', '''Argentine''', 0),
(13, '''as''', '''American Samoa''', 0),
(14, '''au''', '''Australie''', 0),
(15, '''aw''', '''Aruba''', 0),
(16, '''az''', '''Azerbaijan''', 0),
(17, '''ba''', '''Bosnie Herzegovine''', 0),
(18, '''bb''', '''Barbade''', 0),
(19, '''bd''', '''Bangladesh''', 0),
(20, '''be''', '''Belgique''', 0),
(21, '''bf''', '''Burkina Faso''', 0),
(22, '''bg''', '''Bulgarie''', 0),
(23, '''bh''', '''Bahrain''', 0),
(24, '''bi''', '''Burundi''', 0),
(25, '''bj''', '''Benin''', 0),
(26, '''bm''', '''Bermudes''', 0),
(27, '''bn''', '''Brunei Darussalam''', 0),
(28, '''bo''', '''Bolivie''', 0),
(29, '''br''', '''Br', 0),
(30, '''bs''', '''Bahamas''', 0),
(31, '''bt''', '''Bhoutan''', 0),
(32, '''bv''', '''Bouvet (ile)''', 0),
(33, '''bw''', '''Botswana''', 0),
(34, '''by''', '''Bi', 0),
(35, '''bz''', '''B', 0),
(36, '''ca''', '''Canada''', 0),
(37, '''cc''', '''Cocos (Keeling) iles''', 0),
(38, '''cd''', '''Congo', 0),
(39, '''cf''', '''Centrafricaine (R', 0),
(40, '''cg''', '''Congo''', 0),
(41, '''ch''', '''Suisse''', 0),
(42, '''ci''', '''Cote d\\''Ivoire''', 0),
(43, '''ck''', '''Cook (iles)''', 0),
(44, '''cl''', '''Chili''', 0),
(45, '''cm''', '''Cameroun''', 0),
(46, '''cn''', '''Chine''', 0),
(47, '''co''', '''Colombie''', 0),
(48, '''cr''', '''Costa Rica''', 0),
(49, '''cu''', '''Cuba''', 0),
(50, '''cv''', '''Cap Vert''', 0),
(51, '''cx''', '''Christmas (ile)''', 0),
(52, '''cy''', '''Chypre''', 0),
(53, '''cz''', '''Tch', 0),
(54, '''de''', '''Allemagne''', 0),
(55, '''dj''', '''Djibouti''', 0),
(56, '''dk''', '''Danemark''', 0),
(57, '''dm''', '''Dominique''', 0),
(58, '''do''', '''Dominicaine (r', 0),
(59, '''dz''', '''Alg', 0),
(60, '''ec''', '''Equateur''', 0),
(61, '''ee''', '''Estonie''', 0),
(62, '''eg''', '''Egypte''', 0),
(63, '''eh''', '''Sahara Occidental''', 0),
(64, '''er''', '''Erythr', 0),
(65, '''es''', '''Espagne''', 0),
(66, '''et''', '''Ethiopie''', 0),
(67, '''fi''', '''Finlande''', 0),
(68, '''fj''', '''Fiji''', 0),
(69, '''fk''', '''Falkland (Malouines) iles''', 0),
(70, '''fm''', '''Micron', 0),
(71, '''fo''', '''Faroe (iles)''', 0),
(72, '''fr''', '''France''', 0),
(73, '''ga''', '''Gabon''', 0),
(74, '''gd''', '''Grenade''', 0),
(75, '''ge''', '''G', 0),
(76, '''gf''', '''Guyane Fran', 0),
(77, '''gg''', '''Guernsey''', 0),
(78, '''gh''', '''Ghana''', 0),
(79, '''gi''', '''Gibraltar''', 0),
(80, '''gl''', '''Groenland''', 0),
(81, '''gm''', '''Gambie''', 0),
(82, '''gn''', '''Guin', 0),
(83, '''gp''', '''Guadeloupe''', 0),
(84, '''gq''', '''Guin', 0),
(85, '''gr''', '''Gr', 0),
(86, '''gs''', '''Georgie du sud et iles Sandwich du sud''', 0),
(87, '''gt''', '''Guatemala''', 0),
(88, '''gu''', '''Guam''', 0),
(89, '''gw''', '''Guin', 0),
(90, '''gy''', '''Guyana''', 0),
(91, '''hk''', '''Hong Kong''', 0),
(92, '''hm''', '''Heard et McDonald (iles)''', 0),
(93, '''hn''', '''Honduras''', 0),
(94, '''hr''', '''Croatie''', 0),
(95, '''ht''', '''Haiti''', 0),
(96, '''hu''', '''Hongrie''', 0),
(97, '''id''', '''Indon', 0),
(98, '''ie''', '''Irlande''', 0),
(99, '''il''', '''Isra', 0),
(100, '''im''', '''Ile de Man''', 0),
(101, '''in''', '''Inde''', 0),
(102, '''io''', '''Territoire Britannique de l\\''Oc', 0),
(103, '''iq''', '''Iraq''', 0),
(104, '''ir''', '''Iran''', 0),
(105, '''is''', '''Islande''', 0),
(106, '''it''', '''Italie''', 0),
(107, '''je''', '''Jersey''', 0),
(108, '''jm''', '''Jama', 0),
(109, '''jo''', '''Jordanie''', 0),
(110, '''jp''', '''Japon''', 0),
(111, '''ke''', '''Kenya''', 0),
(112, '''kg''', '''Kirgizstan''', 0),
(113, '''kh''', '''Cambodge''', 0),
(114, '''ki''', '''Kiribati''', 0),
(115, '''km''', '''Comores''', 0),
(116, '''kn''', '''Saint Kitts et Nevis''', 0),
(117, '''kp''', '''Cor', 0),
(118, '''kr''', '''Cor', 0),
(119, '''kw''', '''Kowe', 0),
(120, '''ky''', '''Ca', 0),
(121, '''kz''', '''Kazakhstan''', 0),
(122, '''la''', '''Laos''', 0),
(123, '''lb''', '''Liban''', 0),
(124, '''lc''', '''Sainte Lucie''', 0),
(125, '''li''', '''Liechtenstein''', 0),
(126, '''lk''', '''Sri Lanka''', 0),
(127, '''lr''', '''Liberia''', 0),
(128, '''ls''', '''Lesotho''', 0),
(129, '''lt''', '''Lituanie''', 0),
(130, '''lu''', '''Luxembourg''', 0),
(131, '''lv''', '''Latvia''', 0),
(132, '''ly''', '''Libyan Arab Jamahiriya''', 0),
(133, '''ma''', '''Maroc''', 0),
(134, '''mc''', '''Monaco''', 0),
(135, '''md''', '''Moldavie''', 0),
(136, '''mg''', '''Madagascar''', 0),
(137, '''mh''', '''Marshall (iles)''', 0),
(138, '''mk''', '''Mac', 0),
(139, '''ml''', '''Mali''', 0),
(140, '''mm''', '''Myanmar''', 0),
(141, '''mn''', '''Mongolie''', 0),
(142, '''mo''', '''Macao''', 0),
(143, '''mp''', '''Mariannes du nord (iles)''', 0),
(144, '''mq''', '''Martinique''', 0),
(145, '''mr''', '''Mauritanie''', 0),
(146, '''ms''', '''Montserrat''', 0),
(147, '''mt''', '''Malte''', 0),
(148, '''mu''', '''Maurice (ile)''', 0),
(149, '''mv''', '''Maldives''', 0),
(150, '''mw''', '''Malawi''', 0),
(151, '''mx''', '''Mexique''', 0),
(152, '''my''', '''Malaisie''', 0),
(153, '''mz''', '''Mozambique''', 0),
(154, '''na''', '''Namibie''', 0),
(155, '''nc''', '''Nouvelle Cal', 0),
(156, '''ne''', '''Niger''', 0),
(157, '''nf''', '''Norfolk (ile)''', 0),
(158, '''ng''', '''Nig', 0),
(159, '''ni''', '''Nicaragua''', 0),
(160, '''nl''', '''Pays Bas''', 0),
(161, '''no''', '''Norv', 0),
(162, '''np''', '''N', 0),
(163, '''nr''', '''Nauru''', 0),
(164, '''nu''', '''Niue''', 0),
(165, '''nz''', '''Nouvelle Z', 0),
(166, '''om''', '''Oman''', 0),
(167, '''pa''', '''Panama''', 0),
(168, '''pe''', '''P', 0),
(169, '''pf''', '''Polyn', 0),
(170, '''pg''', '''Papouasie Nouvelle Guin', 0),
(171, '''ph''', '''Philippines''', 0),
(172, '''pk''', '''Pakistan''', 0),
(173, '''pl''', '''Pologne''', 0),
(174, '''pm''', '''St. Pierre et Miquelon''', 0),
(175, '''pn''', '''Pitcairn (ile)''', 0),
(176, '''pr''', '''Porto Rico''', 0),
(177, '''pt''', '''Portugal''', 0),
(178, '''pw''', '''Palau''', 0),
(179, '''py''', '''Paraguay''', 0),
(180, '''qa''', '''Qatar''', 0),
(181, '''re''', '''R', 0),
(182, '''ro''', '''Roumanie''', 0),
(183, '''ru''', '''Russie''', 0),
(184, '''rw''', '''Rwanda''', 0),
(185, '''sa''', '''Arabie Saoudite''', 0),
(186, '''sb''', '''Salomon (iles)''', 0),
(187, '''sc''', '''Seychelles''', 0),
(188, '''sd''', '''Soudan''', 0),
(189, '''se''', '''Su', 0),
(190, '''sg''', '''Singapour''', 0),
(191, '''sh''', '''St. H', 0),
(192, '''si''', '''Slov', 0),
(193, '''sj''', '''Svalbard et Jan Mayen (iles)''', 0),
(194, '''sk''', '''Slovaquie''', 0),
(195, '''sl''', '''Sierra Leone''', 0),
(196, '''sm''', '''Saint Marin''', 0),
(197, '''sn''', '''S', 0),
(198, '''so''', '''Somalie''', 0),
(199, '''sr''', '''Suriname''', 0),
(200, '''st''', '''Sao Tome et Principe''', 0),
(201, '''sv''', '''Salvador''', 0),
(202, '''sy''', '''Syrie''', 0),
(203, '''sz''', '''Swaziland''', 0),
(204, '''tc''', '''Turks et Ca', 0),
(205, '''td''', '''Tchad''', 0),
(206, '''tf''', '''Territoires Fran', 0),
(207, '''tg''', '''Togo''', 0),
(208, '''th''', '''Thailande''', 0),
(209, '''tj''', '''Tajikistan''', 0),
(210, '''tk''', '''Tokelau''', 0),
(211, '''tm''', '''Turkm', 0),
(212, '''tn''', '''Tunisie''', 0),
(213, '''to''', '''Tonga''', 0),
(214, '''tp''', '''Timor Oriental''', 0),
(215, '''tr''', '''Turquie''', 0),
(216, '''tt''', '''Trinidad et Tobago''', 0),
(217, '''tv''', '''Tuvalu''', 0),
(218, '''tw''', '''Taiwan''', 0),
(219, '''tz''', '''Tanzanie''', 0),
(220, '''ua''', '''Ukraine''', 0),
(221, '''ug''', '''Ouganda''', 0),
(222, '''uk''', '''Royaume Uni''', 0),
(223, '''gb''', '''Royaume Uni''', 0),
(224, '''um''', '''US Minor Outlying (iles)''', 0),
(225, '''us''', '''Etats Unis''', 0),
(226, '''uy''', '''Uruguay''', 0),
(227, '''uz''', '''Ouzb', 0),
(228, '''va''', '''Vatican''', 0),
(229, '''vc''', '''Saint Vincent et les Grenadines''', 0),
(230, '''ve''', '''Venezuela''', 0),
(231, '''vg''', '''Vierges Britaniques (iles)''', 0),
(232, '''vi''', '''Vierges USA (iles)''', 0),
(233, '''vn''', '''Vi', 0),
(234, '''vu''', '''Vanuatu''', 0),
(235, '''wf''', '''Wallis et Futuna (iles)''', 0),
(236, '''ws''', '''Western Samoa''', 0),
(237, '''ye''', '''Yemen''', 0),
(238, '''yt''', '''Mayotte''', 0),
(239, '''yu''', '''Yugoslavie''', 0),
(240, '''za''', '''Afrique du Sud''', 0),
(241, '''zm''', '''Zambie''', 0),
(242, '''zr''', '''Za', 0),
(243, '''zw''', '''Zimbabwe''', 0),
(244, '''com''', '''.COM''', 0),
(245, '''net''', '''.NET''', 0),
(246, '''org''', '''.ORG''', 0),
(247, '''edu''', '''Education''', 0),
(248, '''int''', '''.INT''', 0),
(249, '''arpa''', '''.ARPA''', 0),
(250, '''at''', '''Autriche''', 0),
(251, '''gov''', '''Gouvernement''', 0),
(252, '''mil''', '''Militaire''', 0),
(253, '''su''', '''Ex U.R.S.S.''', 0),
(254, '''reverse''', '''Reverse''', 0),
(255, '''biz''', '''Businesses''', 0),
(256, '''info''', '''.INFO''', 0),
(257, '''name''', '''.NAME''', 0),
(258, '''pro''', '''.PRO''', 0),
(259, '''coop''', '''.COOP''', 0),
(260, '''aero''', '''.AERO''', 0),
(261, '''museum''', '''.MUSEUM''', 0),
(262, '''tv''', '''.TV''', 0),
(263, '''ws''', '''Web site''', 0),
(264, '''--''', '''Unknown''', 0);

-- --------------------------------------------------------

--
-- Table structure for table `track_c_os`
--

CREATE TABLE IF NOT EXISTS `track_c_os` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `os` varchar(255) NOT NULL DEFAULT '',
  `counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_c_os`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_c_providers`
--

CREATE TABLE IF NOT EXISTS `track_c_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(255) NOT NULL DEFAULT '',
  `counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_c_providers`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_c_referers`
--

CREATE TABLE IF NOT EXISTS `track_c_referers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referer` varchar(255) NOT NULL DEFAULT '',
  `counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_c_referers`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_access`
--

CREATE TABLE IF NOT EXISTS `track_e_access` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_user_id` int(10) unsigned DEFAULT NULL,
  `access_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `c_id` int(11) NOT NULL DEFAULT '0',
  `access_tool` varchar(30) DEFAULT NULL,
  `access_session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`access_id`),
  KEY `access_user_id` (`access_user_id`),
  KEY `access_cid_user` (`c_id`,`access_user_id`),
  KEY `access_session_id` (`access_session_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `track_e_access`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_attempt`
--

CREATE TABLE IF NOT EXISTS `track_e_attempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exe_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `teacher_comment` text NOT NULL,
  `marks` float(6,2) NOT NULL DEFAULT '0.00',
  `position` int(11) DEFAULT '0',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `session_id` int(11) NOT NULL DEFAULT '0',
  `c_id` int(10) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exe_id` (`exe_id`),
  KEY `user_id` (`user_id`),
  KEY `question_id` (`question_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_attempt`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_attempt_coeff`
--

CREATE TABLE IF NOT EXISTS `track_e_attempt_coeff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attempt_id` int(11) NOT NULL,
  `marks_coeff` float(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_attempt_coeff`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_attempt_recording`
--

CREATE TABLE IF NOT EXISTS `track_e_attempt_recording` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exe_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `marks` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `author` int(10) unsigned NOT NULL,
  `teacher_comment` text NOT NULL,
  `session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `exe_id` (`exe_id`),
  KEY `question_id` (`question_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_attempt_recording`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_course_access`
--

CREATE TABLE IF NOT EXISTS `track_e_course_access` (
  `course_access_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `login_course_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_course_date` datetime DEFAULT NULL,
  `counter` int(11) NOT NULL,
  `session_id` int(11) NOT NULL DEFAULT '0',
  `c_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`course_access_id`),
  KEY `user_id` (`user_id`),
  KEY `login_course_date` (`login_course_date`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `track_e_course_access`
--

INSERT INTO `track_e_course_access` (`course_access_id`, `user_id`, `login_course_date`, `logout_course_date`, `counter`, `session_id`, `c_id`) VALUES
(1, 1, '2013-05-24 19:51:03', '2013-05-24 19:51:03', 1, 0, 0),
(2, 1, '2013-05-24 19:51:05', '2013-05-24 19:51:05', 1, 0, 0),
(3, 1, '2013-05-24 19:52:16', '2013-05-24 19:52:16', 1, 0, 0),
(4, 1, '2013-05-24 19:52:17', '2013-05-24 19:52:17', 1, 0, 0),
(5, 1, '2013-05-24 19:52:48', '2013-05-24 19:52:48', 1, 0, 0),
(6, 1, '2013-05-24 19:52:49', '2013-05-24 19:52:49', 1, 0, 0),
(7, 1, '2013-05-24 19:53:06', '2013-05-24 19:53:06', 1, 0, 0),
(8, 1, '2013-05-24 19:53:07', '2013-05-24 19:53:07', 1, 0, 0),
(9, 1, '2013-05-24 19:53:14', '2013-05-24 19:53:14', 1, 0, 0),
(10, 1, '2013-05-24 19:56:52', '2013-05-24 19:56:52', 1, 0, 0),
(11, 1, '2013-05-24 19:57:11', '2013-05-24 19:57:11', 1, 0, 0),
(12, 1, '2013-05-25 17:54:00', '2013-05-25 17:54:00', 1, 0, 0),
(13, 1, '2013-05-25 17:54:06', '2013-05-25 17:54:06', 1, 0, 0),
(14, 1, '2013-05-25 17:54:14', '2013-05-25 17:54:14', 1, 0, 0),
(15, 1, '2013-05-25 17:54:20', '2013-05-25 17:54:20', 1, 0, 0),
(16, 1, '2013-05-25 17:54:25', '2013-05-25 17:54:25', 1, 0, 0),
(17, 1, '2013-05-25 17:54:34', '2013-05-25 17:54:34', 1, 0, 0),
(18, 1, '2013-05-25 19:49:16', '2013-05-25 19:49:16', 1, 0, 0),
(19, 1, '2013-05-25 19:49:24', '2013-05-25 19:49:24', 1, 0, 0),
(20, 1, '2013-05-25 19:49:33', '2013-05-25 19:49:33', 1, 0, 0),
(21, 1, '2013-05-25 19:49:40', '2013-05-25 19:49:40', 1, 0, 0),
(22, 1, '2013-05-25 19:49:51', '2013-05-25 19:49:51', 1, 0, 0),
(23, 1, '2013-05-25 19:50:04', '2013-05-25 19:50:04', 1, 0, 0),
(24, 1, '2013-05-29 16:34:52', '2013-05-29 16:34:52', 1, 0, 0),
(25, 1, '2013-05-29 16:34:53', '2013-05-29 16:34:53', 1, 0, 0),
(26, 1, '2013-05-29 16:37:33', '2013-05-29 16:37:33', 1, 0, 0),
(27, 1, '2013-05-29 16:37:34', '2013-05-29 16:37:34', 1, 0, 0),
(28, 1, '2013-05-29 16:37:50', '2013-05-29 16:37:50', 1, 0, 0),
(29, 1, '2013-05-29 16:37:51', '2013-05-29 16:37:51', 1, 0, 0),
(55, 1, '2013-05-29 19:14:19', '2013-05-29 19:14:19', 1, 0, 0),
(51, 1, '2013-05-29 17:23:06', '2013-05-29 17:23:06', 1, 0, 0),
(52, 1, '2013-05-29 17:23:07', '2013-05-29 17:23:07', 1, 0, 0),
(53, 1, '2013-05-29 19:14:09', '2013-05-29 19:14:09', 1, 0, 0),
(54, 1, '2013-05-29 19:14:14', '2013-05-29 19:14:14', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `track_e_default`
--

CREATE TABLE IF NOT EXISTS `track_e_default` (
  `default_id` int(11) NOT NULL AUTO_INCREMENT,
  `default_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `default_cours_code` varchar(40) NOT NULL DEFAULT '',
  `default_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `default_event_type` varchar(20) NOT NULL DEFAULT '',
  `default_value_type` varchar(20) NOT NULL DEFAULT '',
  `default_value` text NOT NULL,
  `c_id` int(10) unsigned DEFAULT NULL,
  `session_id` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`default_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `track_e_default`
--

INSERT INTO `track_e_default` (`default_id`, `default_user_id`, `default_cours_code`, `default_date`, `default_event_type`, `default_value_type`, `default_value`, `c_id`, `session_id`) VALUES
(2, 1, '', '2013-05-29 17:14:19', 'course_deleted', 'course_code', 'A00001', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `track_e_downloads`
--

CREATE TABLE IF NOT EXISTS `track_e_downloads` (
  `down_id` int(11) NOT NULL AUTO_INCREMENT,
  `down_user_id` int(10) unsigned DEFAULT NULL,
  `down_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `c_id` int(11) NOT NULL DEFAULT '0',
  `down_doc_path` varchar(255) NOT NULL DEFAULT '',
  `down_session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`down_id`),
  KEY `down_session_id` (`down_session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_downloads`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_exercices`
--

CREATE TABLE IF NOT EXISTS `track_e_exercices` (
  `exe_id` int(11) NOT NULL AUTO_INCREMENT,
  `exe_user_id` int(10) unsigned DEFAULT NULL,
  `exe_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `exe_exo_id` int(10) unsigned NOT NULL DEFAULT '0',
  `exe_result` float(6,2) NOT NULL DEFAULT '0.00',
  `exe_weighting` float(6,2) NOT NULL DEFAULT '0.00',
  `c_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(20) NOT NULL DEFAULT '',
  `data_tracking` text NOT NULL,
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `steps_counter` smallint(5) unsigned NOT NULL DEFAULT '0',
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  `orig_lp_id` int(11) NOT NULL DEFAULT '0',
  `orig_lp_item_id` int(11) NOT NULL DEFAULT '0',
  `exe_duration` int(10) unsigned NOT NULL DEFAULT '0',
  `expired_time_control` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `orig_lp_item_view_id` int(11) NOT NULL DEFAULT '0',
  `questions_to_check` text NOT NULL,
  PRIMARY KEY (`exe_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_exercices`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_hotpotatoes`
--

CREATE TABLE IF NOT EXISTS `track_e_hotpotatoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exe_name` varchar(255) NOT NULL,
  `exe_user_id` int(10) unsigned DEFAULT NULL,
  `exe_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `c_id` int(10) unsigned NOT NULL DEFAULT '0',
  `exe_result` smallint(6) NOT NULL DEFAULT '0',
  `exe_weighting` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_hotpotatoes`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_hotspot`
--

CREATE TABLE IF NOT EXISTS `track_e_hotspot` (
  `hotspot_id` int(11) NOT NULL AUTO_INCREMENT,
  `hotspot_user_id` int(11) NOT NULL,
  `hotspot_exe_id` int(11) NOT NULL,
  `hotspot_question_id` int(11) NOT NULL,
  `hotspot_answer_id` int(11) NOT NULL,
  `hotspot_correct` tinyint(3) unsigned NOT NULL,
  `hotspot_coordinate` text NOT NULL,
  `c_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`hotspot_id`),
  KEY `hotspot_user_id` (`hotspot_user_id`),
  KEY `hotspot_exe_id` (`hotspot_exe_id`),
  KEY `hotspot_question_id` (`hotspot_question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_hotspot`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_item_property`
--

CREATE TABLE IF NOT EXISTS `track_e_item_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `item_property_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `progress` int(11) NOT NULL DEFAULT '0',
  `lastedit_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastedit_user_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`,`item_property_id`,`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_item_property`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_lastaccess`
--

CREATE TABLE IF NOT EXISTS `track_e_lastaccess` (
  `access_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_user_id` int(10) unsigned DEFAULT NULL,
  `access_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `c_id` int(11) NOT NULL DEFAULT '0',
  `access_tool` varchar(30) DEFAULT NULL,
  `access_session_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`access_id`),
  KEY `access_c_id_user_id` (`c_id`,`access_user_id`),
  KEY `access_user_id` (`access_user_id`),
  KEY `access_cours_code` (`c_id`),
  KEY `access_session_id` (`access_session_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `track_e_lastaccess`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_links`
--

CREATE TABLE IF NOT EXISTS `track_e_links` (
  `links_id` int(11) NOT NULL AUTO_INCREMENT,
  `links_user_id` int(10) unsigned DEFAULT NULL,
  `links_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `c_id` int(11) NOT NULL DEFAULT '0',
  `links_link_id` int(11) NOT NULL DEFAULT '0',
  `links_session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`links_id`),
  KEY `links_user_id` (`links_user_id`),
  KEY `links_session_id` (`links_session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_links`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_login`
--

CREATE TABLE IF NOT EXISTS `track_e_login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(39) NOT NULL DEFAULT '',
  `logout_date` datetime DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  KEY `login_user_id` (`login_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `track_e_login`
--

INSERT INTO `track_e_login` (`login_id`, `login_user_id`, `login_date`, `login_ip`, `logout_date`) VALUES
(1, 1, '2013-05-24 17:50:44', '127.0.0.1', '2013-05-30 00:14:24');

-- --------------------------------------------------------

--
-- Table structure for table `track_e_online`
--

CREATE TABLE IF NOT EXISTS `track_e_online` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(39) NOT NULL DEFAULT '',
  `course` varchar(40) DEFAULT NULL,
  `session_id` int(11) NOT NULL DEFAULT '0',
  `access_url_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`login_id`),
  KEY `login_user_id` (`login_user_id`),
  KEY `course` (`course`),
  KEY `session_id` (`session_id`),
  KEY `idx_trackonline_uat` (`login_user_id`,`access_url_id`,`login_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `track_e_online`
--

INSERT INTO `track_e_online` (`login_id`, `login_user_id`, `login_date`, `login_ip`, `course`, `session_id`, `access_url_id`) VALUES
(1, 1, '2013-05-29 17:14:23', '127.0.0.1', NULL, 0, 1),
(2, 2, '2013-05-25 15:53:06', '127.0.0.1', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `track_e_open`
--

CREATE TABLE IF NOT EXISTS `track_e_open` (
  `open_id` int(11) NOT NULL AUTO_INCREMENT,
  `open_remote_host` tinytext NOT NULL,
  `open_agent` tinytext NOT NULL,
  `open_referer` tinytext NOT NULL,
  `open_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`open_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_open`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_e_uploads`
--

CREATE TABLE IF NOT EXISTS `track_e_uploads` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_user_id` int(10) unsigned DEFAULT NULL,
  `upload_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `upload_cours_id` varchar(40) NOT NULL DEFAULT '',
  `upload_work_id` int(11) NOT NULL DEFAULT '0',
  `upload_session_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`upload_id`),
  KEY `upload_user_id` (`upload_user_id`),
  KEY `upload_cours_id` (`upload_cours_id`),
  KEY `upload_session_id` (`upload_session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_e_uploads`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_stored_values`
--

CREATE TABLE IF NOT EXISTS `track_stored_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sco_id` int(11) NOT NULL,
  `course_id` char(40) NOT NULL,
  `sv_key` char(64) NOT NULL,
  `sv_value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`sco_id`,`course_id`,`sv_key`),
  KEY `user_id` (`user_id`,`sco_id`,`course_id`,`sv_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_stored_values`
--


-- --------------------------------------------------------

--
-- Table structure for table `track_stored_values_stack`
--

CREATE TABLE IF NOT EXISTS `track_stored_values_stack` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sco_id` int(11) NOT NULL,
  `stack_order` int(11) NOT NULL,
  `course_id` char(40) NOT NULL,
  `sv_key` char(64) NOT NULL,
  `sv_value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`sco_id`,`course_id`,`sv_key`,`stack_order`),
  KEY `user_id` (`user_id`,`sco_id`,`course_id`,`sv_key`,`stack_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `track_stored_values_stack`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lastname` varchar(60) DEFAULT NULL,
  `firstname` varchar(60) DEFAULT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `auth_source` varchar(50) DEFAULT 'platform',
  `email` varchar(100) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '5',
  `official_code` varchar(40) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `picture_uri` varchar(250) DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `competences` text,
  `diplomas` text,
  `openarea` text,
  `teach` text,
  `productions` varchar(250) DEFAULT NULL,
  `chatcall_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `chatcall_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `chatcall_text` varchar(50) NOT NULL DEFAULT '',
  `language` varchar(40) DEFAULT NULL,
  `registration_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expiration_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `openid` varchar(255) DEFAULT NULL,
  `theme` varchar(255) DEFAULT NULL,
  `hr_dept_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `lastname`, `firstname`, `username`, `password`, `auth_source`, `email`, `status`, `official_code`, `phone`, `picture_uri`, `creator_id`, `competences`, `diplomas`, `openarea`, `teach`, `productions`, `chatcall_user_id`, `chatcall_date`, `chatcall_text`, `language`, `registration_date`, `expiration_date`, `active`, `openid`, `theme`, `hr_dept_id`) VALUES
(1, 'Toan', 'Tran', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'platform', 'qtoantin5@gmail.com', 1, 'ADMIN', '0985437140', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '', 'vietnamese', '2013-05-25 00:49:07', '0000-00-00 00:00:00', 1, NULL, NULL, 0),
(2, 'Anonymous', 'Joe', '', '', 'platform', 'anonymous@localhost', 6, 'anonymous', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '', 'vietnamese', '2013-05-25 00:49:07', '0000-00-00 00:00:00', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `usergroup`
--

CREATE TABLE IF NOT EXISTS `usergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `group_type` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `picture` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `visibility` int(11) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `created_on` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `usergroup`
--


-- --------------------------------------------------------

--
-- Table structure for table `usergroup_rel_course`
--

CREATE TABLE IF NOT EXISTS `usergroup_rel_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usergroup_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `usergroup_rel_course`
--


-- --------------------------------------------------------

--
-- Table structure for table `usergroup_rel_question`
--

CREATE TABLE IF NOT EXISTS `usergroup_rel_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `usergroup_id` int(10) unsigned NOT NULL,
  `coefficient` float(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `usergroup_rel_question`
--


-- --------------------------------------------------------

--
-- Table structure for table `usergroup_rel_session`
--

CREATE TABLE IF NOT EXISTS `usergroup_rel_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usergroup_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `usergroup_rel_session`
--


-- --------------------------------------------------------

--
-- Table structure for table `usergroup_rel_tag`
--

CREATE TABLE IF NOT EXISTS `usergroup_rel_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `usergroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usergroup_id` (`usergroup_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `usergroup_rel_tag`
--


-- --------------------------------------------------------

--
-- Table structure for table `usergroup_rel_user`
--

CREATE TABLE IF NOT EXISTS `usergroup_rel_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usergroup_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `relation_type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `usergroup_id` (`usergroup_id`),
  KEY `user_id` (`user_id`),
  KEY `relation_type` (`relation_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `usergroup_rel_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `usergroup_rel_usergroup`
--

CREATE TABLE IF NOT EXISTS `usergroup_rel_usergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `subgroup_id` int(11) NOT NULL,
  `relation_type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `subgroup_id` (`subgroup_id`),
  KEY `relation_type` (`relation_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `usergroup_rel_usergroup`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_api_key`
--

CREATE TABLE IF NOT EXISTS `user_api_key` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `api_key` char(32) NOT NULL,
  `api_service` char(10) NOT NULL DEFAULT 'dokeos',
  `api_end_point` text,
  `created_date` datetime DEFAULT NULL,
  `validity_start_date` datetime DEFAULT NULL,
  `validity_end_date` datetime DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `idx_user_api_keys_user` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_api_key`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_course_category`
--

CREATE TABLE IF NOT EXISTS `user_course_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_c_cat_uid` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_course_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_field`
--

CREATE TABLE IF NOT EXISTS `user_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_type` int(11) NOT NULL DEFAULT '1',
  `field_variable` varchar(64) NOT NULL,
  `field_display_text` varchar(64) DEFAULT NULL,
  `field_default_value` text,
  `field_order` int(11) DEFAULT NULL,
  `field_visible` tinyint(4) DEFAULT '0',
  `field_changeable` tinyint(4) DEFAULT '0',
  `field_filter` tinyint(4) DEFAULT '0',
  `field_loggeable` int(11) DEFAULT '0',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `user_field`
--

INSERT INTO `user_field` (`id`, `field_type`, `field_variable`, `field_display_text`, `field_default_value`, `field_order`, `field_visible`, `field_changeable`, `field_filter`, `field_loggeable`, `tms`) VALUES
(1, 1, 'legal_accept', 'Legal', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(2, 1, 'already_logged_in', 'Already logged in', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(3, 1, 'update_type', 'Update script type', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(4, 10, 'tags', 'tags', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(5, 1, 'rssfeeds', 'RSS', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(6, 1, 'dashboard', 'Dashboard', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(7, 11, 'timezone', 'Timezone', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(8, 4, 'mail_notify_invitation', 'MailNotifyInvitation', '1', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00'),
(9, 4, 'mail_notify_message', 'MailNotifyMessage', '1', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00'),
(10, 4, 'mail_notify_group_message', 'MailNotifyGroupMessage', '1', NULL, 1, 1, 0, 0, '0000-00-00 00:00:00'),
(11, 1, 'user_chat_status', 'User chat status', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00'),
(12, 1, 'google_calendar_url', 'Google Calendar URL', NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_field_options`
--

CREATE TABLE IF NOT EXISTS `user_field_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `option_value` text,
  `option_display_text` varchar(64) DEFAULT NULL,
  `option_order` int(11) DEFAULT NULL,
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `user_field_options`
--

INSERT INTO `user_field_options` (`id`, `field_id`, `option_value`, `option_display_text`, `option_order`, `tms`) VALUES
(1, 8, '1', 'AtOnce', 1, '0000-00-00 00:00:00'),
(2, 8, '8', 'Daily', 2, '0000-00-00 00:00:00'),
(3, 8, '0', 'No', 3, '0000-00-00 00:00:00'),
(4, 9, '1', 'AtOnce', 1, '0000-00-00 00:00:00'),
(5, 9, '8', 'Daily', 2, '0000-00-00 00:00:00'),
(6, 9, '0', 'No', 3, '0000-00-00 00:00:00'),
(7, 10, '1', 'AtOnce', 1, '0000-00-00 00:00:00'),
(8, 10, '8', 'Daily', 2, '0000-00-00 00:00:00'),
(9, 10, '0', 'No', 3, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_field_values`
--

CREATE TABLE IF NOT EXISTS `user_field_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(100) DEFAULT '',
  `tms` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`field_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_field_values`
--

INSERT INTO `user_field_values` (`id`, `user_id`, `field_id`, `field_value`, `author_id`, `comment`, `tms`) VALUES
(1, 1, 2, 'true', 0, '', '2013-05-25 15:53:20'),
(2, 1, 11, '0', 0, '', '2013-05-29 16:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `user_friend_relation_type`
--

CREATE TABLE IF NOT EXISTS `user_friend_relation_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user_friend_relation_type`
--

INSERT INTO `user_friend_relation_type` (`id`, `title`) VALUES
(1, 'SocialUnknow'),
(2, 'SocialParent'),
(3, 'SocialFriend'),
(4, 'SocialGoodFriend'),
(5, 'SocialEnemy'),
(6, 'SocialDeleted');

-- --------------------------------------------------------

--
-- Table structure for table `user_rel_course_vote`
--

CREATE TABLE IF NOT EXISTS `user_rel_course_vote` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned NOT NULL DEFAULT '0',
  `url_id` int(10) unsigned NOT NULL DEFAULT '0',
  `vote` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ucv_cid` (`c_id`),
  KEY `idx_ucv_uid` (`user_id`),
  KEY `idx_ucv_cuid` (`user_id`,`c_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_rel_course_vote`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_rel_event_type`
--

CREATE TABLE IF NOT EXISTS `user_rel_event_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_name_index` (`event_type_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_rel_event_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_rel_tag`
--

CREATE TABLE IF NOT EXISTS `user_rel_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_rel_tag`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_rel_user`
--

CREATE TABLE IF NOT EXISTS `user_rel_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_user_id` int(10) unsigned NOT NULL,
  `relation_type` int(11) NOT NULL DEFAULT '0',
  `last_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_rel_user__user` (`user_id`),
  KEY `idx_user_rel_user__friend_user` (`friend_user_id`),
  KEY `idx_user_rel_user__user_friend_user` (`user_id`,`friend_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_rel_user`
--

